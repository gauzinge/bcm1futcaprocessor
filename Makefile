# $Id$
# I'm a bcm1f bril utca processor
#
BUILD_HOME:=$(shell pwd)/../..
BUILD_SUPPORT=build
PROJECT_NAME=bril

include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfAutoconf.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfDefs.$(XDAQ_OS)
include $(BUILD_HOME)/mfDefs.$(PROJECT_NAME)

Project=$(PROJECT_NAME)
Package=bril/bcm1futcaprocessor
Sources = $(wildcard src/common/*.cc)
IncludeDirs = \
	$(XI2O_UTILS_INCLUDE_PREFIX) \
        $(XERCES_INCLUDE_PREFIX) \
        $(LOG4CPLUS_INCLUDE_PREFIX) \
        $(CGICC_INCLUDE_PREFIX) \
        $(XCEPT_INCLUDE_PREFIX) \
        $(CONFIG_INCLUDE_PREFIX) \
	$(TOOLBOX_INCLUDE_PREFIX) \
        $(PT_INCLUDE_PREFIX) \
	$(XDAQ_INCLUDE_PREFIX) \
        $(XDATA_INCLUDE_PREFIX) \
        $(XOAP_INCLUDE_PREFIX) \
        $(XGI_INCLUDE_PREFIX) \
        $(I2O_INCLUDE_PREFIX) \
        $(XI2O_INCLUDE_PREFIX) \
        $(B2IN_NUB_INCLUDE_PREFIX) \
	$(BRIL_WEBUTILS_INCLUDE_PREFIX) \
	$(INTERFACE_BRIL_INCLUDE_PREFIX) 

LibraryDirs = 
UserSourcePath =
UserCFlags = 
UserCCFlags = 
UserDynamicLinkFlags =
UserStaticLinkFlags =
UserExecutableLinkFlags = 
DependentLibraryDirs = 
DependentLibraries = 
ExternalObjects = 
DynamicLibrary=brilbcm1futcaprocessor
StaticLibrary=
TestLibraries=
TestExecutables=
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/Makefile.rules
include $(XDAQ_ROOT)/$(BUILD_SUPPORT)/mfRPM.rules
include webdev.rules
