//TODO:
//remove message counter and output eventually
#include "bril/bcm1futcaprocessor/Application.h"

using namespace cgicc;

struct RefDestroyer  ////RAII exception safe guard object
{
    toolbox::mem::Reference* m_ref;
    RefDestroyer ( toolbox::mem::Reference* ref ) : m_ref ( ref ) {}
    ~RefDestroyer()
    {
        if ( m_ref ) m_ref->release();
    }
};

XDAQ_INSTANTIATOR_IMPL (bril::bcm1futcaprocessor::Application)

//C'tor
bril::bcm1futcaprocessor::Application::Application (xdaq::ApplicationStub* s ):
    xdaq::Application (s),
    xgi::framework::UIManager (this),
    eventing::api::Member (this),
    m_appLock (toolbox::BSem::FULL),
    m_occQueue (100),
    m_ampQueue (100),
    m_rawQueue (100),
    m_occQueueLS (100),
    m_ampQueueLS (100),
    m_lumiAlgorithm (NULL),
    m_aggregateAlgorithm (NULL),
    m_backgroundAlgorithm (NULL),
    m_albedoAlgorithm (NULL)
{
    //DEBUG TODO
    filestream.open ("bcm1futcaprocessor_time.log", std::ofstream::out);
    longesttime = 0;

    try
    {
        //bind default callback
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::Default, "Default");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::OverviewPage, "OverviewPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::ChannelPage, "ChannelPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::AlbedoPage, "AlbedoPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::InputDataPage, "InputDataPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::ConfigPage, "ConfigPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::MonitoringPage, "MonitoringPage");
        xgi::framework::deferredbind (this, this, &bril::bcm1futcaprocessor::Application::BeamPage, "BeamPage");
        //bind callback for b2in messages (eventing)
        b2in::nub::bind (this, &bril::bcm1futcaprocessor::Application::onMessage);
        //bind callback for webchars
        xgi::deferredbind ( this, this, &Application::requestRawData, "requestRawData" );
        xgi::deferredbind ( this, this, &Application::requestOverviewData, "requestOverviewData" );
        xgi::deferredbind ( this, this, &Application::requestChannelData, "requestChannelData" );
        xgi::deferredbind ( this, this, &Application::requestAlbedoData, "requestAlbedoData" );

        // setting shutdown handler to stop all workloops
        //toolbox::getRuntime()->addShutdownListener (this);

        //initialize all member variables
        m_appInfoSpace = this->getApplicationInfoSpace();
        m_appDescriptor = this->getApplicationDescriptor();
        m_className = m_appDescriptor->getClassName();
        m_appURL = m_appDescriptor->getContextDescriptor()->getURL() + "/" + this->getApplicationDescriptor()->getURN();
        m_logger = this->getApplicationLogger();

        //add a listener for xdata::Event::setDefault values
        m_appInfoSpace->addListener (this, "urn:xdaq-event:setDefaultValues");

        //now set the inputs from the config files
        m_appInfoSpace->fireItemAvailable ("eventinginput", &m_dataSources);
        m_appInfoSpace->fireItemAvailable ("eventingoutput", &m_dataSinks);
        m_appInfoSpace->fireItemAvailable ("channels", &m_channelConfig);
        m_appInfoSpace->fireItemAvailable ("lumialgorithm", &m_lumiProperties);
        m_appInfoSpace->fireItemAvailable ("aggregatealgorithm", &m_aggregateProperties);
        m_appInfoSpace->fireItemAvailable ("backgroundalgorithm", &m_backgroundProperties);
        m_appInfoSpace->fireItemAvailable ("albedoalgorithm", &m_albedoProperties);
        m_appInfoSpace->fireItemAvailable ("timeout", &m_timeout);
        m_appInfoSpace->fireItemAvailable ("webcharts", &m_webCharts);

        //memory Pools for outgoing messages
        m_memPoolFactory = toolbox::mem::getMemoryPoolFactory();
        //use the URN instead of the class name, this way it's unambiguous
        toolbox::net::URN t_memURN ("toolbox-mem-pool", m_appDescriptor->getURN() );
        m_memPool = m_memPoolFactory->createPool (t_memURN, new toolbox::mem::HeapAllocator() );

        //initialize / clear data containers
        m_occCache.clear();
        m_ampCache.clear();
        m_rawCache.clear();

        m_beamMode = "";
        m_VDMFlag = false;
        m_VDMIP5 = false;


        //set up the monitoring already now
        this->setupMonitoring();

        //GUI
        m_tab = Tab::OVERVIEW;

        //init the algorithm pointers to null
        m_aggregateAlgorithm = nullptr;
        m_backgroundAlgorithm = nullptr;
        m_lumiAlgorithm = nullptr;
        m_albedoAlgorithm = nullptr;
    }
    catch (xcept::Exception& e)
    {
        std::stringstream msg;
        msg << RED << " : Caught Exception " << RESET;
        LOG4CPLUS_FATAL (m_logger, __func__ << msg.str() << e.what() );
        //notify_qualified ("error", e);
        throw; // we don't want to continue in case we already have an exception in the constructor
    }

    LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Finished constructor, point your browser to " << BLUE << m_appURL << RESET);
}

//D'tor
bril::bcm1futcaprocessor::Application::~Application()
{
    //Never called when Ctrl-C The code is moved to actionPerformed on toolbox::EventShutdown
}

void bril::bcm1futcaprocessor::Application::Default (xgi::Input* in, xgi::Output* out)
{
    this->ConfigPage (in, out);
}

// we only care for the setDefaultValues event here
void bril::bcm1futcaprocessor::Application::actionPerformed (xdata::Event& e)
{
    LOG4CPLUS_DEBUG (m_logger, __func__ << "Received xdata::Event: " << e.type() );

    if ( e.type() == "urn:xdaq-event:setDefaultValues" )
    {
        try
        {
            //set the list of good channels and algorithms
            this->setupChannels();

            //set up the eventing: subscribe to topics and add action listeners for output buses
            this->setupEventing();

            //set up the webcharts
            if (m_webCharts)
                this->setupCharts();

            //set up the algorithm
            this->setupAlgorithms();

        }
        catch ( xcept::Exception& e )
        {
            std::stringstream msg;
            msg << RED << "Exception on setDefaultValues: " << e.what() << RESET;
            LOG4CPLUS_ERROR ( getApplicationLogger(), msg.str() );
            notifyQualified ( "error", e );
        }

        try
        {
            toolbox::TimeInterval checkinterval (10, 0); // check every 10 seconds
            std::string timername ("stalecachecheck_timer");
            toolbox::task::Timer* timer = toolbox::task::getTimerFactory()->createTimer (timername );
            toolbox::TimeVal start = toolbox::TimeVal::gettimeofday();
            timer->scheduleAtFixedRate (start, this, checkinterval, 0, timername);
        }
        catch (toolbox::exception::Exception& e)
        {
            std::stringstream msg;
            msg << RED << "failed to start timer :" << e.what() << RESET;
            LOG4CPLUS_ERROR (getApplicationLogger(), msg.str()  );
            XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::SetupException, msg.str(), e);
        } //end of set default values xdata::Event type
    }

    else if ( e.type() == "ItemChangedEvent" )
    {
        std::string item = static_cast<xdata::ItemEvent&> ( e ).itemName();
        LOG4CPLUS_INFO ( getApplicationLogger(), BOLDBLUE << __func__ << ": Parameter " << item << " changed."  << RESET);
        //do something here if necessary!
    }
}

//llisten for toolbox::Event which is the case when buses are ready to publish
//so start the publishing workloop here
void bril::bcm1futcaprocessor::Application::actionPerformed (toolbox::Event& e)
{
    LOG4CPLUS_DEBUG (m_logger, __func__ << "Received xdata::Event: " << e.type() );

    if (e.type() == "eventing::api::BusReadyToPublish")
    {
        try
        {
            std::string t_busName = (static_cast<eventing::api::Bus*> (e.originator() ) )->getBusName();
            LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Bus: " << t_busName << " is ready to publish!" << RESET);
            m_unreadyBuses.erase (t_busName);

            if (!m_unreadyBuses.empty() ) return;

            LOG4CPLUS_INFO (m_logger, BOLDGREEN << std::endl << "All output buses ready to publish!" << RESET);

            //start the workloops
            this->setupWorkloops();
        }
        catch (xcept::Exception& e)
        {
            std::stringstream msg;
            msg << RED << " : Caught Exception on toolbox::Event::BusReadyToPublish" << RESET;
            LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
            XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), e);
        }
    }

    if (e.type() == "Shutdown")
    {
        //first, clean up
        if (m_lumiAlgorithm != nullptr) delete m_lumiAlgorithm;

        if (m_backgroundAlgorithm != nullptr) delete m_backgroundAlgorithm;

        if (m_albedoAlgorithm != nullptr) delete m_albedoAlgorithm;

        if (m_aggregateAlgorithm != nullptr) delete m_aggregateAlgorithm;

        //shutdown all workloops
        try
        {
            this->m_occWorkloop->cancel();
            this->m_ampWorkloop->cancel();
            this->m_rawWorkloop->cancel();
            this->m_publishWorkloop->cancel();
            this->m_albedoWorkloop->cancel();
        }
        catch (std::exception&) {}

        LOG4CPLUS_INFO (m_logger, GREEN << "Exited gracefully!" << RESET);
    }
}

//calback if message is received on the eventing bus
void bril::bcm1futcaprocessor::Application::onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist)
{
    //Timer t;
    //t.start();
    std::string t_action = plist.getProperty ("urn:b2in-eventing:action");

    //if action not notify or if reference is 0
    if (t_action != "notify" )
        return;

    if ( ref == 0)
        return;

    std::string t_topic = plist.getProperty ("urn:b2in-eventing:topic");

    //check version header in message
    std::string t_version = plist.getProperty ("DATA_VERSION");

    if (t_version.empty()  || t_version != interface::bril::DATA_VERSION)
    {
        LOG4CPLUS_ERROR (m_logger, __func__ << RED << "Received message on topic " << t_topic << " without or with wrong version header, doing nothing!" << RESET);
        return;
    }
    else
    {
        std::pair<std::string, toolbox::mem::Reference*> pair = std::make_pair (t_topic, ref);
        m_inputQueue.push (pair );
        //m_waitMessage.signal();
    }

    //t.stop();
    //std::stringstream ss;

    //if (t.getElapsedTime() > longesttime)
    //{
    //longesttime = t.getElapsedTime();
    //ss << "ATTENTION: longest time yet!" << std::endl;
    //}

    //t.show ("onMessage topic: " + t_topic, ss);
    //filestream << ss.str();
}

void bril::bcm1futcaprocessor::Application::timeExpired (toolbox::task::TimerEvent& e)
{
    unsigned int timeout_sec_nb4 = 5;
    unsigned int timeout_sec_LS = 60;
    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
    unsigned int nowsec = t.sec();
    std::stringstream msg;

    std::stringstream occLog;

    if (this->is_subscribed (interface::bril::bcm1futcaocchistT::topicname() ) )
    {
        if (nowsec - m_occCache.getLastUpdate() > m_timeout)
        {
            msg << YELLOW << " No addition to occupancy Cache in last " << m_timeout << " seconds - runnumber: " << m_lastOccHeader.runnum << " lumisection: " << m_lastOccHeader.lsnum << " nibble: " << m_lastOccHeader.nbnum << RESET << std::endl;

            if (!m_occCache.is_clean() )
            {
                // get whatever completed NB4s there are and then clear the cache
                OccupancyHistogramVector t_occVec = m_occCache.get_completedNB4 (timeout_sec_nb4, occLog);

                if (t_occVec.size() )
                    m_occQueue.push (t_occVec);

                if (!occLog.str().empty() )
                {
                    LOG4CPLUS_INFO (m_logger, "Time expired (timeout): " << occLog.str() );
                    m_missingHistogramLog += occLog.str();
                    occLog.str ("");
                }

                OccupancyHistogramVectorLS t_occVecLS = m_occCache.get_completedLS (timeout_sec_LS, msg);

                if (t_occVecLS.size() )
                    m_occQueueLS.push (t_occVecLS);
            }
        }
    }

    if (this->is_subscribed (interface::bril::bcm1futcaamphistT::topicname() ) )
    {
        if (nowsec - m_ampCache.getLastUpdate() > m_timeout)
        {
            msg << YELLOW << " No addition to amplitude Cache in last " << m_timeout << " seconds - runnumber: " << m_lastAmpHeader.runnum << " lumisection: " << m_lastAmpHeader.lsnum << " nibble: " << m_lastAmpHeader.nbnum << RESET << std::endl;

            if (!m_ampCache.is_clean() )
            {
                // get whatever completed NB4s there are and then clear the cache
                AmplitudeHistogramVector t_ampVec = m_ampCache.get_completedNB4 (timeout_sec_nb4, msg);

                if (t_ampVec.size() )
                    m_ampQueue.push (t_ampVec);

                AmplitudeHistogramVectorLS t_ampVecLS = m_ampCache.get_completedLS (timeout_sec_LS, msg);

                if (t_ampVecLS.size() )
                    m_ampQueueLS.push (t_ampVecLS);
            }
        }
    }

    if (this->is_subscribed (interface::bril::bcm1futcarawdataT::topicname() ) )
    {
        if (nowsec - m_rawCache.getLastUpdate() > m_timeout )
        {
            msg << YELLOW << " No addition to raw data Cache in last " << m_timeout << " seconds - runnumber: " << m_lastRawHeader.runnum << " lumisection: " << m_lastRawHeader.lsnum << " nibble: " << m_lastRawHeader.nbnum << RESET << std::endl;

            if (!m_rawCache.is_clean() )
            {
                // get whatever completed NB4s there are and then clear the cache
                RawHistogramVector t_rawVec = m_rawCache.get_completedNB4 (timeout_sec_nb4, msg);

                if (t_rawVec.size() )
                    m_rawQueue.push (t_rawVec);

                RawHistogramVectorLS t_rawVecLS = m_rawCache.get_completedLS (timeout_sec_LS, msg);
            }
        }
    }

    //printCacheStatus();
    //std::cout << "Last occupancy histogram: " << nowsec - m_occCache.getLastUpdate() << std::endl;
    //std::cout << "Last amplitude histogram: " << nowsec - m_ampCache.getLastUpdate() << std::endl;
    //std::cout << "Last raw histogram: " << nowsec - m_rawCache.getLastUpdate() << std::endl;
    if (!msg.str().empty() )
        LOG4CPLUS_INFO (m_logger, msg.str() );
}

////////////////////////////////////////////////////////////////////////////////
//PRIVATE METHODS
////////////////////////////////////////////////////////////////////////////////

void bril::bcm1futcaprocessor::Application::setupChannels()
{
    std::stringstream msg;
    m_channelInfo.setupChannels (m_channelConfig, msg);

    if (!msg.str().empty() )
        LOG4CPLUS_INFO (m_logger, msg.str() );

    m_occCache.setChannelAndAlgoList (m_channelInfo.getChannelVector(), m_channelInfo.getAlgoVector() );
    m_ampCache.setChannelAndAlgoList (m_channelInfo.getChannelVector(), m_channelInfo.getAlgoVector() );
    m_rawCache.setChannelAndAlgoList (m_channelInfo.getChannelVector(), m_channelInfo.getAlgoVector() );

    // fill the total channel rates with the correct size and initialize to 0
    m_monVariables.m_totalChannelRate.resize (interface::bril::BCM1F_NCHANNELS * m_channelInfo.getAlgoVector().size(), 0.);
}

void bril::bcm1futcaprocessor::Application::setupEventing()
{
    try
    {
        //first the input
        for (size_t source = 0; source < m_dataSources.elements(); source++)
        {
            std::string t_bus = m_dataSources[source].getProperty ("bus");
            std::string t_topics = m_dataSources[source].getProperty ("topics");
            //separate the topic list into unique elements
            auto t_topicNames = toolbox::parseTokenSet (t_topics, ",");

            for (auto topic : t_topicNames)
            {
                LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Subscribing to " << BLUE << topic << GREEN << " on bus " << BLUE << t_bus << RESET);
                this->getEventingBus (t_bus).subscribe (topic);

                if (m_inputTopics.find (topic) == m_inputTopics.end() )
                    m_inputTopics.insert (topic);
            }
        }
    }
    catch (eventing::api::exception::Exception& e)
    {
        std::stringstream msg;
        msg << RED << " : Failed to subscribe to one or more topics! - " << RESET;
        LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
        //topic = t_topicNames.erase (topic);
        //raise my exception
        //XCEPT_RAISE (bril::bcm1futcaprocessor::exception::EventingException, msg.str() );
        //rethrow as my type
        //XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), dynamic_cast<bril::bcm1futcaprocessor::EventingException&>(e).exp_);

        //do not throw but notify error collector
        XCEPT_DECLARE ( bril::bcm1futcaprocessor::exception::EventingException, myerrorobj, msg.str() );
        this->notifyQualified ("error", myerrorobj);
    }

    //now lets handle the output topics
    m_outTopicMap.clear();
    m_unreadyBuses.clear();

    for (size_t sink = 0; sink < m_dataSinks.elements(); sink++)
    {
        std::string t_topic = m_dataSinks[sink].getProperty ("topic");
        std::string t_buses = m_dataSinks[sink].getProperty ("buses");
        BusSet t_set = toolbox::parseTokenSet (t_buses, ",");
        m_outTopicMap[t_topic] = t_set;
        m_unreadyBuses.insert (t_set.begin(), t_set.end() );
    }

    //now parsed the configuration, let's wait for the buses
    for (auto bus = m_unreadyBuses.begin(); bus != m_unreadyBuses.end(); bus++)
    {
        try
        {
            LOG4CPLUS_INFO (m_logger, GREEN << std::endl << "Waiting for bus: " << BLUE << *bus << RED << " to be ready!" << RESET);
            getEventingBus (*bus).addActionListener (this);
        }
        catch (eventing::api::exception::Exception& e)
        {
            std::stringstream msg;
            msg << RED << " : Failed to add action listener on bus " << BLUE << *bus << RESET << " ";
            LOG4CPLUS_ERROR (m_logger, __func__ << msg.str() << e.what() );
            //raise my exception
            //XCEPT_RAISE (bril::bcm1futcaprocessor::exception::EventingException, msg.str() );
            //rethrow as my type
            //XCEPT_RETHROW (bril::bcm1futcaprocessor::exception::EventingException, msg.str(), dynamic_cast<bril::bcm1futcaprocessor::EventingException&>(e).exp_);

            //do not throw but notify error collector
            XCEPT_DECLARE ( bril::bcm1futcaprocessor::exception::EventingException, myerrorobj, msg.str() );
            this->notifyQualified ("error", myerrorobj);
        }
    }
}

void bril::bcm1futcaprocessor::Application::setupWorkloops()
{
    //start the input workloop
    this->m_inputWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processInput", "waiting" );
    toolbox::task::ActionSignature* as_processingInput = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::inputJob, "processInput" );
    this->m_inputWorkloop->activate();
    this->m_inputWorkloop->submit ( as_processingInput );
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << std::endl << "Starting input processing workloop!" << RESET);


    //now the processing workloops for the histograms
    if (this->is_subscribed (interface::bril::bcm1futcaocchistT::topicname() ) )
    {
        this->m_occWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processOccupancy", "waiting" );
        toolbox::task::ActionSignature* as_processingOcc = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::occProcessJob, "processOccupancy" );
        this->m_occWorkloop->activate();
        this->m_occWorkloop->submit ( as_processingOcc );
        LOG4CPLUS_INFO (m_logger, BOLDYELLOW << std::endl << "Starting occupancy processing workloop!" << RESET);
    }

    if (this->is_subscribed (interface::bril::bcm1futcaamphistT::topicname() ) )
    {
        this->m_ampWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processAmplitude", "waiting" );
        toolbox::task::ActionSignature* as_processingAmp = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::ampProcessJob, "processAmplitude" );
        this->m_ampWorkloop->activate();
        this->m_ampWorkloop->submit ( as_processingAmp );
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << std::endl << "Starting amplitude processing workloop!" << RESET);
    }

    if (this->is_subscribed (interface::bril::bcm1futcarawdataT::topicname() ) )
    {
        this->m_rawWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_processRawData", "waiting" );
        toolbox::task::ActionSignature* as_processingRaw = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::rawProcessJob, "processRawData" );
        this->m_rawWorkloop->activate();
        this->m_rawWorkloop->submit ( as_processingRaw );
        LOG4CPLUS_INFO (m_logger, BOLDMAGENTA << std::endl << "Starting raw data processing workloop!" << RESET);
    }

    //and finally the publishing workloop
    this->m_publishWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_publishHistograms", "waiting" );
    toolbox::task::ActionSignature* as_publishing = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::publishJob, "publishHistograms" );
    this->m_publishWorkloop->activate();
    this->m_publishWorkloop->submit ( as_publishing );
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << std::endl << "Starting publishing workloop!" << RESET);

    //the albedo workloop
    this->m_albedoWorkloop = toolbox::task::getWorkLoopFactory()->getWorkLoop ( m_appDescriptor->getURN() + "_computeAlbedo", "waiting" );
    toolbox::task::ActionSignature* as_computingAlbedo = toolbox::task::bind ( this, &bril::bcm1futcaprocessor::Application::albedoJob, "computeAlbedo" );
    this->m_albedoWorkloop->activate();
    this->m_albedoWorkloop->submit ( as_computingAlbedo );
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << std::endl << "Starting albedo workloop!" << RESET);
}

void bril::bcm1futcaprocessor::Application::onMessageTopicBeam ( toolbox::mem::Reference* ref)
{
    //LOG4CPLUS_INFO (m_logger, GREEN << "Received message on topic beam!" << RESET);
    //from here on need to protect the beamInfo object with a BSEM
    m_appLock.take();
    m_beamInfo.update (ref);

    if (m_beamMode != m_beamInfo.getBeamMode() )
    {
        m_beamMode = m_beamInfo.getBeamMode();
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << "Beam Mode changed to " << m_beamMode << RESET);
    }

    std::stringstream msg;
    m_beamInfo.printStatus (msg);
    m_appLock.give();
    LOG4CPLUS_DEBUG (m_logger, msg.str() );

}

void bril::bcm1futcaprocessor::Application::onMessageTopicVdmFlag ( toolbox::mem::Reference* ref )
{
    m_appLock.take();
    m_beamInfo.updateVdmFlag (ref);

    if (m_VDMFlag != m_beamInfo.m_vdmFlag)
    {
        m_VDMFlag = m_beamInfo.m_vdmFlag;
        LOG4CPLUS_INFO (m_logger, BOLDCYAN << "VDM Flag changed to " << m_VDMFlag << RESET);
    }

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::onMessageTopicVdmScan ( toolbox::mem::Reference* ref )
{
    m_appLock.take();
    m_beamInfo.updateVdmIP (ref);

    if (m_VDMIP5 != m_beamInfo.m_vdmIP5)
    {
        m_VDMIP5 = m_beamInfo.m_vdmIP5;

        if (m_VDMIP5)
            LOG4CPLUS_INFO (m_logger, BOLDCYAN << "VDM IP changed to IP 5!" << RESET);
    }

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::printCacheStatus()
{
    std::stringstream msg;
    msg << std::endl << BOLDYELLOW << "Occupancy Cache Status: " << RESET << std::endl;
    m_occCache.print (msg);
    msg << BOLDYELLOW << "Occupancy Cache Status[LS]: " << RESET << std::endl;
    m_occCache.printLS (msg);

    msg << BOLDCYAN << "Amplitude Cache Status: " << RESET << std::endl;
    m_ampCache.print (msg);
    msg << BOLDCYAN << "Amplitude Cache Status[LS]: " << RESET << std::endl;
    m_ampCache.printLS (msg);

    //msg << BOLDMAGENTA << "RawData Cache Status: " << RESET << std::endl;
    //m_rawCache.print (msg);
    LOG4CPLUS_INFO (m_logger, __func__ << msg.str() );
}

void bril::bcm1futcaprocessor::Application::enqueueComplete()
{
    unsigned int timeout_sec_nb4 = 5;
    unsigned int timeout_sec_LS = 60;
    std::stringstream msg;
    std::stringstream occLog;
    OccupancyHistogramVector t_occVec = m_occCache.get_completedNB4 (timeout_sec_nb4, occLog);
    OccupancyHistogramVectorLS t_occVecLS = m_occCache.get_completedLS (timeout_sec_LS, msg);

    if (!occLog.str().empty() )
    {
        LOG4CPLUS_INFO (m_logger, "Enqueue complete: " << occLog.str() );
        m_missingHistogramLog += occLog.str();
        occLog.str ("");
    }

    if (!t_occVec.empty() )
        m_occQueue.push (t_occVec);

    if (!t_occVecLS.empty() )
        m_occQueueLS.push (t_occVecLS);

    AmplitudeHistogramVector t_ampVec = m_ampCache.get_completedNB4 (timeout_sec_nb4, msg);
    AmplitudeHistogramVectorLS t_ampVecLS = m_ampCache.get_completedLS (timeout_sec_LS, msg);

    if (t_ampVec.size() )
        m_ampQueue.push (t_ampVec);

    if (t_ampVecLS.size() )
        m_ampQueueLS.push (t_ampVecLS);

    RawHistogramVector t_rawVec = m_rawCache.get_completedNB4 (timeout_sec_nb4, msg);
    //this should just go out of scope
    RawHistogramVectorLS t_rawVecLS = m_rawCache.get_completedLS (timeout_sec_LS, msg);

    if (t_rawVec.size() )
        m_rawQueue.push (t_rawVec);

    if (msg.str().size() )
        LOG4CPLUS_INFO (m_logger, msg.str() );

    msg.str ("");
    msg << std::endl;

    if (t_occVec.size() ) msg << BOLDYELLOW << "Drained " << t_occVec.size() << " NB4s from Occupancy Cache!" << RESET << std::endl;

    if (t_ampVec.size() ) msg << BOLDCYAN << "Drained " << t_ampVec.size() << " NB4s from Amplitude Cache!" << RESET << std::endl;

    if (t_rawVec.size() ) msg << BOLDMAGENTA << "Drained " << t_rawVec.size() << " NB4s from RawData Cache!" << RESET << std::endl;

    if (t_occVec.size() || t_ampVec.size() || t_rawVec.size() )
    {
        //this->printCacheStatus();
        LOG4CPLUS_DEBUG (m_logger, msg.str() );
    }
}

void bril::bcm1futcaprocessor::Application::setupCharts()
{
    //Vector with Colors
    std::vector<std::string> t_colors = {"#000000", "#FFFF00", "#1CE6FF", "#FF34FF", "#FF4A46", "#008941", "#006FA6", "#A30059",
                                         "#FFDBE5", "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
                                         "#5A0007", "#809693", "#FEFFE6", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
                                         "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FFAA92", "#FF90C9", "#B903AA", "#D16100",
                                         "#DDEFFF", "#000035", "#7B4F4B", "#A1C299", "#300018", "#0AA6D8", "#013349", "#00846F",
                                         "#372101", "#FFB500", "#C2FFED", "#A079BF", "#CC0744", "#C0B9B2", "#C2FF99", "#001E09",
                                         "#00489C", "#6F0062", "#0CBD66", "#EEC3FF", "#456D75", "#B77B68", "#7A87A1", "#788D66",
                                         "#885578", "#FAD09F", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",

                                         "#34362D", "#B4A8BD", "#00A6AA", "#452C2C", "#636375", "#A3C8C9", "#FF913F", "#938A81",
                                         "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#C8A1A1", "#1E6E00",
                                         "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
                                         "#549E79", "#FFF69F", "#201625", "#72418F", "#BC23FF", "#99ADC0", "#3A2465", "#922329",
                                         "#5B4534", "#FDE8DC", "#404E55", "#0089A3", "#CB7E98", "#A4E804", "#324E72", "#6A3A4C",
                                         "#83AB58", "#001C1E", "#D1F7CE", "#004B28", "#C8D0F6", "#A3A489", "#806C66", "#222800",
                                         "#BF5650", "#E83000", "#66796D", "#DA007C", "#FF1A59", "#8ADBB4", "#1E0200", "#5B4E51",
                                         "#C895C5", "#320033", "#FF6832", "#66E1D3", "#CFCDAC", "#D0AC94", "#7ED379"
                                        };

    std::vector<std::string> t_colors_overview = {"#e6194b", "#3cb44b", "#ffe119", "#0082c8", "#f58231", "#911eb4", "#46f0f0", "#f032e6", "#d2f53c", "#fabebe", "#800000", "#008080", "#000000"};

    //property map for per channel data
    bril::webutils::WebChart::property_map t_ChartMapChannel;
    bril::webutils::WebChart::property_map t_ChartMapAlbedo;

    std::string callbackRaw = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestRawData";
    std::string callbackOverview = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestOverviewData";
    std::string callbackChannel = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestChannelData";
    std::string callbackAlbedo = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/requestAlbedoData";

    for (size_t chan = 0; chan < m_channelInfo.getChannelVector().size(); chan++)
    {
        size_t color = chan;
        size_t color_uncorr = chan * 2;
        size_t color_corr = chan * 2 + 1;
        std::stringstream ss;
        ss << "Channel_" << m_channelInfo.getChannelVector().at (chan);
        t_ChartMapChannel[ss.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color) );
        std::stringstream albedo_corr;
        albedo_corr << "Channel_" << m_channelInfo.getChannelVector().at (chan) << "_corrected";
        std::stringstream albedo_uncorr;
        albedo_uncorr << "Channel_" << m_channelInfo.getChannelVector().at (chan) << "_uncorrected";
        t_ChartMapAlbedo[albedo_corr.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color_corr) );
        t_ChartMapAlbedo[albedo_uncorr.str()] = bril::webutils::WebChart::series_properties (t_colors.at (color_uncorr) );
    }

    //InputCharts
    m_occupancyChart100 = new bril::webutils::WebChart ("chart_occupancy100",
            callbackRaw,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Occupancy Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_occupancyChart101 = new bril::webutils::WebChart ("chart_occupancy101",
            callbackRaw,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Occupancy Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_amplitudeChart100 = new bril::webutils::WebChart ("chart_amplitude100",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Amplitude Data [LS]' }, tooltip: { enable : false }, xAxis: {title: {text: 'ADC', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_amplitudeChart101 = new bril::webutils::WebChart ("chart_amplitude101",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Amplitude Data [LS]' }, tooltip: { enable : false }, xAxis: {title: {text: 'ADC', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_rawChart100 = new bril::webutils::WebChart ("chart_rawdata100",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Raw Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_rawChart101 = new bril::webutils::WebChart ("chart_rawdata101",
            callbackRaw,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Raw Data [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX*6]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    //Agghist, Colhist and Bkghist
    m_agghistChart100 = new bril::webutils::WebChart ("chart_agghist100",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Agghist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_agghistChart101 = new bril::webutils::WebChart ("chart_agghist101",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Agghist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_colhistChart100 = new bril::webutils::WebChart ("chart_colhist100",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Colhist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_colhistChart101 = new bril::webutils::WebChart ("chart_colhist101",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Colhist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: { min: 0, title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_bkghistChart100 = new bril::webutils::WebChart ("chart_bkghst100",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Bkgd Hist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_bkghistChart101 = new bril::webutils::WebChart ("chart_bkghst101",
            callbackChannel,
            "type: 'column', zoomType: 'xy', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Bkgd Hist [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'Bin [BX]', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Counts', useHTML: true } }", //other options
            t_ChartMapChannel);

    //property map for Lumi data
    bril::webutils::WebChart::property_map t_ChartMapLumi;
    t_ChartMapLumi["lumi"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapLumi["lumi_raw"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );
    t_ChartMapLumi["lumi_Si"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (2) );
    t_ChartMapLumi["lumi_raw_Si"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (3) );
    t_ChartMapLumi["lumi_pCVD"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (4) );
    t_ChartMapLumi["lumi_raw_pCVD"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (5) );

    //Lumi Charts
    m_lumiBXChart100 = new bril::webutils::WebChart ("chart_lumiBX100",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapLumi);
    m_lumiBXChart101 = new bril::webutils::WebChart ("chart_lumiBX101",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapLumi);

    m_lumiChart100 = new bril::webutils::WebChart ( "chart_lumi100",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapLumi );
    m_lumiChart101 = new bril::webutils::WebChart ( "chart_lumi101",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapLumi );

    // per channel lumi
    m_lumiBXChannelChart100 = new bril::webutils::WebChart ("chart_lumiBXChannel100",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_lumiBXChannelChart101 = new bril::webutils::WebChart ("chart_lumiBXChannel101",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Luminosity per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Lumi', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_lumiChannelChart100 = new bril::webutils::WebChart ( "chart_lumiChannel100",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );
    m_lumiChannelChart101 = new bril::webutils::WebChart ( "chart_lumiChannel101",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Luminosity per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Luminosity [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );

    // raw lumi
    m_rawlumiBXChannelChart100 = new bril::webutils::WebChart ("chart_rawlumiBXChannel100",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Mu per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Mu', useHTML: true } }", //other options
            t_ChartMapChannel);
    m_rawlumiBXChannelChart101 = new bril::webutils::WebChart ("chart_rawlumiBXChannel101",
            callbackChannel,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Mu per Channel per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'Mu', useHTML: true } }", //other options
            t_ChartMapChannel);

    m_rawlumiChannelChart100 = new bril::webutils::WebChart ( "chart_rawlumiChannel100",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Mu per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Mu [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );
    m_rawlumiChannelChart101 = new bril::webutils::WebChart ( "chart_rawlumiChannel101",
            callbackChannel,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Mu per Channel' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Mu [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapChannel );

    //property map for BKG data
    bril::webutils::WebChart::property_map t_ChartMapBkg;
    t_ChartMapBkg["bkg1"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapBkg["bkg2"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );
    t_ChartMapBkg["bkg1_nc"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (2) );
    t_ChartMapBkg["bkg2_nc"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (3) );

    bril::webutils::WebChart::property_map t_ChartMapBkgBX;
    t_ChartMapBkgBX["bkg1"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (0) );
    t_ChartMapBkgBX["bkg2"] = bril::webutils::WebChart::series_properties (t_colors_overview.at (1) );

    //BKG Charts
    m_bkgBXChart100 = new bril::webutils::WebChart ("chart_bkgBX100",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Background per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'BKG', useHTML: true } }", //other options
            t_ChartMapBkgBX);
    m_bkgBXChart101 = new bril::webutils::WebChart ("chart_bkgBX101",
            callbackOverview,
            "type: 'column', zoomType: 'x', animation: 'false'",
            "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0, shadow: false, animation: false } }, title: { text: 'Background per BX [NB4]' }, tooltip: { enable : false }, xAxis: {title: {text: 'BX', useHTML: true}}, yAxis: {  min: 0,  title: { text: 'BKG', useHTML: true } }", //other options
            t_ChartMapBkgBX);

    m_bkgChart100 = new bril::webutils::WebChart ( "chart_bkg100",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Background' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Background [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapBkg );
    m_bkgChart101 = new bril::webutils::WebChart ( "chart_bkg101",
            callbackOverview,
            "type: 'line', zoomType: 'xy', animation: true",
            "plotOptions: { line: { animation: false } }, title: { text: 'Background' }, xAxis: { type: 'datetime' }, yAxis:{ min: 0, title: {text: 'Background [NB4]', useHTML: true } }", //the datetime is necessary to correctly format timestamps
            t_ChartMapBkg );

    //albedo charts
    m_albedoChart100 = new bril::webutils::WebChart ( "chart_albedo100",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Queue Algo 100' }, tooltip: { enable : false }", t_ChartMapAlbedo );
    m_albedoChart101 = new bril::webutils::WebChart ( "chart_albedo101",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Queue Algo 101' }, tooltip: { enable : false }", t_ChartMapAlbedo );
    m_fractionChart100 = new bril::webutils::WebChart ( "chart_fraction100",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Fraction Algo 100' }, tooltip: { enable : false }", t_ChartMapChannel );
    m_fractionChart101 = new bril::webutils::WebChart ( "chart_fraction101",
            callbackAlbedo,
            "type: 'line', zoomType: 'xy', animation: false",
            "plotOptions: { line: {step:'center',marker: { radius: 0 }, animation: false}},"
            //  "plotOptions: { column: { pointPadding: 0, borderWidth: 0, groupPadding: 0.1, shadow: false, animation: false, states: { hover: { enabled: false } } } },"
            "title: { text: 'Albedo Fraction Algo 101' }, tooltip: { enable : false }", t_ChartMapChannel );

}

void bril::bcm1futcaprocessor::Application::setupAlgorithms()
{
    if (m_aggregateAlgorithm != NULL)
        delete m_aggregateAlgorithm;

    m_aggregateAlgorithm = AlgorithmFactory::aggregator (m_aggregateProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Aggrgator Algorithm " << RESET);

    if (m_backgroundAlgorithm)
        delete m_backgroundAlgorithm;

    m_backgroundAlgorithm = AlgorithmFactory::background (m_backgroundProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Background Algorithm " << RESET);

    if (m_lumiAlgorithm)
        delete m_lumiAlgorithm;

    m_lumiAlgorithm = AlgorithmFactory::lumi (m_lumiProperties, &m_channelInfo, m_memPoolFactory, m_memPool);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Luminosity Algorithm " << RESET);

    if (m_albedoAlgorithm)
        delete m_albedoAlgorithm;

    m_albedoAlgorithm = AlgorithmFactory::albedo (m_albedoProperties, &m_channelInfo);
    LOG4CPLUS_INFO (m_logger, BOLDBLUE << "Enabling Albedo Algorithm " << RESET);
}

void bril::bcm1futcaprocessor::Application::setupMonitoring()
{
    //LOG4CPLUS_DEBUG( getApplicationLogger(), __func__ );
    std::string monurn = createQualifiedInfoSpace ( "bcm1futcaprocessorMon" ).toString();
    m_monInfospace = xdata::getInfoSpaceFactory()->get ( monurn );
    const int nvars = 12;
    const char* names[nvars] = { "beammode", "fill", "run", "ls", "nb", "timestamp", "background1", "background2", "luminosityscvd", "luminositypcvd", "luminositysi", "channelrates" };
    xdata::Serializable* vars[nvars] = { &m_monVariables.m_monBeammode, &m_monVariables.m_monFill, &m_monVariables.m_monRun, &m_monVariables.m_monLs, &m_monVariables.m_monNb, &m_monVariables.m_monTimestamp, &m_monVariables.m_bg1Plus, &m_monVariables.m_bg2Minus, &m_monVariables.m_lumiAvg[0], &m_monVariables.m_lumiAvg[1], &m_monVariables.m_lumiAvg[2], &m_monVariables.m_totalChannelRate };

    for ( int i = 0; i != nvars; ++i )
    {
        m_monVariables.m_monVarlist.push_back ( names[i] );
        m_monVariables.m_monVars.push_back ( vars[i] );
        m_monInfospace->fireItemAvailable ( names[i], vars[i] );
    }
}

////////////////////////////////////////////////////////////////////////////////
//WORKLOOPS
////////////////////////////////////////////////////////////////////////////////
bool bril::bcm1futcaprocessor::Application::inputJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //m_waitMessage.wait();

        while (m_inputQueue.elements() )
        {
            //LOG4CPLUS_INFO (m_logger, RED << "Input QUEUE size: " << m_inputQueue.elements() << RESET);
            std::pair<std::string, toolbox::mem::Reference*> pair = m_inputQueue.pop();
            toolbox::mem::AutoReference t_refGuard (pair.second); // guarantees ref is release when refguard goes out of scope (like mutex lock_guard)

            //RefDestroyer rd (pair.second);

            //get the input header so I can save the last location
            interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (pair.second->getDataLocation() );

            if (pair.first == interface::bril::bcm1futcaocchistT::topicname() )
            {
                m_occCache.add (pair.second, pair.first);
                m_lastOccHeader = *t_inHeader;
            }

            if (pair.first == interface::bril::bcm1futcaamphistT::topicname() )
            {
                m_ampCache.add (pair.second, pair.first);
                m_lastAmpHeader = *t_inHeader;
            }

            if (pair.first == interface::bril::bcm1futcarawdataT::topicname() )
            {
                m_rawCache.add (pair.second, pair.first);
                m_lastRawHeader = *t_inHeader;
            }

            if (pair.first == interface::bril::beamT::topicname() )
                this->onMessageTopicBeam (pair.second);

            if (pair.first == interface::bril::vdmflagT::topicname() )
                this->onMessageTopicVdmFlag (pair.second);

            if (pair.first == interface::bril::vdmscanT::topicname() )
                this->onMessageTopicVdmScan (pair.second);
        }

        //TODO: this maybe inside the while loop??
        //NTS: it's fine to do it here since enqueueComplete puts a vector in the squeue
        this->enqueueComplete();
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " << e.what() << RESET );
        notifyQualified ( "error", e );
    }

    return true;
}

bool bril::bcm1futcaprocessor::Application::occProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //m_waitOccupancy.timedwait (1, 0); //maybe timedwait if we want a way out every now and then
        //m_waitOccupancy.wait(); //maybe timedwait if we want a way out every now and then

        //first, pop whatever is in the queue
        //size_t currentqueuesize = m_occQueue.elements();
        //std::cout << RED << "QUEUESIZE: " << currentqueuesize << RESET << std::endl;

        //if (currentqueuesize == 0) return true;
        if (m_occQueue.elements() )
        {
            OccupancyHistogramVector t_Vec = m_occQueue.pop();
            this->doTimeAlignment (t_Vec);

            if (m_webCharts)
            {
                this->fillChartData<uint16_t, s_nBinOcc> (m_occupancyChart100, m_occ_data100, t_Vec, 100);
                this->fillChartData<uint16_t, s_nBinOcc> (m_occupancyChart101, m_occ_data101, t_Vec, 101);
            }

            m_lumiAlgorithm->compute (t_Vec, m_beamInfo, &m_publishQueue, &m_albedoQueue, &m_monVariables);
            m_aggregateAlgorithm->compute (t_Vec, m_beamInfo, &m_publishQueue, &m_monVariables);
            m_backgroundAlgorithm->compute (t_Vec, m_beamInfo, &m_publishQueue, &m_monVariables);
            m_monInfospace->fireItemGroupChanged (m_monVariables.m_monVarlist, this);
        }

        //now check if I have to process an aggregated LS histogram
        if (m_occQueueLS.elements() )
        {
            OccupancyHistogramVectorLS t_Vec = m_occQueueLS.pop();
            this->doTimeAlignment (t_Vec);
            m_aggregateAlgorithm->compute (t_Vec, m_beamInfo, &m_publishQueue, &m_monVariables);
            LOG4CPLUS_DEBUG (m_logger, BOLDBLUE << __func__ << " occupancy workloop popped a vector containing " << t_Vec.size() << " LS from occupancy queue" << RESET);
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " << e.what() << RESET );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::ampProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //m_waitAmplitude.wait(); //maybe timedwait if we want a way out every now and then
        //m_waitAmplitude.timedwait (1, 0); //maybe timedwait if we want a way out every now and then

        //first, pop whatever is in the queue
        //size_t currentqueuesize = m_ampQueue.elements();


        if (m_ampQueue.elements() )
            AmplitudeHistogramVector t_Vec = m_ampQueue.pop();


        //now check if I have to process an aggregated LS histogram
        if (m_ampQueueLS.elements() )
        {
            AmplitudeHistogramVectorLS t_Vec = m_ampQueueLS.pop();

            if (m_webCharts)
            {
                this->fillChartData<uint32_t, s_nBinAmp> (m_amplitudeChart100, m_amp_data100, t_Vec, 100);
                this->fillChartData<uint32_t, s_nBinAmp> (m_amplitudeChart101, m_amp_data101, t_Vec, 101);
            }
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::rawProcessJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //m_waitRawdata.wait (); //maybe timedwait if we want a way out every now and then
        //m_waitRawdata.timedwait (1, 0); //maybe timedwait if we want a way out every now and then

        //first, pop whatever is in the queue

        if (m_rawQueue.elements() )
        {
            RawHistogramVector t_Vec = m_rawQueue.pop();

            if (m_webCharts)
            {
                this->fillChartData<uint8_t, s_nBinRaw> (m_rawChart100, m_raw_data100, t_Vec, 100);
                this->fillChartData<uint8_t, s_nBinRaw> (m_rawChart101, m_raw_data101, t_Vec, 101);
            }
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

bool bril::bcm1futcaprocessor::Application::publishJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        //first, pop whatever is in the queue
        //size_t currentqueuesize = m_publishQueue.elements();
        //std::cout << BOLDBLUE << "PUBLISH QUEUE SIZE: " << currentqueuesize << RESET << std::endl;

        while (m_publishQueue.elements() )
            //if (m_publishQueue.elements() )
        {
            std::pair<std::string, toolbox::mem::Reference*> t_pair = m_publishQueue.pop();

            if (m_webCharts)
            {
                if (t_pair.first.find (interface::bril::bcm1futca_lumiT::topicname() ) != std::string::npos ||
                        t_pair.first.find (interface::bril::bcm1futca_pcvdlumiT::topicname() ) != std::string::npos ||
                        t_pair.first.find (interface::bril::bcm1futca_silumiT::topicname() ) != std::string::npos)
                    this->fillChartDataLumi (t_pair);

                else if (t_pair.first == interface::bril::bcm1futca_backgroundT::topicname() )
                    this->fillChartDataBKG (t_pair);

                else if (t_pair.first == interface::bril::bcm1futca_agg_histT::topicname() ||
                         t_pair.first == interface::bril::bcm1futca_col_histT::topicname() ||
                         t_pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
                    this->fillChartDataHist (t_pair);
            }

            //now still need to publish
            //std::cout << BLUE << "Publishing topic: " << t_pair.first << RESET << std::endl;
            this->publish (t_pair);
            //t_pair.second->release();
            //t_pair.second = 0;
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

void bril::bcm1futcaprocessor::Application::publish (std::pair<std::string, toolbox::mem::Reference*> pair)
{
    std::string payloaddict;

    if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_agg_histT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_col_histT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_bkg_histT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_bkg12T::topicname() )
        payloaddict = interface::bril::bcm1futca_bkg12T::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_backgroundT::topicname() )
        payloaddict = interface::bril::bcm1futca_backgroundT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_lumiT::topicname() )
        payloaddict = interface::bril::bcm1futca_lumiT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_pcvdlumiT::topicname() )
        payloaddict = interface::bril::bcm1futca_pcvdlumiT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_silumiT::topicname() )
        payloaddict = interface::bril::bcm1futca_silumiT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_occ_section_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_occ_section_histT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_amp_analysisT::topicname() )
        payloaddict = interface::bril::bcm1futca_amp_analysisT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_amp_section_histT::topicname() )
        payloaddict = interface::bril::bcm1futca_amp_section_histT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_baseline_tp_infoT::topicname() )
        payloaddict = interface::bril::bcm1futca_baseline_tp_infoT::payloaddict();
    else if (pair.first == interface::bril::bcm1futca_ADC_peaksT::topicname() )
        payloaddict = interface::bril::bcm1futca_ADC_peaksT::payloaddict();
    //this is needed for the per channel publication or if the algo ID is appended to the topicname for overall lumi
    //if the algo ID is appended to the channel lumi this does not make a differecne as it is the same payloaddict
    else if (pair.first.find ("bcm1futca_lumi_") != std::string::npos)
        payloaddict = interface::bril::bcm1futca_lumiT::payloaddict();
    //this is true when algo ID is appended to the topicnames for VdM
    else if (pair.first.find ("bcm1futca_pcvdlumi_") != std::string::npos)
        payloaddict = interface::bril::bcm1futca_pcvdlumiT::payloaddict();
    //this is true when algo ID is appended to the topicnames for VdM
    else if (pair.first.find ("bcm1futca_silumi_") != std::string::npos)
        payloaddict = interface::bril::bcm1futca_silumiT::payloaddict();

    xdata::Properties plist;
    plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);

    if (!payloaddict.empty() )
        plist.setProperty ("PAYLOAD_DICT", payloaddict);

    std::string t_beamMode = m_beamInfo.getBeamMode();

    if (t_beamMode == "SETUP" || t_beamMode == "ABORT" || t_beamMode == "BEAM DUMP" || t_beamMode == "RAMP DOWN" || t_beamMode == "CYCLING" || t_beamMode == "RECOVERY" || t_beamMode == "NO BEAM")
        plist.setProperty ("NOSTORE", "1");

    //this is a workaround needed to send per channel data - if the topicname is bcm1futca_lumi_X where
    //X is the channel number, we need to treat it as standard lumi topic from the xml
    std::string t_topicname = pair.first;

    if (pair.first.find (interface::bril::bcm1futca_lumiT::topicname() ) != std::string::npos)
        t_topicname = interface::bril::bcm1futca_lumiT::topicname();
    else if (pair.first.find (interface::bril::bcm1futca_pcvdlumiT::topicname() ) != std::string::npos)
        t_topicname = interface::bril::bcm1futca_pcvdlumiT::topicname();
    else if (pair.first.find (interface::bril::bcm1futca_silumiT::topicname() ) != std::string::npos)
        t_topicname = interface::bril::bcm1futca_silumiT::topicname();

    auto topicIt = m_outTopicMap.find (t_topicname);

    if (topicIt != std::end (m_outTopicMap) )
    {
        for (auto busIt : topicIt->second)
        {
            toolbox::mem::Reference* ref = pair.second->duplicate();

            try
            {
                this->getEventingBus (busIt).publish (pair.first, ref, plist);
                //std::cout << "Publishing " << pair.first << " (" << topicIt->first << ") on " << busIt << std::endl;
            }
            catch (xcept::Exception& e)
            {
                LOG4CPLUS_ERROR (m_logger, RED << "Failed to publish topic " << pair.first << " on bus " << busIt << RESET);

                if (ref)
                {
                    ref->release();
                    ref = 0;
                }
            }
        }
    }
    else
        LOG4CPLUS_INFO (m_logger, RED << "Error, topic: " << pair.first << " is not found in the map of output topics - not publishing!" << RESET);

    //release the reference
    pair.second->release();
    pair.second = 0;
}

bool bril::bcm1futcaprocessor::Application::albedoJob (toolbox::task::WorkLoop* wl)
{
    try
    {
        if (m_albedoAlgorithm != nullptr)
        {
            m_albedoAlgorithm->compute (m_beamInfo, &m_albedoQueue, m_appLock, m_albedo_data, m_fraction_data);

            ////now pick apart the albedo data and fill the chart
            if (m_webCharts)
            {
                for (auto data : m_albedo_data)
                {
                    if (data.first.find ("_Algo_100") != std::string::npos )
                    {
                        std::string tmpstring = data.first;
                        tmpstring.erase (tmpstring.find ("_Algo_100"), 9 );
                        m_albedo_data100[tmpstring] = data.second;
                        //std::cout << tmpstring << std::endl;
                    }
                    else
                    {
                        std::string tmpstring = data.first;
                        tmpstring.erase (tmpstring.find ("_Algo_101"), 9 );
                        m_albedo_data101[tmpstring] = data.second;
                        //std::cout << tmpstring << std::endl;
                    }
                }

                m_albedoChart100->newDataReady();
                m_albedoChart101->newDataReady();

                for (auto data : m_fraction_data)
                {
                    if (data.first.find ("_Algo_100") != std::string::npos )
                    {
                        std::string tmpstring = data.first;
                        tmpstring.erase (tmpstring.find ("_Algo_100"), 9 );
                        m_fraction_data100[tmpstring] = data.second;
                    }
                    else
                    {
                        std::string tmpstring = data.first;
                        tmpstring.erase (tmpstring.find ("_Algo_101"), 9 );
                        m_fraction_data101[tmpstring] = data.second;
                    }
                }

                m_fractionChart100->newDataReady();
                m_fractionChart101->newDataReady();
            }
        }
        else
        {
            m_appLock.take();
            m_albedoQueue.clear();
            m_appLock.give();
        }
    }
    catch ( xcept::Exception& e )
    {
        LOG4CPLUS_ERROR ( getApplicationLogger(), BOLDRED << __func__ << ": Caught exception " <<  e.what() << RESET  );
        notifyQualified ( "error", e );
    }

    //usleep (2000);

    return true;
}

void bril::bcm1futcaprocessor::Application::requestRawData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Raw input data
    m_occupancyChart100->writeDataForQuery ( cgi, out, m_occ_data100 );
    m_occupancyChart101->writeDataForQuery ( cgi, out, m_occ_data101 );
    m_amplitudeChart100->writeDataForQuery ( cgi, out, m_amp_data100 );
    m_amplitudeChart101->writeDataForQuery ( cgi, out, m_amp_data101 );
    m_rawChart100->writeDataForQuery ( cgi, out, m_raw_data100 );
    m_rawChart101->writeDataForQuery ( cgi, out, m_raw_data101 );
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestOverviewData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Lumi charts
    m_lumiBXChart100->writeDataForQuery ( cgi, out, m_lumiBXdata100 );
    m_lumiChart100->writeDataForQuery (cgi, out, m_lumiData100);
    m_lumiBXChart101->writeDataForQuery ( cgi, out, m_lumiBXdata101 );
    m_lumiChart101->writeDataForQuery (cgi, out, m_lumiData101);
    //BKG charts
    m_bkgBXChart100->writeDataForQuery ( cgi, out, m_bkgBXdata100 );
    m_bkgBXChart101->writeDataForQuery ( cgi, out, m_bkgBXdata101 );
    m_bkgChart100->writeDataForQuery (cgi, out, m_bkgData100);
    m_bkgChart101->writeDataForQuery (cgi, out, m_bkgData101);
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestChannelData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Lumi charts
    m_lumiBXChannelChart100->writeDataForQuery ( cgi, out, m_lumiBXChanneldata100 );
    m_lumiChannelChart100->writeDataForQuery (cgi, out, m_lumiChannelData100);
    m_rawlumiBXChannelChart100->writeDataForQuery ( cgi, out, m_rawlumiBXChanneldata100 );
    m_rawlumiChannelChart100->writeDataForQuery (cgi, out, m_rawlumiChannelData100);

    m_lumiBXChannelChart101->writeDataForQuery ( cgi, out, m_lumiBXChanneldata101 );
    m_lumiChannelChart101->writeDataForQuery (cgi, out, m_lumiChannelData101);
    m_rawlumiBXChannelChart101->writeDataForQuery ( cgi, out, m_rawlumiBXChanneldata101 );
    m_rawlumiChannelChart101->writeDataForQuery (cgi, out, m_rawlumiChannelData101);

    m_agghistChart100->writeDataForQuery (cgi, out, m_agghist_data100);
    m_colhistChart100->writeDataForQuery (cgi, out, m_colhist_data100);
    m_bkghistChart100->writeDataForQuery (cgi, out, m_bkghist_data100);
    m_agghistChart101->writeDataForQuery (cgi, out, m_agghist_data101);
    m_colhistChart101->writeDataForQuery (cgi, out, m_colhist_data101);
    m_bkghistChart101->writeDataForQuery (cgi, out, m_bkghist_data101);
    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::requestAlbedoData ( xgi::Input* in, xgi::Output* out )
{
    m_appLock.take();
    cgicc::Cgicc cgi ( in ); // just do this
    bril::webutils::WebChart::dataHeader ( out ); // and this
    //Raw input data
    m_albedoChart100->writeDataForQuery ( cgi, out, m_albedo_data100 );
    m_albedoChart101->writeDataForQuery ( cgi, out, m_albedo_data101 );
    m_fractionChart100->writeDataForQuery ( cgi, out, m_fraction_data100 );
    m_fractionChart101->writeDataForQuery ( cgi, out, m_fraction_data101 );
    m_appLock.give();
}

template<typename DataT, size_t nBin> void bril::bcm1futcaprocessor::Application::fillChartData (webutils::WebChart* chart, std::map<std::string, std::vector<DataT>>& chartMap, std::vector<HistogramBuffer<DataT, nBin>> bufVec, unsigned int algo)
{
    //loop the vector of HistogramBuffers
    for (auto buf : bufVec)
    {
        std::string histtype = buf.getHistType();

        for (auto hist : buf.m_channelMap)
        {
            unsigned int t_chan;
            unsigned int t_algo;
            hist.first.decode (t_algo, t_chan);

            if (t_algo == algo)
            {
                std::stringstream ss;
                ss << "Channel_" << t_chan;

                m_appLock.take();

                std::vector<DataT> t_tmpVec (hist.second.cbegin(), hist.second.cend() );

                chartMap[ss.str()] = t_tmpVec;
                chart->newDataReady();

                m_appLock.give();
            }
        }
    }
}

void bril::bcm1futcaprocessor::Application::fillChartDataLumi (std::pair<std::string, toolbox::mem::Reference*> pair)
{
    size_t queulenght = 1500;
    //get the Datum Header from the reference
    float lumi_bx[interface::bril::MAX_NBX];

    float lumi_bx_raw[interface::bril::MAX_NBX];
    float lumi;// = 0;
    float lumi_raw;// = 0;

    memset (lumi_bx, 0, sizeof (lumi_bx) );
    memset (lumi_bx_raw, 0, sizeof (lumi_bx_raw) );

    interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (pair.second->getDataLocation() );
    unsigned int algoID = t_inHeader->getAlgoID();
    unsigned int channelID = t_inHeader->getChannelID();

    std::string sensorstring;

    if (pair.first.find (interface::bril::bcm1futca_lumiT::topicname() ) != std::string::npos)
    {
        sensorstring = "";
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_lumiT::payloaddict() );
        //decode the info
        //t_streamer.extract_field (&lumi_bx, "bx", t_inHeader->payloadanchor);
        //t_streamer.extract_field (&lumi_bx_raw, "bxraw", t_inHeader->payloadanchor);
        t_streamer.extract_field (lumi_bx, "bx", t_inHeader->payloadanchor);
        t_streamer.extract_field (lumi_bx_raw, "bxraw", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi, "avg", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi_raw, "avgraw", t_inHeader->payloadanchor);
    }
    else if (pair.first.find (interface::bril::bcm1futca_pcvdlumiT::topicname() ) != std::string::npos)
    {
        sensorstring = "_pCVD";
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_pcvdlumiT::payloaddict() );
        //decode the info
        t_streamer.extract_field (lumi_bx, "bx", t_inHeader->payloadanchor);
        t_streamer.extract_field (lumi_bx_raw, "bxraw", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi, "avg", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi_raw, "avgraw", t_inHeader->payloadanchor);
    }
    else if (pair.first.find (interface::bril::bcm1futca_silumiT::topicname() ) != std::string::npos)
    {
        sensorstring = "_Si";
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_silumiT::payloaddict() );
        //decode the info
        t_streamer.extract_field (lumi_bx, "bx", t_inHeader->payloadanchor);
        t_streamer.extract_field (lumi_bx_raw, "bxraw", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi, "avg", t_inHeader->payloadanchor);
        t_streamer.extract_field (&lumi_raw, "avgraw", t_inHeader->payloadanchor);
    }

    //create a compound data streamer to extrat the beam payload

    std::vector<float> t_tmpVec (lumi_bx, lumi_bx + interface::bril::MAX_NBX );

    std::vector<float> t_tmpVec_raw (lumi_bx_raw, lumi_bx_raw + interface::bril::MAX_NBX );

    toolbox::TimeVal t_time = toolbox::TimeVal::gettimeofday().sec() * 1000;

    m_appLock.take();

    if (channelID == 0)
    {
        std::stringstream ss;
        ss << "lumi" << sensorstring;
        std::stringstream ss_raw;
        ss_raw << "lumi_raw" << sensorstring;

        if (algoID == 100)
        {
            m_lumiBXdata100[ss.str()] = t_tmpVec;
            m_lumiBXdata100[ss_raw.str()] = t_tmpVec_raw;

            m_lumiData100[ss.str()].push_back (std::make_pair (t_time, lumi) );
            int overflow = m_lumiData100[ss.str()].size() - queulenght;

            if (overflow > 0) m_lumiData100[ss.str()].erase (m_lumiData100[ss.str()].begin(), m_lumiData100[ss.str()].begin() + overflow);

            m_lumiData100[ss_raw.str()].push_back (std::make_pair (t_time, lumi_raw) );
            overflow = m_lumiData100[ss_raw.str()].size() - queulenght;

            if (overflow > 0) m_lumiData100[ss_raw.str()].erase (m_lumiData100[ss_raw.str()].begin(), m_lumiData100[ss_raw.str()].begin() + overflow);


            m_lumiBXChart100->newDataReady();
            m_lumiChart100->newDataReady();
        }
        else if (algoID == 101)
        {
            m_lumiBXdata101[ss.str()] = t_tmpVec;
            m_lumiBXdata101[ss_raw.str()] = t_tmpVec_raw;

            m_lumiData101[ss.str()].push_back (std::make_pair (t_time, lumi) );
            int overflow = m_lumiData101[ss.str()].size() - queulenght;

            if (overflow > 0) m_lumiData101[ss.str()].erase (m_lumiData101[ss.str()].begin(), m_lumiData101[ss.str()].begin() + overflow);

            m_lumiData101[ss_raw.str()].push_back (std::make_pair (t_time, lumi_raw) );
            overflow = m_lumiData101[ss_raw.str()].size() - queulenght;

            if (overflow > 0) m_lumiData101[ss_raw.str()].erase (m_lumiData101[ss_raw.str()].begin(), m_lumiData101[ss_raw.str()].begin() + overflow);

            m_lumiBXChart101->newDataReady();
            m_lumiChart101->newDataReady();
        }
    }
    else
    {
        std::stringstream ss;
        ss << "Channel_" << channelID;

        if (algoID == 100)
        {
            // this is lumi
            m_lumiBXChanneldata100[ss.str()] = t_tmpVec;
            // this is mu
            m_rawlumiBXChanneldata100[ss.str()] = t_tmpVec_raw;

            // lumi
            m_lumiChannelData100[ss.str()].push_back (std::make_pair (t_time, lumi) );
            int overflow = m_lumiChannelData100[ss.str()].size() - queulenght;

            if (overflow > 0) m_lumiChannelData100[ss.str()].erase (m_lumiChannelData100[ss.str()].begin(), m_lumiChannelData100[ss.str()].begin() + overflow);

            // mu
            m_rawlumiChannelData100[ss.str()].push_back (std::make_pair (t_time, lumi_raw) );
            overflow = m_rawlumiChannelData100[ss.str()].size() - queulenght;

            if (overflow > 0) m_rawlumiChannelData100[ss.str()].erase (m_rawlumiChannelData100[ss.str()].begin(), m_rawlumiChannelData100[ss.str()].begin() + overflow);

            m_lumiBXChannelChart100->newDataReady();
            m_lumiChannelChart100->newDataReady();
            m_rawlumiBXChannelChart100->newDataReady();
            m_rawlumiChannelChart100->newDataReady();
        }
        else if (algoID == 101)
        {
            // this is lumi
            m_lumiBXChanneldata101[ss.str()] = t_tmpVec;
            // this is mu
            m_rawlumiBXChanneldata101[ss.str()] = t_tmpVec_raw;

            // lumi
            m_lumiChannelData101[ss.str()].push_back (std::make_pair (t_time, lumi) );
            int overflow = m_lumiChannelData101[ss.str()].size() - queulenght;

            if (overflow > 0) m_lumiChannelData101[ss.str()].erase (m_lumiChannelData101[ss.str()].begin(), m_lumiChannelData101[ss.str()].begin() + overflow);

            // mu
            m_rawlumiChannelData101[ss.str()].push_back (std::make_pair (t_time, lumi_raw) );
            overflow = m_rawlumiChannelData101[ss.str()].size() - queulenght;

            if (overflow > 0) m_rawlumiChannelData101[ss.str()].erase (m_rawlumiChannelData101[ss.str()].begin(), m_rawlumiChannelData101[ss.str()].begin() + overflow);

            m_lumiBXChannelChart101->newDataReady();
            m_lumiChannelChart101->newDataReady();
            m_rawlumiBXChannelChart101->newDataReady();
            m_rawlumiChannelChart101->newDataReady();
        }
    }

    m_appLock.give();
}

void bril::bcm1futcaprocessor::Application::fillChartDataBKG (std::pair<std::string, toolbox::mem::Reference*> pair)
{
    size_t queulenght = 1500;
    //get the Datum Header from the reference
    float bkg1_bx[interface::bril::MAX_NBX];
    float bkg2_bx[interface::bril::MAX_NBX];
    float bkg1;// = 0;
    float bkg1_nc;// = 0;
    float bkg2;// = 0;
    float bkg2_nc;// = 0;

    memset (bkg1_bx, 0, sizeof (bkg1_bx) );
    memset (bkg2_bx, 0, sizeof (bkg2_bx) );

    interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (pair.second->getDataLocation() );
    unsigned int algoID = t_inHeader->getAlgoID();
    unsigned int channelID = t_inHeader->getChannelID();

    if (channelID == 0)
    {
        //create a compound data streamer to extrat the beam payload
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_backgroundT::payloaddict() );
        //decode the info
        //t_streamer.extract_field (&bkg1_bx, "bkgd1hist", t_inHeader->payloadanchor);
        //t_streamer.extract_field (&bkg2_bx, "bkgd2hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (bkg1_bx, "bkgd1hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (bkg2_bx, "bkgd2hist", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg1, "bkgd1", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg1_nc, "bkgd1_nc", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg2, "bkgd2", t_inHeader->payloadanchor);
        t_streamer.extract_field (&bkg2_nc, "bkgd2_nc", t_inHeader->payloadanchor);

        std::vector<float> t_tmpVec1 (bkg1_bx, bkg1_bx + interface::bril::MAX_NBX );
        std::vector<float> t_tmpVec2 (bkg2_bx, bkg2_bx + interface::bril::MAX_NBX );

        toolbox::TimeVal t_time = toolbox::TimeVal::gettimeofday().sec() * 1000;

        std::stringstream ss1;
        ss1 << "bkg1";
        std::stringstream ss2;
        ss2 << "bkg2";
        std::stringstream ss1nc;
        ss1nc << "bkg1_nc";
        std::stringstream ss2nc;
        ss2nc << "bkg2_nc";

        m_appLock.take();

        if (algoID == 100)
        {
            m_bkgBXdata100[ss1.str()] = t_tmpVec1;
            m_bkgBXdata100[ss2.str()] = t_tmpVec2;

            m_bkgData100[ss1.str()].push_back (std::make_pair (t_time, bkg1) );
            int overflow = m_bkgData100[ss1.str()].size() - queulenght;

            if (overflow > 0) m_bkgData100[ss1.str()].erase (m_bkgData100[ss1.str()].begin(), m_bkgData100[ss1.str()].begin() + overflow);

            m_bkgData100[ss1nc.str()].push_back (std::make_pair (t_time, bkg1_nc) );
            overflow = m_bkgData100[ss1nc.str()].size() - queulenght;

            if (overflow > 0) m_bkgData100[ss1nc.str()].erase (m_bkgData100[ss1nc.str()].begin(), m_bkgData100[ss1nc.str()].begin() + overflow);

            m_bkgData100[ss2.str()].push_back (std::make_pair (t_time, bkg2) );
            overflow = m_bkgData100[ss2.str()].size() - queulenght;

            if (overflow > 0) m_bkgData100[ss2.str()].erase (m_bkgData100[ss2.str()].begin(), m_bkgData100[ss2.str()].begin() + overflow);

            m_bkgData100[ss2nc.str()].push_back (std::make_pair (t_time, bkg2_nc) );
            overflow = m_bkgData100[ss2nc.str()].size() - queulenght;

            if (overflow > 0) m_bkgData100[ss2nc.str()].erase (m_bkgData100[ss2nc.str()].begin(), m_bkgData100[ss2nc.str()].begin() + overflow);

            m_bkgBXChart100->newDataReady();
            m_bkgChart100->newDataReady();
        }
        else if (algoID == 101)
        {
            m_bkgBXdata101[ss1.str()] = t_tmpVec1;
            m_bkgBXdata101[ss2.str()] = t_tmpVec2;

            m_bkgData101[ss1.str()].push_back (std::make_pair (t_time, bkg1) );
            int overflow = m_bkgData101[ss1.str()].size() - queulenght;

            if (overflow > 0) m_bkgData101[ss1.str()].erase (m_bkgData101[ss1.str()].begin(), m_bkgData101[ss1.str()].begin() + overflow);

            m_bkgData101[ss1nc.str()].push_back (std::make_pair (t_time, bkg1_nc) );
            overflow = m_bkgData101[ss1nc.str()].size() - queulenght;

            if (overflow > 0) m_bkgData101[ss1nc.str()].erase (m_bkgData101[ss1nc.str()].begin(), m_bkgData101[ss1nc.str()].begin() + overflow);

            m_bkgData101[ss2.str()].push_back (std::make_pair (t_time, bkg2) );
            overflow = m_bkgData101[ss2.str()].size() - queulenght;

            if (overflow > 0) m_bkgData101[ss2.str()].erase (m_bkgData101[ss2.str()].begin(), m_bkgData101[ss2.str()].begin() + overflow);

            m_bkgData101[ss2nc.str()].push_back (std::make_pair (t_time, bkg2_nc) );
            overflow = m_bkgData101[ss2nc.str()].size() - queulenght;

            if (overflow > 0) m_bkgData101[ss2nc.str()].erase (m_bkgData101[ss2nc.str()].begin(), m_bkgData101[ss2nc.str()].begin() + overflow);

            m_bkgBXChart101->newDataReady();
            m_bkgChart101->newDataReady();
        }

        m_appLock.give();
    }
}

void bril::bcm1futcaprocessor::Application::fillChartDataHist (std::pair<std::string, toolbox::mem::Reference*> pair)
{
    //get the Datum Header from the reference
    uint16_t data[interface::bril::MAX_NBX];

    memset (data, 0, sizeof (data) );

    interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (pair.second->getDataLocation() );
    unsigned int algoID = t_inHeader->getAlgoID();
    unsigned int channelID = t_inHeader->getChannelID();

    //create a compound data streamer to extrat the beam payload
    if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
    {
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_agg_histT::payloaddict() );
        //t_streamer.extract_field (&data, "agghist", t_inHeader->payloadanchor);
        t_streamer.extract_field (data, "agghist", t_inHeader->payloadanchor);
    }
    else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
    {
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_col_histT::payloaddict() );
        t_streamer.extract_field (data, "colhist", t_inHeader->payloadanchor);
    }
    else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
    {
        interface::bril::CompoundDataStreamer t_streamer (interface::bril::bcm1futca_bkg_histT::payloaddict() );
        t_streamer.extract_field (data, "bkghist", t_inHeader->payloadanchor);
    }

    std::vector<uint16_t> t_tmpVec (data, data + interface::bril::MAX_NBX );

    std::stringstream ss;
    ss << "Channel_" << channelID;

    m_appLock.take();

    if (algoID == 100)
    {
        if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
        {
            m_agghist_data100[ss.str()] = t_tmpVec;
            m_agghistChart100->newDataReady();
        }
        else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
        {
            m_colhist_data100[ss.str()] = t_tmpVec;
            m_colhistChart100->newDataReady();
        }
        else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
        {
            m_bkghist_data100[ss.str()] = t_tmpVec;
            m_bkghistChart100->newDataReady();
        }
    }
    else if (algoID == 101)
    {
        if (pair.first == interface::bril::bcm1futca_agg_histT::topicname() )
        {
            m_agghist_data101[ss.str()] = t_tmpVec;
            m_agghistChart101->newDataReady();
        }
        else if (pair.first == interface::bril::bcm1futca_col_histT::topicname() )
        {
            m_colhist_data101[ss.str()] = t_tmpVec;
            m_colhistChart101->newDataReady();
        }
        else if (pair.first == interface::bril::bcm1futca_bkg_histT::topicname() )
        {
            m_bkghist_data101[ss.str()] = t_tmpVec;
            m_bkghistChart101->newDataReady();
        }
    }

    m_appLock.give();
}

template<typename DataT, size_t nBin> void bril::bcm1futcaprocessor::Application::doTimeAlignment (std::vector<HistogramBuffer<DataT, nBin>>& bufVec)
{
    //loop the vector of HistogramBuffers
    for (auto& buf : bufVec)
    {
        for (auto& hist : buf.m_channelMap)
        {
            unsigned int t_chan;
            unsigned int t_algo;
            hist.first.decode (t_algo, t_chan);
            int t_delay = m_channelInfo.getChannelDelay (hist.first);

            if (t_delay != 0)
                hist.second.shift (t_delay);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
//WEB CALLBACKS
////////////////////////////////////////////////////////////////////////////////
void bril::bcm1futcaprocessor::Application::createTabBar (/*xgi::Input* in,*/ xgi::Output* out, bril::bcm1futcaprocessor::Tab tab) 
{
    std::string t_urn = m_appDescriptor->getContextDescriptor()->getURL() + "/" + m_appDescriptor->getURN() + "/";
    //draw the xdaq header
    //this->getHTMLHeader (in, out);
    // Create the Title, Tab bar

    //Style this thing
    *out << "<link rel=\"stylesheet\" type=\"text/css\" href=\"/bril/bcm1futcaprocessor/html/stylesheet.css\" media=\"screen\" />";

    std::ostringstream cTabBarString;

    // switch to show the current tab
    switch (tab)
    {
        case Tab::OVERVIEW:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button active") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button") << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::CHANNELS:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button active") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::ALBEDO:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button active") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::INPUT:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button active") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::CONFIG:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button active") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button") << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::MONITORING:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button active")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;

        case Tab::BEAM:
            cTabBarString << a ("Overview").set ("href", t_urn + "OverviewPage").set ("class", "button") << a ("Channels").set ("href", t_urn + "ChannelPage").set ("class", "button") << a ("Albedo").set ("href", t_urn + "AlbedoPage").set ("class", "button") << a ("InputData").set ("href", t_urn + "InputDataPage").set ("class", "button") << a ("Configuration").set ("href", t_urn + "ConfigPage").set ("class", "button") << a ("Monitoring").set ("href", t_urn + "MonitoringPage").set ("class", "button")  << a ("Beam").set ("href", t_urn + "BeamPage").set ("class", "button active") << a ("BeamMode: " + m_beamMode).set ("class", "text");
            break;
    }


    *out << cgicc::div ().set ("class", "title").add (h2 (a ("BCM1F uTCA Processor" ).set ("href", t_urn + "Overview").set ("style", "float: left") ) ).add (img().set ("src", "/bril/bcm1futcaprocessor/html/bril.png" ).set ("alt", "brillogo").set ("height", "50").set ("style", "float: right") ) << std::endl;
    *out << cgicc::div (cTabBarString.str() ).set ("class", "tab") << std::endl;
}

void bril::bcm1futcaprocessor::Application::OverviewPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::OVERVIEW;
    bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><br/>";
    *out << "<div class=\"plots-contentleft\"><h3>Luminosity & Background Algo 100</h3><br/>";

    if (m_webCharts) m_lumiChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_lumiBXChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgBXChart100->writeChart (out);

    *out << "</div>";
    *out << "<div class=\"plots-contentright\"><h3>Luminosity & Background Algo 101</h3><br/>";

    if (m_webCharts) m_lumiChart101->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_lumiBXChart101->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgChart101->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_bkgBXChart101->writeChart (out);

    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::ChannelPage (xgi::Input* in, xgi::Output* out) 
{
    m_tab = Tab::CHANNELS;
    bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><br/>";
    *out << "<div class=\"plots-contentleft\"><h3>Luminosity & Hists Algo 100</h3><br/>";

    if (m_webCharts) m_lumiChannelChart100->writeChart (out);

    *out << "<br/>";

    //if (m_webCharts) m_lumiBXChannelChart100->writeChart (out);

    *out << "<br/>";

    //if (m_webCharts) m_agghistChart100->writeChart (out);

    *out << "<br/>";
    *out << "<div class=\"plots-contentleft\">";

    //if (m_webCharts) m_colhistChart100->writeChart (out);

    *out << "</div>";
    *out << "<div class=\"plots-contentright\">";

    //if (m_webCharts) m_bkghistChart100->writeChart (out);

    *out << "</div>";
    *out << "</div>";
    *out << "<div class=\"plots-contentright\"><h3>Luminosity & Hists Algo 101</h3><br/>";

    if (m_webCharts) m_lumiChannelChart101->writeChart (out);

    *out << "<br/>";

    //if (m_webCharts) m_lumiBXChannelChart101->writeChart (out);

    *out << "<br/>";

    //if (m_webCharts) m_agghistChart101->writeChart (out);

    *out << "<br/>";
    *out << "<div class=\"plots-contentleft\">";

    //if (m_webCharts) m_colhistChart101->writeChart (out);

    *out << "</div>";
    *out << "<div class=\"plots-contentright\">";

    //if (m_webCharts) m_bkghistChart101->writeChart (out);

    *out << "</div>";
    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::AlbedoPage (xgi::Input* in, xgi::Output* out) 
{
    m_tab = Tab::ALBEDO;
    bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><br/>";
    *out << "<div class=\"plots-contentleft\"><h3>Corrected & Uncorrected Algo 100</h3><br/>";

    if (m_webCharts) m_albedoChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_fractionChart100->writeChart (out);

    *out << "</div>";
    *out << "<div class=\"plots-contentright\"><h3>Corrected & Uncorrected Algo 101</h3><br/>";

    if (m_webCharts) m_albedoChart101->writeChart (out);

    *out << "<br/>";

    //*out << "<div class=\"plots-contentleft\">";
    if (m_webCharts) m_fractionChart101->writeChart (out);

    //*out << "</div>";
    //*out << "<div class=\"plots-contentright\">";
    //m_bkghistChart->writeChart (out);
    //*out << "</div>";
    *out << "</div></div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::InputDataPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::INPUT;
    bril::webutils::WebChart::pageHeader (out);
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><br/>";
    *out << "<div class=\"plots-contentleft\"><h3>Input Data Algo 100</h3><br/>";

    if (m_webCharts) m_occupancyChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_amplitudeChart100->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_rawChart100->writeChart (out);

    *out << "</div>";
    *out << "<div class=\"plots-contentright\"><h3>Input Data Algo 101</h3><br/>";

    if (m_webCharts) m_occupancyChart101->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_amplitudeChart101->writeChart (out);

    *out << "<br/>";

    if (m_webCharts) m_rawChart101->writeChart (out);

    *out << "</div>";
    *out << "</div><br/>";
    *out << a ("URL: " + m_appURL).set ("href", m_appURL).set ("class", "text") << std::endl;
}

void bril::bcm1futcaprocessor::Application::ConfigPage (xgi::Input* in, xgi::Output* out)
{
    std::string webchartstring = (m_webCharts) ? "1" : "0";
    std::stringstream timeout;
    timeout << m_timeout;
    m_tab = Tab::CONFIG;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Configuration Parameters</h3><br/>";
    *out << "<div class=\"plots-contentleft\">";
    *out << "<h3>Eventing Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" ><thead>";
    *out << tr().add (th ("") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";
    *out << tr().add (td ("") ).add (td ("timeout") ).add (td (timeout.str() ) );
    *out << tr().add (td ("") ).add (td ("webcharts") ).add (td (webchartstring) );
    *out << "</tbody><thead>";

    *out << tr().add (th ("IO") ).add (th ("Bus") ).add (th ("Topic") ) << "</thead><tbody>";

    for (size_t source = 0; source < m_dataSources.elements(); source++)
    {
        std::string t_bus = m_dataSources[source].getProperty ("bus");
        std::string t_topics = m_dataSources[source].getProperty ("topics");
        *out << tr().add (td ("Input") ).add (td (t_bus) ).add (td (t_topics) );
    }

    *out << "</tbody><thead>" << tr().add (th ("IO") ).add (th ("Topic") ).add (th ("Buses") ) << "</thead><tbody>";

    for (size_t sink = 0; sink < m_dataSinks.elements(); sink++)
    {
        std::string t_topic = m_dataSinks[sink].getProperty ("topic");
        std::string t_buses = m_dataSinks[sink].getProperty ("buses");
        *out << tr().add (td ("Output") ).add (td (t_topic) ).add (td (t_buses) );
    }

    *out << "</tbody></table><br/>";
    *out << "<h3>Algorithm Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" >";
    //*out << tr().add (th ("Lumi Algorithm") ).set ("colspan", "2" );
    *out << "<thead>" <<  tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_LumiProperties{"class", "calibtag", "bestAlgo", "commissioningmode"};

    for (auto prop : t_LumiProperties)
        *out << tr().add (td ("Lumi") ).add (td (prop) ).add (td (m_lumiProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_AggProperties{"class"};

    for (auto prop : t_AggProperties)
        *out << tr().add (td ("Aggregator") ).add (td (prop) ).add (td (m_aggregateProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_BkgProperties{"class", "bestAlgo"};

    for (auto prop : t_BkgProperties)
        *out << tr().add (td ("Background") ).add (td (prop) ).add (td (m_backgroundProperties.getProperty (prop) ) );

    *out << "</tbody><thead>" << tr().add (th ("Algorithm") ).add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";

    std::vector<std::string> t_AlbedoProperties{"class", "modelFilePath", "albedoQueueLength", "albedoCalculationTime"};

    for (auto prop : t_AlbedoProperties)
        *out << tr().add (td ("Albedo") ).add (td (prop) ).add (td (m_albedoProperties.getProperty (prop) ) );

    *out << "</tbody></table><br/>";
    *out << "</div>";
    *out << "<div class=\"plots-contentright\">";
    *out << "<h3>Channel Config</h3><br/>";
    *out << "<table class=\"xdaq-table\" >";
    *out << "<thead>" << tr().add (th ("Channel") ).add (th ("Algo") ).add (th ("delay") ).add (th ("uselumi") ).add (th ("usebkg") ).add (th ("sigmavis") ).add (th ("calibSBIL") ).add (th ("nonlinearity") ).add (th ("factor") ) << "</thead><tbody>";

    for (size_t channel = 0; channel < m_channelConfig.elements(); channel++)
    {
        std::string channelID = m_channelConfig[channel].getProperty ("channel");
        *out << tr().add (td (channelID) ).add (td ("100") ).add (td (m_channelConfig[channel].getProperty ("delay100") ) ).add (td (m_channelConfig[channel].getProperty ("uselumi") ) ).add (td (m_channelConfig[channel].getProperty ("usebkg") ) ).add (td (m_channelConfig[channel].getProperty ("sigmavis100") ) ).add (td (m_channelConfig[channel].getProperty ("calibSBIL100") ) ).add (td (m_channelConfig[channel].getProperty ("nonlinearity100") ) ).add (td (m_channelConfig[channel].getProperty ("factor100") ) );
        *out << tr().add (td (channelID) ).add (td ("101") ).add (td (m_channelConfig[channel].getProperty ("delay101") ) ).add (td (m_channelConfig[channel].getProperty ("uselumi") ) ).add (td (m_channelConfig[channel].getProperty ("usebkg") ) ).add (td (m_channelConfig[channel].getProperty ("sigmavis101") ) ).add (td (m_channelConfig[channel].getProperty ("calibSBIL101") ) ).add (td (m_channelConfig[channel].getProperty ("nonlinearity101") ) ).add (td (m_channelConfig[channel].getProperty ("factor101") ) );
    }

    *out << "</tbody></table><br/>";
    *out << "</div></div>";
}

void bril::bcm1futcaprocessor::Application::MonitoringPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::MONITORING;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Monitoring Parameters</h3><br/>";
    *out << "<table class=\"xdaq-table\" border=\"1\">";
    *out << "<thead>" << tr().add (th ("Parameter") ).add (th ("Value") ) << "</thead><tbody>";
    m_appLock.take();

    int index = 0;

    for (auto iVar : m_monVariables.m_monVarlist)
    {
        *out << tr().add (td (iVar) ).add (td (m_monVariables.m_monVars[index]->toString() ).set ("style", "word-break:break-all;") );
        index++;
    }

    m_appLock.give();
    *out << "</tbody></table>";
    *out << "</div>";
    *out << textarea (m_missingHistogramLog).set ("rows", "10").set ("columns", "100");
}

void bril::bcm1futcaprocessor::Application::BeamPage (xgi::Input* in, xgi::Output* out)
{
    m_tab = Tab::BEAM;
    this->createTabBar (out, m_tab);
    *out << "<div class=\"plots\"><h3>Beam Info</h3><br/>";
    *out << "<table class=\"xdaq-table\" border=\"1\">";
    m_appLock.take();
    std::string collstring = (m_beamInfo.m_beamPresent) ? "true" : "false";
    *out << tr().add (td ("MachineMode") ).add (td (m_beamInfo.m_machineMode) );
    *out << tr().add (td ("BeamMode") ).add (td (m_beamInfo.m_beamMode) );
    *out << tr().add (td ("Total Current Beam 1") ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_b1Itot) ) );
    *out << tr().add (td ("Total Current Beam 2") ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_b2Itot) ) );
    *out << tr().add (td ("Collidable Beams") ).add (td (collstring) );
    *out << "</table><br/>";

    *out << "<table class=\"xdaq-table\" >";

    *out << "<thead>" << tr().add (th ("BX") ).add (th ("B1 Intensity") ).add (th ("B2 Intensity") ).add (th ("B1 Config") ).add (th ("B2 Config") ).add (th ("Collision Mask") ).add (th ("BKG1 Mask") ).add (th ("BKG2 Mask") ) << "</thead><tbody>";

    for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
    {
        std::string colstring = (m_beamInfo.m_collidingMask.test (iBX) ) ? "1" : "0";
        std::string colclass = (m_beamInfo.m_collidingMask.test (iBX) ) ? "on" : "off";
        std::string bkg1string = (m_beamInfo.m_bkg1Mask.test (iBX) ) ? "1" : "0";
        std::string bkg1class = (m_beamInfo.m_bkg1Mask.test (iBX) ) ? "on" : "off";
        std::string bkg2string = (m_beamInfo.m_bkg2Mask.test (iBX) ) ? "1" : "0";
        std::string bkg2class = (m_beamInfo.m_bkg2Mask.test (iBX) ) ? "on" : "off";
        *out << tr().add (td (bril::bcm1futcaprocessor::convert (iBX) ) ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx1Intensity[iBX] ) ) )
             .add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx2Intensity[iBX] ) ) ).add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx1Conf[iBX] ) ) )
             .add (td (bril::bcm1futcaprocessor::convert (m_beamInfo.m_bx2Conf[iBX] ) ) ).add (td (colstring).set ("class", colclass) ).add (td (bkg1string ).set ("class", bkg1class) ).add (td (bkg2string).set ("class", bkg2class) );
    }

    m_appLock.give();
    *out << "</tbody></table>";
    *out << "</div>";
}
