#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"
#include "bril/bcm1futcaprocessor/Algorithms/ZeroCounting.h"
#include "bril/bcm1futcaprocessor/Algorithms/AggregateAlgorithm.h"
#include "bril/bcm1futcaprocessor/Algorithms/BackgroundAlgorithm.h"
#include "bril/bcm1futcaprocessor/Algorithms/AlbedoAlgorithm.h"

namespace bril {

    namespace bcm1futcaprocessor {

        //virtual default destructors for the various algorithms

        LumiAlgorithm::~LumiAlgorithm()
        {}

        AggregateAlgorithm::~AggregateAlgorithm()
        {}

        AmplitudeAlgorithm::~AmplitudeAlgorithm()
        {}

        BackgroundAlgorithm::~BackgroundAlgorithm()
        {}

        AlbedoAlgorithm::~AlbedoAlgorithm()
        {}

        //Algorithm Factory creation methods
        LumiAlgorithm* AlgorithmFactory::lumi (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
        {
            std::string name = props.getProperty ( "class" );

            if ( name == "ZeroCounting" )
                return new ZeroCounting ( props, channels, factory, pool );

            XCEPT_RAISE ( exception::AlgorithmException, "Lumi algorithm \"" + name + "\" is not available." );
            return 0;
        }
        AggregateAlgorithm* AlgorithmFactory::aggregator (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
        {
            std::string name = props.getProperty ( "class" );

            if ( name == "DefaultAggregator" )
                return new DefaultAggregator ( props, channels, factory, pool );

            XCEPT_RAISE ( exception::AlgorithmException, "Aggregate algorithm \"" + name + "\" is not available." );
            return 0;
        }

        AmplitudeAlgorithm* AlgorithmFactory::amplitude (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool  )
        {
            std::string name = props.getProperty ( "class" );

            //if ( name == "DefaultAmplitude" )
            //return new DefaultAmplitude ( props );

            //if ( name == "CalibrationAmplitude" )
            //return new CalibrationAmplitude();

            XCEPT_RAISE ( exception::AlgorithmException, "Amplitude algorithm \"" + name + "\" is not available." );
            return 0;
        }

        BackgroundAlgorithm* AlgorithmFactory::background (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool  )
        {
            std::string name = props.getProperty ( "class" );

            if ( name == "DefaultBackground" )
                return new DefaultBackground ( props, channels, factory, pool );

            XCEPT_RAISE ( exception::AlgorithmException, "Background algorithm \"" + name + "\" is not available." );

            return 0;
        }

        AlbedoAlgorithm* AlgorithmFactory::albedo (xdata::Properties& props, ChannelInfo* channels)
        {
            std::string name = props.getProperty ( "class" );

            if ( name == "DefaultAlbedo" )
                return new DefaultAlbedo ( props, channels);

            XCEPT_RAISE ( exception::AlgorithmException, "Albedo algorithm \"" + name + "\" is not available." );
            return 0;
        }
    } //namespace bcm1futcaprocessor
} //namespace bril
