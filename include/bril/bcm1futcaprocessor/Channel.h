#ifndef _bril_bcm1f_utcaprocessor_channel_h_
#define _bril_bcm1f_utcaprocessor_channel_h_

#include <algorithm>
#include <numeric>
#include <bitset>

#include "xdata/Properties.h"
#include "xdata/Vector.h"
#include "interface/bril/CommonDataFormat.h"
#include "bril/bcm1futcaprocessor/HistogramCache.h"

namespace bril {

    namespace bcm1futcaprocessor {

        enum class SensorType {sCVD, pCVD, Si, Disconnected, All};

        enum class SensorPosition {plus, minus, undefined};

        struct SensorIdentifier
        {
            SensorType m_type;
            unsigned int m_algoID;
            SensorIdentifier (SensorType type, unsigned int algo) :
                m_type (type),
                m_algoID (algo)
            {}
            bool operator < (const SensorIdentifier& rhs) const
            {
                return this->m_type < rhs.m_type || (this->m_type == rhs.m_type && this->m_algoID < rhs.m_algoID );
            }
        };
        struct Corrections
        {
            float m_sigmaVis;
            float m_calibSBIL;
            float m_nonLinearity;
            float m_factor;

            Corrections (float sigmavis, float calibsbil, float nonlinearity, float otherfactor) :
                m_sigmaVis (sigmavis),
                m_calibSBIL (calibsbil),
                m_nonLinearity (nonlinearity),
                m_factor (otherfactor)
            {}

            void print()
            {
                std::cout << "Sigma visible: " << m_sigmaVis << " nonlinearity: " << m_nonLinearity << " calib SBIL: " << m_calibSBIL << std::endl;
            }
        };

        struct AlbedoFraction
        {
            std::vector<float> m_fraction;
            //float m_fraction[interface::bril::MAX_NBX];
            float m_noise;

            AlbedoFraction() :
                m_fraction (interface::bril::MAX_NBX, 1.),
                m_noise (0)
            {
            }

            float getFraction (size_t iBX)
            {
                if (iBX < interface::bril::MAX_NBX)
                    return m_fraction.at (iBX);
                else return 1.;
            }

            float getNoise()
            {
                return m_noise;
            }
            void setFraction (size_t iBX, float value)
            {
                if (iBX < interface::bril::MAX_NBX)
                    m_fraction.at (iBX) = value;
            }

            void setNoise (float value)
            {
                m_noise = value;
            }
        };

        class ChannelInfo
        {
          private:
            std::vector<unsigned int> m_goodChannelList;
            const std::vector<unsigned int> m_allowedAlgoList = {100, 101};

            std::bitset<interface::bril::BCM1F_NCHANNELS> m_channelForLumi;
            std::bitset<interface::bril::BCM1F_NCHANNELS> m_channelForBkg;

            using ChannelDelayMap = std::map<HistogramIdentifier, int>;
            ChannelDelayMap m_delayMap;

            using CorrectionMap = std::map<HistogramIdentifier, Corrections>;
            CorrectionMap m_corrections;

            using AlbedoMap = std::map<HistogramIdentifier, AlbedoFraction>;
            AlbedoMap m_albedo;

            std::vector<HistogramIdentifier> m_histIdentifierVector;

          public:
            ChannelInfo() :
                m_goodChannelList (0)
            {
                //m_allowedAlgoList.push_back (100);
                //m_allowedAlgoList.push_back (101);
                m_goodChannelList.clear();
                m_channelForLumi.reset();
                m_channelForBkg.reset();
                m_delayMap.clear();
                m_corrections.clear();
                m_albedo.clear();
                m_histIdentifierVector.clear();
            }

            ChannelInfo& operator = (const ChannelInfo& rhs)
            {
                this->m_goodChannelList = rhs.m_goodChannelList;
                //this->m_allowedAlgoList = rhs.m_allowedAlgoList;
                this->m_channelForLumi = rhs.m_channelForLumi;
                this->m_channelForBkg = rhs.m_channelForBkg;
                this->m_delayMap = rhs.m_delayMap;
                this->m_corrections = rhs.m_corrections;
                this->m_albedo = rhs.m_albedo;
                this->m_histIdentifierVector = rhs.m_histIdentifierVector;
                return *this;
            }

            ~ChannelInfo();

            void setupChannels (xdata::Vector<xdata::Properties> channelConfig, std::stringstream& logstream)
            {

                logstream << BOLDGREEN << std::endl << "######################################################" << RESET << std::endl;
                logstream << BOLDGREEN << "Channel Configuration: " << RESET << std::endl;

                //first the input
                for (size_t channel = 0; channel < channelConfig.elements(); channel++)
                {
                    unsigned int t_channel = std::stoul (channelConfig[channel].getProperty ("channel") );
                    m_goodChannelList.push_back (t_channel );

                    bool t_useLumi = (channelConfig[channel].getProperty ("uselumi") == "true") ? true : false;

                    if (t_useLumi) m_channelForLumi.set (t_channel - 1);

                    bool t_useBkg = (channelConfig[channel].getProperty ("usebkg") == "true") ? true : false;

                    if (t_useBkg) m_channelForBkg.set (t_channel - 1);

                    int t_algo100delay = std::stoi (channelConfig[channel].getProperty ("delay100") );
                    m_delayMap.emplace (HistogramIdentifier (100, t_channel), t_algo100delay);
                    int t_algo101delay = std::stoi (channelConfig[channel].getProperty ("delay101") );
                    m_delayMap.emplace (HistogramIdentifier (101, t_channel), t_algo101delay);

                    //corrections on  a per channel / algo basis
                    float t_sigmaVis100 = std::stof (channelConfig[channel].getProperty ("sigmavis100") );
                    float t_calibSBIL100 = std::stof (channelConfig[channel].getProperty ("calibSBIL100") );
                    float t_nonLinearity100 = std::stof (channelConfig[channel].getProperty ("nonlinearity100") );
                    float t_factor100 = std::stof (channelConfig[channel].getProperty ("factor100") );

                    float t_sigmaVis101 = std::stof (channelConfig[channel].getProperty ("sigmavis101") );
                    float t_calibSBIL101 = std::stof (channelConfig[channel].getProperty ("calibSBIL101") );
                    float t_nonLinearity101 = std::stof (channelConfig[channel].getProperty ("nonlinearity101") );
                    float t_factor101 = std::stof (channelConfig[channel].getProperty ("factor101") );


                    //initialize the albedo fraction
                    m_albedo.emplace (HistogramIdentifier (100, t_channel), AlbedoFraction() );
                    m_albedo.emplace (HistogramIdentifier (101, t_channel), AlbedoFraction() );

                    m_corrections.emplace (HistogramIdentifier (100, t_channel), Corrections (t_sigmaVis100, t_calibSBIL100, t_nonLinearity100, t_factor100) );
                    m_corrections.emplace (HistogramIdentifier (101, t_channel), Corrections (t_sigmaVis101, t_calibSBIL101, t_nonLinearity101, t_factor101) );

                    //for convenience
                    m_histIdentifierVector.push_back (HistogramIdentifier (100, t_channel) );
                    m_histIdentifierVector.push_back (HistogramIdentifier (101, t_channel) );

                    logstream << BLUE << "Channel: " << GREEN << t_channel << BLUE << " - use Lumi: " << YELLOW << t_useLumi << BLUE << " use BGK: " << YELLOW << t_useBkg << BLUE << " algorithm 100 delay: " << CYAN << t_algo100delay << BLUE << " algorithm 101 delay: " << MAGENTA << t_algo101delay << RESET << std::endl;
                    logstream << CYAN << "Corrections: " ;
                    logstream << BLUE << " SigmaVis100: " << t_sigmaVis100 << " SigmaVis101: " << t_sigmaVis101;
                    logstream << MAGENTA << " CalibSBIL100: " << t_calibSBIL100 << " CalibSBIL101: " << t_calibSBIL101 ;
                    logstream << YELLOW << " Nonlinearity100: " << t_nonLinearity100 << " Nonlinearity101: " << t_nonLinearity101;
                    logstream << GREEN << " OtherFactor100: " << t_factor100 << " OtherFactor101: " << t_factor101 << RESET << std::endl;
                }

                logstream << BOLDGREEN << "######################################################" << RESET << std::endl;
            }

            std::vector<HistogramIdentifier> getHistIdentifierVector()
            {
                return m_histIdentifierVector;
            }

            std::vector<unsigned int> getChannelVector()
            {
                return m_goodChannelList;
            }

            std::vector<unsigned int> getAlgoVector()
            {
                return m_allowedAlgoList;
            }

            int getChannelDelay (HistogramIdentifier identifier)
            {
                auto hist_delay = m_delayMap.find (identifier);

                if (hist_delay != std::end (m_delayMap) )
                    return hist_delay->second;
                else
                    return 0;
            }

            float getAlbedoFraction (HistogramIdentifier identifier, size_t iBX)
            {
                auto albedo = m_albedo.find (identifier);
                float fraction = 1.;

                if (albedo != std::end (m_albedo) )
                    fraction = albedo->second.getFraction (iBX);

                return fraction;
            }

            float getNoise (HistogramIdentifier identifier)
            {
                auto albedo = m_albedo.find (identifier);
                float noise = 0.;

                if (albedo != std::end (m_albedo) )
                    noise = albedo->second.getNoise ();

                return noise;
            }

            float getAlbedoFraction (unsigned int algo, unsigned int channel, size_t iBX)
            {
                HistogramIdentifier t_identifier (algo, channel);
                auto albedo = m_albedo.find (t_identifier);

                if (albedo != std::end (m_albedo) )
                    return albedo->second.getFraction (iBX);

                else
                    return 1.;
            }

            void setAlbedoFraction (HistogramIdentifier identifier, size_t iBX, float value)
            {
                auto albedo = m_albedo.find (identifier);

                if (albedo != std::end (m_albedo) )
                    albedo->second.setFraction (iBX, value);
            }

            void setNoise (HistogramIdentifier identifier, float value)
            {
                auto albedo = m_albedo.find (identifier);

                if (albedo != std::end (m_albedo) )
                    albedo->second.setNoise (value);
            }

            float getFactor (HistogramIdentifier identifier)
            {
                auto factor = m_corrections.find (identifier);

                if (factor != std::end (m_corrections) ) return factor->second.m_factor;
                else return 0;
            }

            Corrections getCorrections (HistogramIdentifier identifier)
            {
                auto correction = m_corrections.find (identifier);

                if (correction != std::end (m_corrections) ) return correction->second;
                //if not found return just ones
                else return Corrections (1, 1, 1, 1);
            }

            bool useLumi (unsigned int channel)
            {
                return m_channelForLumi.test (channel - 1);
            }

            bool useBkg (unsigned int channel)
            {
                return m_channelForBkg.test (channel - 1);
            }

            SensorType getSensorType (unsigned int channel)
            {
                //to account for the fact that channels start counting from 1 - is this necessary?
                channel -= 1;

                if (channel == 0 || channel == 1 || channel == 4 || channel == 5 || channel == 8 || channel == 9
                        || channel == 12 || channel == 13 || channel == 16 || channel == 17 || channel == 24 || channel == 25
                        || channel == 28 || channel == 29 || channel == 32 || channel == 33 || channel == 36 || channel == 37
                        || channel == 40 || channel == 41) return SensorType::pCVD;

                else if (channel == 10 || channel == 11 || channel == 22 || channel == 23 || channel == 34 || channel == 35
                         || channel == 46 || channel == 47) return SensorType::sCVD;

                else if (channel == 3 || channel == 6 || channel == 15 || channel == 19 || channel == 21 || channel == 27
                         || channel == 31 || channel == 38 || channel == 42 || channel == 45) return SensorType::Si;

                else return SensorType::Disconnected;

            }

            SensorPosition getSensorPosition (unsigned int channel)
            {
                if (channel > 0 && channel < 25) return SensorPosition::plus;
                else if (channel > 24 && channel < 49) return SensorPosition::minus;
                else return SensorPosition::undefined;

            }


        };

    } //namespace bcm1futcaprocessor
} //namespace bril

#endif
