/* Copyright 2017 Imperial Collge London
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.

   Programmer :     Georg Auzinger
   Version :        1.0
   Date of creation:05/04/2018
   Support :        mail to : georg.auzinger@SPAMNOT.cern.ch
   FileName :       Application.h

   BCM1F UTCA Processor application for BRILDAQ framework
*/

#ifndef _bril_bcm1f_utcaprocessor_application_h_
#define _bril_bcm1f_utcaprocessor_application_h_

//std::inlcudes
#include <string>
#include <list>

//XDAQ includes
#include "xdaq/Application.h"

//xgi includes
#include "xgi/Utils.h"
#include "xgi/exception/Exception.h"
#include "xgi/Method.h"
#include "xgi/Output.h"
#include "xgi/exception/Exception.h"
#include "xgi/framework/UIManager.h"
#include "xgi/framework/Method.h"

//xdata includes
#include "xdata/ActionListener.h"
#include "xdata/InfoSpace.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/String.h"
#include "xdata/Vector.h"
#include "xdata/Float.h"
#include "xdata/Integer.h"
#include "xdata/UnsignedInteger.h"
#include "xdata/Boolean.h"
#include "xdata/TimeVal.h"
#include "xdata/Serializable.h"

//cgicc inlcudes
#include "cgicc/CgiDefs.h"
#include "cgicc/Cgicc.h"
#include "cgicc/HTTPHTMLHeader.h"
#include "cgicc/HTMLClasses.h"

//toolbox includs
#include "toolbox/task/WorkLoop.h"
#include "toolbox/task/WorkLoopFactory.h"
#include "toolbox/task/Action.h"
#include "toolbox/task/Guard.h"
#include "toolbox/task/TimerFactory.h"
#include "toolbox/task/Timer.h"
#include "toolbox/task/TimerListener.h"
#include "toolbox/mem/MemoryPoolFactory.h"
#include "toolbox/mem/Reference.h"
#include "toolbox/mem/HeapAllocator.h"
#include "toolbox/mem/AutoReference.h"
#include "toolbox/squeue.h"
#include "toolbox/EventDispatcher.h"
#include "toolbox/BSem.h"
#include "toolbox/Condition.h"
#include "toolbox/Runtime.h"
#include "toolbox/ShutdownEvent.h"

//other XDAQ includes
#include "eventing/api/Member.h"
#include "b2in/nub/exception/Exception.h"
#include "b2in/nub/Method.h"
#include "log4cplus/logger.h"

//BRIL specific includes
#include "interface/bril/BCM1FUTCATopics.hh"
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/TCDSTopics.hh"
#include "bril/webutils/WebUtils.h"
#include "bril/webutils/WebCharts.h"

//my custom includes
#include "bril/bcm1futcaprocessor/HistogramCache.h"
#include "bril/bcm1futcaprocessor/Channel.h"
#include "bril/bcm1futcaprocessor/Beam.h"
#include "bril/bcm1futcaprocessor/Utils/ConsoleColor.h"
#include "bril/bcm1futcaprocessor/Utils/Utils.h"
#include "bril/bcm1futcaprocessor/exception/Exception.h"
#include "bril/bcm1futcaprocessor/Definition.h"
#include "bril/bcm1futcaprocessor/Timer.h"

//Algorithm related stuff
#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"
#include "bril/bcm1futcaprocessor/Algorithms/ZeroCounting.h"
#include "bril/bcm1futcaprocessor/Algorithms/AggregateAlgorithm.h"
#include "bril/bcm1futcaprocessor/Algorithms/BackgroundAlgorithm.h"
#include "bril/bcm1futcaprocessor/Algorithms/AlbedoAlgorithm.h"

namespace bril {

    namespace bcm1futcaprocessor {

        enum class Tab {OVERVIEW, CHANNELS, ALBEDO, INPUT, CONFIG, MONITORING, BEAM};

        class Application : public xdaq::Application,
            public xgi::framework::UIManager,
            public eventing::api::Member,
            public xdata::ActionListener,
            public toolbox::ActionListener,
            public toolbox::EventDispatcher,
            public toolbox::task::TimerListener
        {

          public:
            XDAQ_INSTANTIATOR();
            // constructor
            Application (xdaq::ApplicationStub* s);
            // destructor
            ~Application ();
            // xgi(web) callback
            void Default (xgi::Input* in, xgi::Output* out);
            void OverviewPage (xgi::Input* in, xgi::Output* out);
            void ChannelPage (xgi::Input* in, xgi::Output* out);
            void AlbedoPage (xgi::Input* in, xgi::Output* out);
            void InputDataPage (xgi::Input* in, xgi::Output* out);
            void ConfigPage (xgi::Input* in, xgi::Output* out);
            void MonitoringPage (xgi::Input* in, xgi::Output* out);
            void BeamPage (xgi::Input* in, xgi::Output* out);
            // infospace event callback
            virtual void actionPerformed (xdata::Event& e);
            // toolbox event callback
            virtual void actionPerformed (toolbox::Event& e);
            // b2in message callback
            void onMessage (toolbox::mem::Reference* ref, xdata::Properties& plist);
            // timer callback
            virtual void timeExpired (toolbox::task::TimerEvent& e);

            /////////////////////////
            //Workloop related members
            /////////////////////////
            toolbox::task::WorkLoop* m_inputWorkloop;
            bool inputJob (toolbox::task::WorkLoop* wl);

            toolbox::task::WorkLoop* m_occWorkloop;
            bool occProcessJob (toolbox::task::WorkLoop* wl);

            toolbox::task::WorkLoop* m_ampWorkloop;
            bool ampProcessJob (toolbox::task::WorkLoop* wl);

            toolbox::task::WorkLoop* m_rawWorkloop;
            bool rawProcessJob (toolbox::task::WorkLoop* wl);

            toolbox::task::WorkLoop* m_publishWorkloop;
            bool publishJob (toolbox::task::WorkLoop* wl);

            toolbox::task::WorkLoop* m_albedoWorkloop;
            bool albedoJob (toolbox::task::WorkLoop* wl);

          private:

            /////////////////////////
            //GUI Stuff
            /////////////////////////
            Tab m_tab;


            /////////////////////////
            //DEBUG Stuff
            /////////////////////////
            std::ofstream filestream;
            double longesttime;


            /////////////////////////
            //Configuration parameters
            /////////////////////////

            //Topics for this application
            xdata::Vector<xdata::Properties> m_dataSources;
            xdata::Vector<xdata::Properties> m_dataSinks;
            xdata::Vector<xdata::Properties> m_channelConfig;
            xdata::UnsignedInteger m_timeout;

            xdata::Properties m_lumiProperties;
            xdata::Properties m_aggregateProperties;
            xdata::Properties m_backgroundProperties;
            xdata::Properties m_albedoProperties;

            xdata::Boolean m_webCharts;

            /////////////////////////
            //Channel configuration
            /////////////////////////

            ChannelInfo m_channelInfo;

            /////////////////////////
            //Input and Output topics
            /////////////////////////

            //map of bus vs set of topics for internal use
            //here map topic vs map of buses - the opposite of input
            using TopicName = std::string;
            using BusSet = std::set<std::string>;
            std::set<TopicName> m_inputTopics;
            std::map<TopicName, BusSet > m_outTopicMap;
            BusSet m_unreadyBuses;

            /////////////////////////
            //some information about this application
            /////////////////////////

            xdata::InfoSpace* m_appInfoSpace;
#ifdef x86_64_centos7
            const
#endif
            xdaq::ApplicationDescriptor* m_appDescriptor;
            std::string m_className;
            std::string m_appURL;
            // the logger since I am lazy
            log4cplus::Logger m_logger;
            std::string m_missingHistogramLog;

            /////////////////////////
            //Applock, memReferences etc.
            /////////////////////////

            toolbox::BSem m_appLock;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;

            /////////////////////////
            //condition variables to wake up the processing thread!
            /////////////////////////

            //not sure I want to use these!
            //toolbox::Condition m_waitMessage;
            //toolbox::Condition m_waitOccupancy;
            //toolbox::Condition m_waitAmplitude;
            //toolbox::Condition m_waitRawdata;

            /////////////////////////
            //containers for data
            /////////////////////////

            using OccupancyCache = HistogramCache<uint16_t, s_nBinOcc, interface::bril::bcm1futcaocchist>;
            OccupancyCache m_occCache;

            using AmplitudeCache = HistogramCache<uint32_t, s_nBinAmp, interface::bril::bcm1futcaamphist>;
            AmplitudeCache m_ampCache;

            using RawdataCache = HistogramCache<uint8_t, s_nBinRaw, interface::bril::bcm1futcarawdata>;
            RawdataCache m_rawCache;

            //beam data
            Beam m_beamInfo;
            std::string m_beamMode;
            bool m_VDMFlag;
            bool m_VDMIP5;

            //last received message header for timeout checks
            interface::bril::DatumHead m_lastOccHeader;
            interface::bril::DatumHead m_lastAmpHeader;
            interface::bril::DatumHead m_lastRawHeader;

            /////////////////////////
            //queues for data sharing
            /////////////////////////

            toolbox::squeue<OccupancyHistogramVector> m_occQueue;
            toolbox::squeue<AmplitudeHistogramVector> m_ampQueue;
            toolbox::squeue<RawHistogramVector> m_rawQueue;

            toolbox::squeue<OccupancyHistogramVectorLS> m_occQueueLS;
            toolbox::squeue<AmplitudeHistogramVectorLS> m_ampQueueLS;
            //toolbox::squeue<RawHistogramVectorLS> m_rawQueueLS;


            //using AlbedoQueue = std::deque < std::map < HistogramIdentifier, std::array<float, interface::bril::MAX_NBX>>>;
            AlbedoQueue m_albedoQueue;

            //for handling incoming messages on eventing to unblock onMessage
            toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>> m_inputQueue;
            //for publishing output histograms
            toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>> m_publishQueue;

            /////////////////////////
            //Webcharts
            /////////////////////////

            //input data
            webutils::WebChart* m_occupancyChart100;
            webutils::WebChart* m_occupancyChart101;
            std::map<std::string, std::vector<uint16_t>> m_occ_data100;
            std::map<std::string, std::vector<uint16_t>> m_occ_data101;
            webutils::WebChart* m_amplitudeChart100;
            webutils::WebChart* m_amplitudeChart101;
            std::map<std::string, std::vector<uint32_t>> m_amp_data100;
            std::map<std::string, std::vector<uint32_t>> m_amp_data101;
            webutils::WebChart* m_rawChart100;
            webutils::WebChart* m_rawChart101;
            std::map<std::string, std::vector<uint8_t>> m_raw_data100;
            std::map<std::string, std::vector<uint8_t>> m_raw_data101;

            //Lumi Stuff
            webutils::WebChart* m_lumiBXChart100;
            std::map<std::string, std::vector<float>> m_lumiBXdata100;
            webutils::WebChart* m_lumiBXChart101;
            std::map<std::string, std::vector<float>> m_lumiBXdata101;

            webutils::WebChart* m_lumiChart100;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_lumiData100;
            webutils::WebChart* m_lumiChart101;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_lumiData101;

            webutils::WebChart* m_lumiBXChannelChart100;
            std::map<std::string, std::vector<float>> m_lumiBXChanneldata100;
            webutils::WebChart* m_lumiBXChannelChart101;
            std::map<std::string, std::vector<float>> m_lumiBXChanneldata101;

            webutils::WebChart* m_lumiChannelChart100;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_lumiChannelData100;
            webutils::WebChart* m_lumiChannelChart101;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_lumiChannelData101;

            webutils::WebChart* m_rawlumiBXChannelChart100;
            std::map<std::string, std::vector<float>> m_rawlumiBXChanneldata100;
            webutils::WebChart* m_rawlumiBXChannelChart101;
            std::map<std::string, std::vector<float>> m_rawlumiBXChanneldata101;

            webutils::WebChart* m_rawlumiChannelChart100;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_rawlumiChannelData100;
            webutils::WebChart* m_rawlumiChannelChart101;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_rawlumiChannelData101;

            //Background Stuff
            webutils::WebChart* m_bkgBXChart100;
            std::map<std::string, std::vector<float>> m_bkgBXdata100;
            webutils::WebChart* m_bkgBXChart101;
            std::map<std::string, std::vector<float>> m_bkgBXdata101;
            webutils::WebChart* m_bkgChart100;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_bkgData100;
            webutils::WebChart* m_bkgChart101;
            std::map<std::string, std::deque<std::pair<unsigned long, float>>> m_bkgData101;

            //Agghist, colHist and BkgHist
            webutils::WebChart* m_agghistChart100;
            std::map<std::string, std::vector<uint16_t>> m_agghist_data100;
            webutils::WebChart* m_agghistChart101;
            std::map<std::string, std::vector<uint16_t>> m_agghist_data101;

            webutils::WebChart* m_colhistChart100;
            std::map<std::string, std::vector<uint16_t>> m_colhist_data100;
            webutils::WebChart* m_colhistChart101;
            std::map<std::string, std::vector<uint16_t>> m_colhist_data101;

            webutils::WebChart* m_bkghistChart100;
            std::map<std::string, std::vector<uint16_t>> m_bkghist_data100;
            webutils::WebChart* m_bkghistChart101;
            std::map<std::string, std::vector<uint16_t>> m_bkghist_data101;

            //Albedo Stuff
            webutils::WebChart* m_albedoChart100;
            std::map<std::string, std::vector<float>> m_albedo_data100;
            webutils::WebChart* m_albedoChart101;
            std::map<std::string, std::vector<float>> m_albedo_data101;
            //the one for the workloop, need to pick apart by algo id
            std::map<std::string, std::vector<float>> m_albedo_data;

            webutils::WebChart* m_fractionChart100;
            std::map<std::string, std::vector<float>> m_fraction_data100;
            webutils::WebChart* m_fractionChart101;
            std::map<std::string, std::vector<float>> m_fraction_data101;
            //the one for the workloop, need to pick apart by algo id
            std::map<std::string, std::vector<float>> m_fraction_data;

            /////////////////////////
            //Monitoring
            /////////////////////////
            xdata::InfoSpace* m_monInfospace;
            MonitoringVariables m_monVariables;

            /////////////////////////
            //Algorithms
            /////////////////////////

            LumiAlgorithm* m_lumiAlgorithm;
            AggregateAlgorithm* m_aggregateAlgorithm;
            BackgroundAlgorithm* m_backgroundAlgorithm;
            AlbedoAlgorithm* m_albedoAlgorithm;

          private:
            void setupChannels();
            void setupEventing();
            void setupWorkloops();
            void setupCharts();
            void setupAlgorithms();
            void setupMonitoring();
            void onMessageTopicBeam ( toolbox::mem::Reference* ref );
            void onMessageTopicVdmFlag ( toolbox::mem::Reference* ref );
            void onMessageTopicVdmScan ( toolbox::mem::Reference* ref );
            void printCacheStatus();
            void enqueueComplete();
            //Webcharts request data
            void requestRawData ( xgi::Input* in, xgi::Output* out );
            void requestOverviewData ( xgi::Input* in, xgi::Output* out );
            void requestChannelData ( xgi::Input* in, xgi::Output* out );
            void requestAlbedoData ( xgi::Input* in, xgi::Output* out );

            template<typename DataT, size_t nBin>
            void fillChartData (webutils::WebChart* chart, std::map<std::string, std::vector<DataT>>& chartMap, std::vector<HistogramBuffer<DataT, nBin>> bufVec, unsigned int algo);
            void fillChartDataLumi (std::pair<std::string, toolbox::mem::Reference*> pair);
            void fillChartDataBKG (std::pair<std::string, toolbox::mem::Reference*> pair);
            void fillChartDataHist (std::pair<std::string, toolbox::mem::Reference*> pair);

            template<typename DataT, size_t nBin>
            void doTimeAlignment (std::vector<HistogramBuffer<DataT, nBin>>& bufVec);

            inline bool is_subscribed (std::string topicname)
            {
                if (m_inputTopics.find (topicname) != std::end (m_inputTopics) ) return true;
                else return false;
            }

            void publish (std::pair<std::string, toolbox::mem::Reference*> pair);

            //Web callbacks
            void createTabBar (/*xgi::Input* in,*/ xgi::Output* out, Tab tab);          private:
            Application (const Application&);
            Application& operator = (const Application&);

        }; //Application

    } //namespace bcm1futcaprocessor
} //namespace bril


#endif
