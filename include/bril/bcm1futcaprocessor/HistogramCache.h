#ifndef _bril_bcm1f_utcaprocessor_histogramcache_h_
#define _bril_bcm1f_utcaprocessor_histogramcache_h_

//std c++
#include <algorithm>
#include <numeric>

//BRIL
#include "interface/bril/CommonDataFormat.h"

//XDAQ
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/TimeVal.h"
#include "toolbox/task/Guard.h"

//mine
#include "bril/bcm1futcaprocessor/Histogram.h"
#include "bril/bcm1futcaprocessor/Utils/ConsoleColor.h"

namespace bril {

    namespace bcm1futcaprocessor {

        typedef toolbox::task::Guard<toolbox::BSem> Locker;

        struct HistogramIdentifier
        {
          public:
            HistogramIdentifier (unsigned int algoID, unsigned int channelID) : m_identifier (0)
            {
                m_identifier |= ( (algoID & 0xFF) << 8) | (channelID & 0xFF);
            }
            void decode (unsigned int& algoID, unsigned int& channelID) const
            {
                algoID = (m_identifier & 0xFF00) >> 8;
                channelID = (m_identifier & 0x00FF);
            }

            bool operator < (const HistogramIdentifier& rhs) const
            {
                return this->m_identifier < rhs.m_identifier;
            }

            bool operator == (const HistogramIdentifier& rhs) const
            {
                if (this->m_identifier == rhs.m_identifier) return true;
                else return false;
            }


          private:
            uint16_t m_identifier;
        }; // struct HistogramIdentifier


        struct NB4Identifier
        {
            unsigned int m_fillnum;
            unsigned int m_runnum;
            unsigned int m_lsnum;
            unsigned int m_nbnum;
            unsigned int m_timestampsec;
            unsigned int m_timestampmsec;

            NB4Identifier (unsigned int fillnum, unsigned int runnum, unsigned int lsnum, unsigned int nbnum, unsigned int timestampsec, unsigned int timestampmsec) :
                m_fillnum (fillnum),
                m_runnum (runnum),
                m_lsnum (lsnum),
                m_nbnum (nbnum),
                m_timestampsec (timestampsec),
                m_timestampmsec (timestampmsec)
            {}

            NB4Identifier() :
                m_fillnum (0),
                m_runnum (0),
                m_lsnum (0),
                m_nbnum (0),
                m_timestampsec (0),
                m_timestampmsec (0)
            {}

            bool operator < (const NB4Identifier& rhs) const
            {
                return this->m_fillnum < rhs.m_fillnum || (this->m_fillnum == rhs.m_fillnum  && (this->m_runnum < rhs.m_runnum || (this->m_runnum == rhs.m_runnum &&  (this->m_lsnum < rhs.m_lsnum || (this->m_lsnum == rhs.m_lsnum && this->m_nbnum < rhs.m_nbnum) ) ) ) );

                //return this->m_runnum < rhs.m_runnum || (this->m_runnum == rhs.m_runnum && (this->m_lsnum < rhs.m_lsnum || (this->m_lsnum == rhs.m_lsnum && this->m_nbnum < rhs.m_nbnum) ) );
            }
        }; // struct NB4Identifier

        struct LSIdentifier
        {
            unsigned int m_fillnum;
            unsigned int m_runnum;
            unsigned int m_lsnum;

            LSIdentifier (unsigned int fillnum, unsigned int runnum, unsigned int lsnum) :
                m_fillnum (fillnum),
                m_runnum (runnum),
                m_lsnum (lsnum)
            {}

            LSIdentifier() :
                m_fillnum (0),
                m_runnum (0),
                m_lsnum (0)
            {}

            bool operator < (const LSIdentifier& rhs) const
            {
                return this->m_fillnum < rhs.m_fillnum || (this->m_fillnum == rhs.m_fillnum  && (this->m_runnum < rhs.m_runnum || (this->m_runnum == rhs.m_runnum &&  (this->m_lsnum < rhs.m_lsnum  ) ) ) );

            }
        }; // struct LSIdentifier

        template<typename DataT, size_t nBin>
        class HistogramBuffer
        {

          public:
            std::map<HistogramIdentifier, Histogram<DataT, nBin>> m_channelMap;
            NB4Identifier m_nb4Identifier;
            std::string m_histType;
            size_t m_aggregateCounter;
          private:
            bool m_isComplete;
            //std::vector<unsigned int> m_goodChannelList;
            //std::vector<unsigned int> m_allowedAlgos;
            size_t m_defaultSize;
          public:

            HistogramBuffer (size_t defaultSize, NB4Identifier nbIdentifier, std::string histType) :
                m_nb4Identifier (nbIdentifier),
                m_histType (histType),
                m_aggregateCounter (0),
                m_isComplete (false),
                m_defaultSize (defaultSize)
            {
                m_channelMap.clear();
            }

            bool is_complete()
            {
                return m_isComplete;
            }

            size_t size()
            {
                return m_channelMap.size();
            }

            NB4Identifier getNB4Identifier()
            {
                return m_nb4Identifier;
            }

            std::string getHistType()
            {
                return m_histType;
            }

            size_t getAggregateOperations()
            {
                return m_aggregateCounter;
            }

            void check_complete()
            {
                //we don't even have the correct number of histograms yet so we return and leave m_isComplete false
                if (m_channelMap.size() != m_defaultSize ) return;
                else
                {
                    //IMPORTANT: the below is extra catious but unnecessary - either it's complete or not
                    //thus it's much faster
                    m_isComplete = true;
                    //bool t_isProblem = false;

                    ////otherwise check that I have a histogram for each channel in the good channel list and for each allowed algorithm
                    //for (auto channel : m_goodChannelList)
                    //{
                    //for (auto algo : m_allowedAlgos)
                    //{
                    //HistogramIdentifier t_identifier (algo, channel );
                    //auto hist = m_channelMap.find (t_identifier);

                    //if (hist == std::end (m_channelMap) ) t_isProblem = true;
                    //}
                    //}

                    ////if t_isProblem is still false, we are good!
                    //if (!t_isProblem) m_isComplete = true;
                }
            }

            void add (HistogramIdentifier histIdentifier, Histogram<DataT, nBin> hist)
            {
                //check if this element already exists
                auto pos = m_channelMap.find (histIdentifier);

                if (pos != std::end (m_channelMap) )
                    std::cout << RED << "ERROR: a histogram for this channel and algo exists already!" << RESET << std::endl;
                else
                    m_channelMap.insert (std::make_pair (histIdentifier, hist) );

                this->check_complete();
            }

            void aggregate (bril::bcm1futcaprocessor::HistogramBuffer<uint16_t, nBin>& aBuf)
            {
                //first, iterate the histogram map
                if (!this->m_channelMap.size() ) // this is only the case when my current buffer is empty
                {
                    //reset the aggregation counter
                    m_aggregateCounter = 0;

                    for (auto aBufIt : aBuf.m_channelMap)
                        this->m_channelMap[aBufIt.first] = aBufIt.second.convertuint32();

                    this->check_complete();
                    m_aggregateCounter++;
                }
                else if (this->is_complete() && aBuf.is_complete() )
                {
                    auto aBufIt = std::begin (aBuf.m_channelMap);

                    for (auto histIt : this->m_channelMap )
                    {
                        //if (histIt.first == aBufIt->first)
                        histIt.second += aBufIt->second.convertuint32();
                        //else std::cout << BOLDRED << "ERROR, channel and algo identifier don't match!" << RESET << std::endl;

                        aBufIt++;
                    }

                    m_aggregateCounter++;
                }
                else std::cout << BOLDRED << "ERROR, something went wrong here!" << RESET << std::endl;
            }

            void aggregate (bril::bcm1futcaprocessor::HistogramBuffer<uint32_t, nBin>& aBuf)
            {
                //first, iterate the histogram map
                if (!this->m_channelMap.size() ) // this is only the case when my current buffer is empty
                {
                    //reset the aggregation counter
                    m_aggregateCounter = 0;

                    for (auto aBufIt : aBuf.m_channelMap)
                        this->m_channelMap[aBufIt.first] = aBufIt.second;

                    this->check_complete();
                    m_aggregateCounter++;
                }
                else if (this->is_complete() && aBuf.is_complete() )
                {
                    auto aBufIt = std::begin (aBuf.m_channelMap);

                    for (auto histIt : this->m_channelMap )
                    {
                        //if (histIt.first == aBufIt->first)
                        histIt.second += aBufIt->second;
                        //else std::cout << BOLDRED << "ERROR, channel and algo identifier don't match!" << RESET << std::endl;

                        aBufIt++;
                    }

                    m_aggregateCounter++;
                }
                else std::cout << BOLDRED << "ERROR, something went wrong here!" << RESET << std::endl;
            }

            void aggregate (bril::bcm1futcaprocessor::HistogramBuffer<uint8_t, nBin>& aBuf)
            {
                //first, iterate the histogram map
                if (!this->m_channelMap.size() ) // this is only the case when my current buffer is empty
                {
                    //reset the aggregation counter
                    m_aggregateCounter = 0;

                    for (auto aBufIt : aBuf.m_channelMap)
                        this->m_channelMap[aBufIt.first] = aBufIt.second.convertuint32();

                    this->check_complete();
                    m_aggregateCounter++;
                }


                else if (this->is_complete() && aBuf.is_complete() )
                {
                    auto aBufIt = std::begin (aBuf.m_channelMap);

                    for (auto histIt : this->m_channelMap )
                    {
                        //if (histIt.first == aBufIt->first)
                        histIt.second += aBufIt->second.convertuint32();
                        //else std::cout << BOLDRED << "ERROR, channel and algo identifier don't match!" << RESET << std::endl;

                        aBufIt++;
                    }

                    m_aggregateCounter++;
                }
                else std::cout << BOLDRED << "ERROR, something went wrong here!" << RESET << std::endl;
            }
        }; // class histogramBuffer

        template<typename DataT, size_t nBin, typename DatumT>
        class HistogramCache
        {
          private:
            typedef std::map<NB4Identifier, HistogramBuffer<DataT, nBin>> NibbleBuffer;
            typedef typename std::map<NB4Identifier, HistogramBuffer<DataT, nBin>>::iterator NibbleIter;
            //using NibbleBuffer = std::map<NB4Identifier, HistogramBuffer<DataT, nBin>>;
            NibbleBuffer m_nibbleBuffer;

            typedef std::map<LSIdentifier, HistogramBuffer<uint32_t, nBin>> LSBuffer;
            typedef typename std::map<LSIdentifier, HistogramBuffer<uint32_t, nBin>>::iterator LSIter;
            //using LSBuffer = std::map<LSIdentifier, HistogramBuffer<uint32_t, nBin>>;
            LSBuffer m_lsBuffer;

            //bool m_lsComplete;
            //size_t m_nb4Counter;
            //unsigned int m_currentLS;

            std::vector<unsigned int> m_goodChannelList;
            std::vector<unsigned int> m_allowedAlgos;

            unsigned int m_lastAdditionTime;
            std::string m_histName;
            //for thread safety
            toolbox::BSem m_appLock;

          public:
            HistogramCache (std::vector<unsigned int> goodChannels, std::vector<unsigned int> algos) :
                m_goodChannelList (goodChannels),
                m_allowedAlgos (algos),
                m_histName (""),
                m_appLock (toolbox::BSem::FULL)
            {
                //first, sort the good channel list
                std::sort (m_goodChannelList.begin(), m_goodChannelList.end() );
                //clear the map that is used to store the nibble4s
                m_nibbleBuffer.clear();
                m_lsBuffer.clear();

                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }
            HistogramCache() :
                m_histName (""),
                m_appLock (toolbox::BSem::FULL)
            {
                m_goodChannelList.clear();
                m_allowedAlgos.clear();
                m_nibbleBuffer.clear(); //m_lsBuffer.clear();
                m_lsBuffer.clear();

                //set the last received Time to construction time
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                m_lastAdditionTime = t.sec();
            }

            void setChannelAndAlgoList (std::vector<unsigned int> goodChannels, std::vector<unsigned int> algos)
            {
                m_goodChannelList = goodChannels;
                //first, sort the good channel list
                std::sort (m_goodChannelList.begin(), m_goodChannelList.end() );
                m_allowedAlgos = algos;
            }


            void add (toolbox::mem::Reference* ref, std::string pHistName, std::ostream& logstream = std::cout)
            {
                m_histName = pHistName;
                //here cast the reference into the correct type and decode the nb number
                //get the b2in message header with all the run number, nibble, NB4 etc info, this also includes the channel id
                //this const cast is necessary because Datum does not have a const-qualified accessor ...
                const interface::bril::Datum<DatumT>* t_datum = (interface::bril::Datum<DatumT>*) (ref->getDataLocation() );

                //get the specific info about run, fill, ls, nb, channel
                unsigned int fillnum = t_datum->fillnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int runnum = t_datum->runnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int lsnum = t_datum->lsnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int nbnum = t_datum->nbnum; //i  have to encode nbnum, lsnum and runnum into the key
                unsigned int mod = nbnum % 4;

                unsigned int channelID = t_datum->getChannelID();
                unsigned int algoID = t_datum->getAlgoID();

                if (mod != 0)
                {
                    logstream << MAGENTA << "Attention, pulling dirty trick since I had to correct the NB4 number from " << nbnum << " to " << nbnum - mod << RESET << " for channel " << channelID << " algo " << algoID << " ls " << lsnum <<  std::endl;
                    nbnum -= mod;
                }

                unsigned int timestampsec = t_datum->timestampsec;
                unsigned int timestampmsec = t_datum->timestampmsec;
                NB4Identifier t_nibble4 (fillnum, runnum, lsnum, nbnum, timestampsec, timestampmsec);

                //only add if the channel and algorithm are accepted
                if ( (std::find (m_goodChannelList.begin(), m_goodChannelList.end(), channelID) != m_goodChannelList.end() ) &&
                        (std::find (m_allowedAlgos.begin(), m_allowedAlgos.end(), algoID) != m_allowedAlgos.end() ) )
                {
                    HistogramIdentifier t_identifier (algoID, channelID);

                    //convert the reference into a histogram to start with!
                    Histogram<DataT, nBin> t_hist (*t_datum);

                    //from here on need to protect the m_nibblebuffer with a BSEM
                    Locker t_locker (m_appLock);

                    //now check if that NB4Identifier already exists in the nibble4 map
                    auto nb4 = m_nibbleBuffer.find (t_nibble4);

                    //if it does, call the histogramBuffer.add method with the histogram as argument
                    if (nb4 != std::end (m_nibbleBuffer) )
                    {
                        nb4->second.add (t_identifier, t_hist);

                        //now check if the nb4 is complete and put it in the ls buffer
                        if (nb4->second.is_complete() )
                        {
                            LSIdentifier t_LS (fillnum, runnum, lsnum);
                            //check if the LS already exists in the cache
                            auto t_lsBuffer = m_lsBuffer.find (t_LS);

                            //if it does not, then add an empty Buffer object in the map
                            if (t_lsBuffer == std::end (m_lsBuffer) )
                            {
                                //create a "dumb" NB4 identifier for the histogram cache object to  hold the LS histogram
                                //and set the timestampsec to creation time
                                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                                unsigned int now = t.sec();
                                NB4Identifier t_nb4forLS = t_nibble4;
                                t_nb4forLS.m_nbnum = 0;
                                t_nb4forLS.m_timestampsec = now;
                                t_nb4forLS.m_timestampmsec = 0;
                                m_lsBuffer.emplace (t_LS, HistogramBuffer<uint32_t, nBin> (m_goodChannelList.size() * m_allowedAlgos.size(), t_nb4forLS, nb4->second.getHistType() ) );
                                t_lsBuffer = m_lsBuffer.find (t_LS); //put the iterator there since now it exists for sure!
                            }

                            t_lsBuffer->second.aggregate (nb4->second);
                        }
                    }
                    else // as in the NB4Identifier does not yet exist in the map
                    {
                        //create a histogram buffer object at location t_nibble4
                        m_nibbleBuffer.emplace (t_nibble4, HistogramBuffer<DataT, nBin> (m_goodChannelList.size() * m_allowedAlgos.size(), t_nibble4, pHistName) );
                        //and add the histogram
                        //first put the iterator to the newly created object
                        nb4 = m_nibbleBuffer.find (t_nibble4);
                        nb4->second.add (t_identifier, t_hist);

                        //I just created a new NB4 buffer, so it can be by no means complete and thus can not go to the LS buffer yet

                    }

                    //update last received time
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    m_lastAdditionTime = t.sec();
                }
            }

            void clear()
            {
                Locker t_locker (m_appLock);
                m_nibbleBuffer.clear();
                m_lsBuffer.clear();
            }

            bool is_clean()
            {
                Locker t_locker (m_appLock);

                if (m_nibbleBuffer.size() == 0) return true;
                else return false;
            }

            unsigned int getLastUpdate()
            {
                return m_lastAdditionTime;
            }

            std::vector<HistogramBuffer<DataT, nBin>> get_completedNB4 (unsigned int timeout_sec, std::ostream& logstream = std::cout )
            {
                Locker t_locker (m_appLock);

                std::vector<HistogramBuffer<DataT, nBin>> retvec;

                for (auto nb4It = m_nibbleBuffer.begin(); nb4It != m_nibbleBuffer.end();)
                {
                    if (nb4It->second.is_complete() )
                    {
                        retvec.push_back (nb4It->second);
                        nb4It = m_nibbleBuffer.erase (nb4It);
                    }

                    else if (this->is_stale_NB4 (nb4It, timeout_sec, logstream) )
                        nb4It = m_nibbleBuffer.erase (nb4It);

                    else nb4It++;
                }

                return retvec;
            }

            std::vector<HistogramBuffer<uint32_t, nBin>> get_completedLS (unsigned int timeout_sec, std::ostream& logstream = std::cout)
            {
                Locker t_locker (m_appLock);
                std::vector<HistogramBuffer<uint32_t, nBin>> retvec;

                for (auto buffer = m_lsBuffer.begin(); buffer != m_lsBuffer.end();)
                {
                    if (buffer->second.getAggregateOperations() == 16) // the ls is complete
                    {
                        retvec.push_back (buffer->second );
                        buffer = m_lsBuffer.erase (buffer);
                    }
                    else if (this->is_stale_LS (buffer, timeout_sec, logstream) )
                    {
                        //if I want to enqueue it, I should scale it
                        double factor = 16 / buffer->second.getAggregateOperations();

                        for (auto hist : buffer->second.m_channelMap)
                            hist.second.scale (factor);

                        retvec.push_back (buffer->second);
                        buffer = m_lsBuffer.erase (buffer);
                    }
                    else
                        buffer++;
                }

                return retvec;
            }

            void print (std::stringstream& msg)
            {
                Locker t_locker (m_appLock);
                //msg << "Histogram Cache currently containing data from  " << m_nibbleBuffer.size() << " NB4: " << std::endl;

                for (auto nb4 : m_nibbleBuffer)
                    msg << nb4.first.m_lsnum << "-" << nb4.first.m_nbnum << ": from  " << nb4.second.size() / 2 << " Channels - is complete: " << nb4.second.is_complete() << std::endl;
            }

            void printLS (std::stringstream& msg)
            {
                Locker t_locker (m_appLock);
                //msg << RED << "Lumi Section Histogram Cache currently containing data from " << m_lsBuffer.size() << " Lumi Sections" << RESET << std::endl;

                for (auto ls : m_lsBuffer)
                    msg << ls.first.m_lsnum << ": containing " << ls.second.getAggregateOperations() << " nb4s" << RESET << std::endl;
            }


          private:
            bool is_stale_NB4 (NibbleIter it, unsigned int timeout_sec, std::ostream& logstream = std::cout)
            {
                bool stale = false;
                //here just check if a NB4 buffer is older then timeout_sec seconds - if so mark it as stale so it can be deleted
                //also print the debug output so I know which channels are missing
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                unsigned int now = t.sec();

                if ( (now - it->first.m_timestampsec) > timeout_sec && !it->second.is_complete() )
                {
                    //too old so stale
                    stale = true;
                    //alarm everyone
                    std::cout << RED << "Found stale (older than " << timeout_sec << " seconds) NB4 buffer for type: " << m_histName << " -- LS: " << it->first.m_lsnum << " NB4 " << it->first.m_nbnum << " since it contains only " << it->second.size() / 2 << " channels instead of " << m_goodChannelList.size() << RESET << std::endl;

                    std::vector<unsigned int> t_receivedChannelList;

                    for (auto hist : it->second.m_channelMap)
                    {
                        unsigned int algo;
                        unsigned int channel;
                        hist.first.decode (algo, channel);

                        if (algo == 100)
                            t_receivedChannelList.push_back (channel);
                    }

                    //here list channels missing
                    std::sort (t_receivedChannelList.begin(), t_receivedChannelList.end() );
                    std::vector<unsigned int> t_difference;
                    std::set_symmetric_difference (m_goodChannelList.begin(), m_goodChannelList.end(), t_receivedChannelList.begin(), t_receivedChannelList.end(), back_inserter (t_difference) );

                    logstream << std::endl <<  "Discarding incomplete NB4 buffer for type: " << m_histName << " -- LS " << it->first.m_lsnum << " NB4 " << it->first.m_nbnum << " as it is older than " << timeout_sec << " seconds - Missing channels: ";

                    for (auto chan : t_difference)
                        logstream << chan << ", ";

                    logstream << std::endl;
                }

                return stale;
            }


            bool is_stale_LS (LSIter it, unsigned int timeout_sec,  std::ostream& logstream = std::cout)
            {
                bool stale = false;
                //the Lumisection should have the creation time in SW in the key NB4 identifier
                //its in it->second.getNB4Identifier().m_timestampsec
                toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                unsigned int now = t.sec();

                if ( (now - it->second.getNB4Identifier().m_timestampsec) > timeout_sec && it->second.getAggregateOperations() != 16)
                {
                    //the LS buffer is older than the timeout
                    stale = true;
                    std::cout << BOLDRED << "Found Lumisection older than " << timeout_sec << " seconds - for LS " << it->first.m_lsnum << " enqueing and scaling by appropriate size later! - the actual size is: " << it->second.getAggregateOperations() << RESET << std::endl;
                }

                return stale;
            }


        }; // class Histogram Cache

    } // namespace bcm1futcaprocessor

} // namespace bril
#endif
