#ifndef _bril_bcm1f_utcaprocessor_beam_h_
#define _bril_bcm1f_utcaprocessor_beam_h_

#include <algorithm>
#include <numeric>
#include <bitset>

//XDAQ
#include "toolbox/mem/Reference.h"
#include "toolbox/BSem.h"
#include "toolbox/task/Guard.h"

//BRIL
#include "interface/bril/BEAMTopics.hh"
#include "interface/bril/CommonDataFormat.h"
#include "interface/bril/CompoundDataStreamer.h"

namespace bril {

    namespace bcm1futcaprocessor {

        class Beam
        {
          public:
            //total beam current for B1 & B2
            float m_b1Itot;
            float m_b2Itot;
            //intensities for B1 & B2
            float m_bx1Intensity[interface::bril::MAX_NBX];
            float m_bx2Intensity[interface::bril::MAX_NBX];
            //configuration for B1 & B2
            bool m_bx1Conf[interface::bril::MAX_NBX];
            bool m_bx2Conf[interface::bril::MAX_NBX];
            //modes
            std::string m_machineMode;
            std::string m_beamMode;

            bool m_beamPresent;

            //some internal helper
            int m_lastCollidingBunch;

            //masks for colliding bunches
            std::bitset<interface::bril::MAX_NBX> m_collidingMask;
            std::bitset<interface::bril::MAX_NBX> m_bkg1Mask;
            std::bitset<interface::bril::MAX_NBX> m_bkg2Mask;

            //VdM flags
            bool m_vdmFlag;
            bool m_vdmIP5;

            //for thread safety
            //toolbox::BSem m_appLock;

          public:
            Beam() :
                m_b1Itot (0),
                m_b2Itot (0),
                m_machineMode (""),
                m_beamMode (""),
                m_beamPresent (false),
                m_lastCollidingBunch (-300),
                m_vdmFlag (false),
                m_vdmIP5 (false)
                //m_appLock (toolbox::BSem::FULL)
            {
                memset (m_bx1Intensity, 0, sizeof (m_bx1Intensity) );
                memset (m_bx2Intensity, 0, sizeof (m_bx2Intensity) );
                memset (m_bx1Conf, 0, sizeof (m_bx1Conf) );
                memset (m_bx2Conf, 0, sizeof (m_bx2Conf) );

                m_collidingMask.reset();
                m_bkg1Mask.reset();
                m_bkg2Mask.reset();
            }


            void update (toolbox::mem::Reference* ref)
            {
                //Locker t_locker (m_appLock);
                //set last Colliding Bunch to -300 to restart the calculation
                m_lastCollidingBunch = -300;

                //get the Datum Header from the reference
                interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (ref->getDataLocation() );
                //create a compound data streamer to extrat the beam payload
                interface::bril::CompoundDataStreamer t_streamer (interface::bril::beamT::payloaddict() );
                //decode the info
                t_streamer.extract_field (&m_b1Itot, "intensity1", t_inHeader->payloadanchor);
                t_streamer.extract_field (&m_b2Itot, "intensity2", t_inHeader->payloadanchor);
                t_streamer.extract_field (m_bx1Intensity, "bxintensity1", t_inHeader->payloadanchor);
                t_streamer.extract_field (m_bx2Intensity, "bxintensity2", t_inHeader->payloadanchor);
                t_streamer.extract_field (m_bx1Conf, "bxconfig1", t_inHeader->payloadanchor);
                t_streamer.extract_field (m_bx2Conf, "bxconfig2", t_inHeader->payloadanchor);
                char t_machineMode[50];
                t_streamer.extract_field (t_machineMode, "machinemode", t_inHeader->payloadanchor);
                m_machineMode = std::string (t_machineMode);
                char t_beamMode[50];
                t_streamer.extract_field (t_beamMode, "status", t_inHeader->payloadanchor);
                m_beamMode = std::string (t_beamMode);

                //now figure out if beams can collide or not
                if (    m_beamMode == "FLAT TOP" ||
                        m_beamMode == "SQUEEZE"  ||
                        m_beamMode == "ADJUST"   ||
                        m_beamMode == "STABLE BEAMS" ||
                        m_beamMode == "UNSTABLE BEAMS" ||
                        m_beamMode == "BEAM DUMP WARNING")
                    m_beamPresent = true;
                else
                    m_beamPresent = false;

                //now let's compute some masks and parameters
                for (size_t bx = 0; bx < interface::bril::MAX_NBX; bx++)
                {
                    int lastColl = bx - m_lastCollidingBunch;

                    if (m_bx1Conf[bx] && (!m_beamPresent || lastColl > 30) )
                        m_bkg1Mask.set (bx);

                    if (m_bx2Conf[bx] && (!m_beamPresent || lastColl > 30) )
                        m_bkg2Mask.set (bx);

                    if (m_bx1Conf[bx] && m_bx2Conf[bx])
                    {
                        m_collidingMask.set (bx);
                        m_lastCollidingBunch = bx;
                    }
                }

            } // method update

            void updateVdmFlag (toolbox::mem::Reference* ref)
            {
                bool t_vdmFlag = false;
                interface::bril::vdmflagT* topic = (interface::bril::vdmflagT*) (ref->getDataLocation() );
                //this is dirty but avoids using a compound data streamer
                t_vdmFlag = topic->payload() [0];
                m_vdmFlag = t_vdmFlag;
            }

            void updateVdmIP (toolbox::mem::Reference* ref)
            {
                uint8_t t_IP = 0;
                //get the Datum Header from the reference
                interface::bril::DatumHead* t_inHeader = (interface::bril::DatumHead*) (ref->getDataLocation() );
                //create a compound data streamer to extrat the beam payload
                interface::bril::CompoundDataStreamer t_streamer (interface::bril::vdmscanT::payloaddict() );
                //decode the info
                t_streamer.extract_field (&t_IP, "ip", t_inHeader->payloadanchor);

                if (t_IP & (1 << 4) ) m_vdmIP5 = true;
                else m_vdmIP5 = false;
            }

            void printStatus (std::stringstream& msg)
            {
                //Locker t_locker (m_appLock);
                msg << std::endl;
                msg << BOLDBLUE << "******************************************" << std::endl;
                msg << BOLDBLUE << "**             BEAM STATUS              **" << std::endl;
                msg << BOLDBLUE << "******************************************" << RESET << std::endl;
                msg << BOLDBLUE << "Machine Mode: " << GREEN << m_machineMode << RESET << std::endl;
                msg << BOLDBLUE << "Beam Mode: " << GREEN << m_beamMode << RESET << std::endl;
                msg << BOLDBLUE << "Can collide: " << GREEN << m_beamPresent << RESET << std::endl;
                msg << BOLDBLUE << "# of colliding bunches: " << GREEN << m_collidingMask.count() << BOLDBLUE << " # of bunches in BKG1 mask: " << GREEN << m_bkg1Mask.count() << BOLDBLUE << " # of bunches in BKG2 mask: " << GREEN << m_bkg2Mask.count() << RESET << std::endl << std::endl;
            }

            std::string getBeamMode() const
            {
                return m_beamMode;
            }

            bool isColliding (size_t bx) const
            {
                return m_collidingMask.test (bx);
            }

            bool test_bkg (unsigned int beam, unsigned int BX) const
            {
                if (beam == 1) return m_bkg1Mask.test (BX);
                else return m_bkg2Mask.test (BX);
            }

            float getBunchCurrentSumBkg ( unsigned int ibeam) const
            {
                float sum = 0;

                for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                {
                    if (test_bkg (ibeam, iBX) && ibeam == 1) sum += m_bx1Intensity[iBX] / 1e11;

                    if (test_bkg (ibeam, iBX)  && ibeam == 2) sum += m_bx2Intensity[iBX] / 1e11;
                }

                return sum;
            }

            float getBunchCurrent (unsigned int ibeam, size_t BX) const
            {
                if (ibeam == 1) return m_bx1Intensity[BX] / 1e11;

                else if (ibeam == 2) return m_bx2Intensity[BX] / 1e11;
                else return 0;
            }

            std::string getBeamMode()
            {
                return m_beamMode;
            }
        };

    } //namespace bcm1futcaprocessor
} //namespace bril

#endif
