//TODO:
//take only odd bins for the albedo computation?? WRONG, check timing diagram where we expect collision products and MIB and take all others
//for bkg_nc take all non collding bunches and integrate over full 25ns DONE
//colhist: use only luminous (3,4,5,6) bins of colliding bunches DONE
//bkghist: use only MIB bins(1,2) of non colliding bunches according to bunch mask DONE

#ifndef _bril_bcm1f_utcaprocessor_backgroundalgorithm_h_
#define _bril_bcm1f_utcaprocessor_backgroundalgorithm_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"
#include <deque>

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class DefaultBackground : public BackgroundAlgorithm
        {
          public:
            DefaultBackground (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool )
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;

                m_bestAlgo = std::stoul (props.getProperty ("bestAlgo") );
                m_albedoAboveBkg = 0;
                //m_last4BKG1[100] = std::deque<float> (4, 0);
                //m_last4BKG1[101] = std::deque<float> (4, 0);
                //m_last4BKG2[100] = std::deque<float> (4, 0);
                //m_last4BKG2[101] = std::deque<float> (4, 0);
            }

            void compute (  OccupancyHistogramVector& histogramVector, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>* publishQueue, MonitoringVariables* monitoring)
            {
                //first, loop the Histogram Vector
                for (auto& histBuffer : histogramVector)
                {
                    //For the background topic
                    std::string pdict = interface::bril::bcm1futca_backgroundT::payloaddict();
                    xdata::Properties plist;
                    plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);
                    plist.setProperty ("PAYLOAD_DICT", pdict);

                    //first, the References for the lumi numbers
                    toolbox::mem::Reference* background100 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_lumiT::maxsize() );
                    interface::bril::bcm1futca_backgroundT* backgroundhist100 = this->getHistPtr<interface::bril::bcm1futca_backgroundT> (background100, histBuffer.m_nb4Identifier, SensorType::All, 0, 100);

                    toolbox::mem::Reference* background101 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_backgroundT::maxsize() );
                    interface::bril::bcm1futca_backgroundT* backgroundhist101 = this->getHistPtr<interface::bril::bcm1futca_backgroundT> (background101, histBuffer.m_nb4Identifier, SensorType::All, 0, 101);

                    ////now I have a histogram buffer object
                    ////histBuffer.first is the NB4 identifier in case I need it later
                    ////histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    ////here i should instantiate the various variables for bkg calculations
                    ////to be fair, I need these split up by AlgoIDs too

                    std::map<unsigned int, float> bkg1sum
                    {
                        std::make_pair (100, 0),
                        std::make_pair (101, 0)
                    };

                    std::map<unsigned int, float> bkg2sum = bkg1sum;
                    //these are for the bkg_nc12
                    std::map<unsigned int, float> bkg1sum_nc = bkg1sum;
                    std::map<unsigned int, float> bkg2sum_nc = bkg1sum;

                    //these come from the beam topic, so compute them in the Beam object
                    float bunchCurrent1Sum = beam.getBunchCurrentSumBkg (1);
                    float bunchCurrent2Sum = beam.getBunchCurrentSumBkg (2);

                    //these I need to map to algo ID, otherwise i count doulbe
                    std::map<unsigned int, float> totalActiveArea1 = bkg1sum;
                    std::map<unsigned int, float> totalActiveArea2 = bkg1sum;

                    std::map<unsigned int, float> bkg_plus = bkg1sum;
                    std::map<unsigned int, float> bkg_minus = bkg1sum;

                    std::map<unsigned int, unsigned int> nActiveChannels1
                    {
                        std::make_pair (100, 0),
                        std::make_pair (101, 0)
                    };

                    std::map<unsigned int, unsigned int> nActiveChannels2 = nActiveChannels1;

                    //for the per BX background
                    std::map<unsigned int, std::vector<float>> bkg1_bx;
                    bkg1_bx.emplace (100, std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    bkg1_bx.emplace (101, std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    std::map<unsigned int, std::vector<float>> bkg2_bx = bkg1_bx;

                    for (auto& hist : histBuffer.m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        ////now looping the acutal channel histograms
                        ////hist.first is the Histogram Identifier
                        ////hist.second is the histogram object

                        //prepare and get the colhist
                        std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_col_histT::topicname(), this->createColHist (hist.second, beam, histBuffer.m_nb4Identifier, hist.first) );
                        publishQueue->push (t_pair);

                        //prepare and get the bkg hist
                        t_pair = std::make_pair (interface::bril::bcm1futca_bkg_histT::topicname(), this->createBkgHist (hist.second, beam, histBuffer.m_nb4Identifier, hist.first) );
                        publishQueue->push (t_pair);

                        hist.second.maskAbortGap();//maks the abort gap first before integrating for total rate

                        ////now test if we want this channel for bkg calculations
                        if (m_channelInfo->useBkg (t_channel) )
                        {
                            SensorPosition t_pos = m_channelInfo->getSensorPosition (t_channel);
                            SensorType t_type = m_channelInfo->getSensorType (t_channel);

                            // get the total active area and number of sensors
                            // this needs to be mapped to the algo ID otherwise I would count twice
                            if (t_pos == SensorPosition::plus)
                            {
                                nActiveChannels1[t_algo]++;

                                if (t_type == SensorType::pCVD || t_type == SensorType::sCVD) totalActiveArea1[t_algo] += bril::bcm1futcaprocessor::s_areaDiamond * m_channelInfo->getFactor (hist.first);
                                else if (t_type == SensorType::Si) totalActiveArea1[t_algo] += bril::bcm1futcaprocessor::s_areaSilicon * m_channelInfo->getFactor (hist.first);
                            }
                            else if (t_pos == SensorPosition::minus)
                            {
                                nActiveChannels2[t_algo]++;

                                if (t_type == SensorType::pCVD || t_type == SensorType::sCVD) totalActiveArea2[t_algo] += bril::bcm1futcaprocessor::s_areaDiamond * m_channelInfo->getFactor (hist.first);
                                else if (t_type == SensorType::Si) totalActiveArea2[t_algo] += bril::bcm1futcaprocessor::s_areaSilicon * m_channelInfo->getFactor (hist.first);
                            }

                            //now loop the uTCA histogram bins
                            //float binsum_nc = 0;

                            for (size_t iBin = 0; iBin < hist.second.size(); iBin++ )
                            {
                                unsigned int t_beam = 0;

                                if (t_pos == SensorPosition::plus) t_beam = 1;
                                else if (t_pos == SensorPosition::minus) t_beam = 2;

                                //first, check if we want to use this bunch for BKG
                                size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;

                                if (beam.test_bkg (t_beam, static_cast<unsigned int> (iBX) ) )
                                {
                                    if (iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC == 0) // this is a MIB BIN
                                    {
                                        //calculate albedo as average over the last 20 bins - this will have to change; TODO
                                        float albedo = 0;
                                        float n_albedoBins = 0;

                                        for (size_t i = 0; i < 20; i++)
                                        {
                                            size_t albedoBin = iBin - i - 1;

                                            if (albedoBin < 0) albedoBin += interface::bril::BCM1FUTCA_BINPERBX_OCC * interface::bril::MAX_NBX;

                                            if (this->isAlbedoBin (albedoBin) )
                                            {
                                                albedo += static_cast<float> ( hist.second[albedoBin]) / 4.; //normalize by the number of nibbles
                                                n_albedoBins++;
                                            }
                                        }

                                        albedo /= n_albedoBins;
                                        //compute the initial number for the background
                                        float t_bkg = static_cast<float> (hist.second[iBin]) / m_channelInfo->getFactor (hist.first) / 4.;
                                        //subtract the albedo
                                        t_bkg -= albedo;

                                        if (t_bkg < 0)
                                        {
                                            t_bkg = 0;
                                            m_albedoAboveBkg++;
                                        }

                                        //now check if the bunch is not colliding - this is an additional restriction as some colliding bunches are used for BKG if they are the first in a train
                                        if (!beam.isColliding (iBX) )
                                        {
                                            if (t_pos == SensorPosition::plus)
                                                bkg1sum_nc[t_algo] += t_bkg;
                                            else if (t_pos == SensorPosition::minus)
                                                bkg2sum_nc[t_algo] += t_bkg;
                                        }

                                        if (t_pos == SensorPosition::plus)
                                        {
                                            bkg1sum[t_algo] += t_bkg;
                                            bkg1_bx[t_algo].at (iBX) = t_bkg;
                                        }
                                        else if (t_pos == SensorPosition::minus)
                                        {
                                            bkg2sum[t_algo] += t_bkg;
                                            bkg2_bx[t_algo].at (iBX) = t_bkg;
                                        }
                                    } //end of check if first bin in BX
                                } //end of bkg bin check

                                //TODO: alternative version of treating nc background but I guess this is wrong
                                //if (!beam.isColliding (iBX) )
                                //{
                                ////we need to aggregate into bxsum_nc and reset every 6 bins
                                //float t_bkg = static_cast<float> (hist.second[iBin]) / m_channelInfo->getFactor (hist.first) / 4.;
                                //binsum_nc += t_bkg;

                                //if (iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC == 0  && iBin != 0) //condition that we are in a new BX
                                //{
                                //if (t_pos == SensorPosition::plus)
                                //bkg1sum_nc[t_algo] += binsum_nc;
                                //else if (t_pos == SensorPosition::minus)
                                //bkg2sum_nc[t_algo] += binsum_nc;

                                ////reset the binsum
                                //binsum_nc = 0;
                                //}
                                //}
                            }// end of bin loop
                        }
                    }// end of channel / algo loop

                    //now need to normalize to to Hz/cm^2
                    //1 nibble = ~0.3s; t = 4096 orbits * 3564 BX * 25ns/BX = 0.365s
                    for (auto algo : m_channelInfo->getAlgoVector() )
                    {

                        //normalize the BKG sum
                        if (totalActiveArea1[algo] != 0)
                        {
                            bkg1sum[algo] /= (0.365 * 4. * totalActiveArea1[algo]);
                            bkg1sum_nc[algo] /= (0.365 * 4. * totalActiveArea1[algo]);
                        }

                        if (totalActiveArea2[algo] != 0)
                        {
                            bkg2sum[algo] /= (0.365 * 4. * totalActiveArea2[algo]);
                            bkg2sum_nc[algo] /= (0.365 * 4. * totalActiveArea2[algo]);
                        }

                        //normalize the per BX background
                        //1 nibble: t = 4096 orbits * 25ns = 0.00009988776655s
                        for (size_t i = 0; i < interface::bril::MAX_NBX; i++)
                        {
                            if (totalActiveArea1[algo] != 0) bkg1_bx[algo].at (i) /= (0.00009988776655 * 4 * totalActiveArea1[algo]);

                            if (totalActiveArea2[algo] != 0) bkg2_bx[algo].at (i) /= (0.00009988776655 * 4 * totalActiveArea2[algo]);

                            //TODO: need to normalize by bunch Current?
                            //
                            //attention, be sure to not sum all bunches but check in VME CODE TODO
                            float b1bxcurrent = beam.getBunchCurrent (1, i);
                            float b2bxcurrent = beam.getBunchCurrent (2, i);

                            if (b1bxcurrent != 0)
                                bkg1_bx[algo].at (i) /= b1bxcurrent;

                            if (b2bxcurrent != 0)
                                bkg2_bx[algo].at (i) /= b2bxcurrent;

                            //check for division by 0 TODO
                        }

                        if (bunchCurrent1Sum > 0.01)
                        {
                            bkg1sum[algo] /= bunchCurrent1Sum;
                            bkg1sum_nc[algo] /= bunchCurrent1Sum;
                        }
                        else
                        {
                            bkg1sum[algo] = 0;
                            bkg1sum_nc[algo] = 0;
                        }

                        if (bunchCurrent2Sum > 0.01)
                        {
                            bkg2sum[algo] /= bunchCurrent2Sum;
                            bkg2sum_nc[algo] /= bunchCurrent2Sum;
                        }
                        else
                        {
                            bkg2sum[algo] = 0;
                            bkg2sum_nc[algo] = 0;
                        }

                        bkg_plus[algo] = bkg1sum[algo];
                        bkg_minus[algo] = bkg2sum[algo];

                        //now insert into the deque to keep the last 4 background values
                        //m_last4BKG1[algo].push_back (bkg1sum[algo]);

                        //if (m_last4BKG1[algo].size() == 5 ) m_last4BKG1[algo].pop_front();

                        //m_last4BKG2[algo].push_back (bkg2sum[algo]);

                        //if (m_last4BKG2[algo].size() == 5 ) m_last4BKG2[algo].pop_front();

                        ////differentiate between different bunch current cases
                        //if (bunchCurrent1Sum > 0.5)
                        //bkg_plus[algo] = m_last4BKG1[algo].back();
                        //else
                        //{
                        //for (auto history : m_last4BKG1[algo])
                        //bkg_plus[algo] += history;

                        //bkg_plus[algo] /= static_cast<float> (m_last4BKG1[algo].size() );
                        //}

                        //if (bunchCurrent2Sum > 0.5)
                        //bkg_minus[algo] = m_last4BKG2[algo].back();
                        //else
                        //{
                        //for (auto history : m_last4BKG2[algo])
                        //bkg_minus[algo] += history;

                        //bkg_minus[algo] /= static_cast<float> (m_last4BKG2[algo].size() );
                        //}

                        //publish the bkg12 topic for each algo ID
                        std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_bkg12T::topicname(), this->createBKG12Topic (algo, histBuffer.m_nb4Identifier, bkg_plus[algo], bkg_minus[algo]) );
                        publishQueue->push (t_pair);
                    }

                    //copy the per algo Id per BX vectors to arrays
                    float bkg1_bx_array[interface::bril::MAX_NBX];
                    std::copy (bkg1_bx[100].begin(), bkg1_bx[100].end(), bkg1_bx_array);
                    float bkg2_bx_array[interface::bril::MAX_NBX];
                    std::copy (bkg2_bx[100].begin(), bkg2_bx[100].end(), bkg2_bx_array);

                    //here i have processed a complete NB4 and calculated the background for both algoIDs
                    interface::bril::CompoundDataStreamer tc100 (pdict);
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd1", &bkg_plus[100] );
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd2", &bkg_minus[100] );
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd1_nc", &bkg1sum_nc[100]);
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd2_nc", &bkg2sum_nc[100]);
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd1histsum", &bkg1sum[100] );
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd2histsum", &bkg2sum[100] );
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd1hist", bkg1_bx_array );
                    tc100.insert_field (backgroundhist100->payloadanchor, "bkgd2hist", bkg2_bx_array );

                    ////put into publishing queueu
                    std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_backgroundT::topicname(), background100);
                    publishQueue->push (t_pair);

                    //now algo ID 101
                    //copy data to array first
                    std::copy (bkg1_bx[101].begin(), bkg1_bx[101].end(), bkg1_bx_array);
                    std::copy (bkg2_bx[101].begin(), bkg2_bx[101].end(), bkg2_bx_array);

                    interface::bril::CompoundDataStreamer tc101 (pdict);
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd1", &bkg_plus[101] );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd2", &bkg_minus[101]);
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd1_nc", &bkg1sum_nc[101] );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd2_nc", &bkg2sum_nc[101] );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd1histsum", &bkg1sum[101] );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd2histsum", &bkg2sum[101] );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd1hist", bkg1_bx_array );
                    tc101.insert_field (backgroundhist101->payloadanchor, "bkgd2hist", bkg2_bx_array );

                    ////put into publishing queueu
                    t_pair = std::make_pair (interface::bril::bcm1futca_backgroundT::topicname(), background101);
                    publishQueue->push (t_pair);

                    //now still need to fill the monitoring variables
                    monitoring->m_bg1Plus = bkg_plus[m_bestAlgo];
                    monitoring->m_bg2Minus = bkg_minus[m_bestAlgo];
                } // end of histogrambuffer vector loop
            }

            std::string name() const
            {
                return "DefaultBackground";
            }

          private:

            toolbox::mem::Reference* createBKG12Topic (unsigned int algo, NB4Identifier nb4, float bkg1, float bkg2)
            {
                toolbox::mem::Reference* ref = 0;
                ref = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_bkg12T::maxsize() );
                ref->setDataSize (interface::bril::bcm1futca_bkg12T::maxsize() );
                interface::bril::bcm1futca_bkg12T* bkg = (interface::bril::bcm1futca_bkg12T*) (ref->getDataLocation() );
                bkg->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                bkg->setFrequency (4);

                int bestsource = interface:: bril::DataSource::BCM1FUTCA;

                bkg->setResource (bestsource, algo, 0, interface::bril::StorageType::COMPOUND);
                bkg->setTotalsize (interface::bril::bcm1futca_bkg12T::maxsize() );

                std::string pdict = interface::bril::bcm1futca_bkg12T::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::CompoundDataStreamer tc (pdict);
                tc.insert_field (bkg->payloadanchor, "bkgd1", &bkg1 );
                tc.insert_field (bkg->payloadanchor, "bkgd2", &bkg1 );

                return ref;
            }

            toolbox::mem::Reference* createColHist (bril::bcm1futcaprocessor::Histogram < uint16_t, interface::bril::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, const Beam& beam, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);

                //now loop over the histogram and mask the MIB bins (0,1)
                for (size_t iBin = 0; iBin < hist.size(); iBin++)
                {
                    size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;
                    size_t subBX = iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC;

                    if (!beam.isColliding (iBX) ) hist.at (iBin) = 0;
                    else
                    {
                        //so it acutally is colliding
                        if (!this->isLuminousBin (subBX) ) hist.at (iBin) = 0;
                    }

                }

                //first, get a rebinned histogram per bx that I can publish as agghist
                Histogram < uint16_t, interface::bril::MAX_NBX> col_hist = hist.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                //not sure if this is explicitly needed after the previous treatment
                col_hist.maskAbortGap();

                //compute total rates??
                uint32_t totalrate = col_hist.integral();

                toolbox::mem::Reference* t_colhist = 0;
                t_colhist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_col_histT::maxsize() );
                t_colhist->setDataSize (interface::bril::bcm1futca_col_histT::maxsize() );
                interface::bril::bcm1futca_col_histT* colhist = (interface::bril::bcm1futca_col_histT*) (t_colhist->getDataLocation() );
                colhist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                colhist->setFrequency (4);

                int bestsource = interface:: bril::DataSource::BCM1FUTCA;
                SensorType type = m_channelInfo->getSensorType (t_channel);

                if (type == SensorType::pCVD) bestsource = interface::bril::DataSource::BCM1FUTCAPCVD ;
                else if (type == SensorType::sCVD) bestsource = interface::bril::DataSource::BCM1FUTCASCVD ;
                else if (type == SensorType::Si) bestsource = interface::bril::DataSource::BCM1FUTCASI ;

                colhist->setResource (bestsource, t_algo, t_channel, interface::bril::StorageType::COMPOUND);
                colhist->setTotalsize (interface::bril::bcm1futca_col_histT::maxsize() );

                std::string pdict = interface::bril::bcm1futca_col_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::CompoundDataStreamer tc (pdict);
                tc.insert_field (colhist->payloadanchor, "totalhits", &totalrate );
                tc.insert_field (colhist->payloadanchor, "colhist", col_hist.data() );

                return t_colhist;
            }

            toolbox::mem::Reference* createBkgHist (bril::bcm1futcaprocessor::Histogram < uint16_t, interface::bril::MAX_NBX* interface::bril::BCM1FUTCA_BINPERBX_OCC> hist, const Beam& beam, NB4Identifier nb4, HistogramIdentifier histIdentifier)
            {
                unsigned int t_channel;
                unsigned int t_algo;
                histIdentifier.decode (t_algo, t_channel);

                //now loop over the histogram and mask the Lumi bins (0,1)
                for (size_t iBin = 0; iBin < hist.size(); iBin++)
                {
                    size_t iBX = iBin / interface::bril::BCM1FUTCA_BINPERBX_OCC;
                    size_t subBX = iBin % interface::bril::BCM1FUTCA_BINPERBX_OCC;

                    if (beam.isColliding (iBX) ) hist.at (iBin) = 0;
                    else
                    {
                        //so it acutally is not colliding
                        if (this->isLuminousBin (subBX) ) hist.at (iBin) = 0;
                    }
                }

                //first, get a rebinned histogram per bx that I can publish as agghist
                Histogram < uint16_t, interface::bril::MAX_NBX> bkg_hist = hist.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                //not sure if this is explicitly needed after the previous treatment
                bkg_hist.maskAbortGap();

                //compute total rates??
                uint32_t totalrate = bkg_hist.integral();

                toolbox::mem::Reference* t_bkghist = 0;
                t_bkghist = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_bkg_histT::maxsize() );
                t_bkghist->setDataSize (interface::bril::bcm1futca_bkg_histT::maxsize() );
                interface::bril::bcm1futca_bkg_histT* bkghist = (interface::bril::bcm1futca_bkg_histT*) (t_bkghist->getDataLocation() );
                bkghist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                bkghist->setFrequency (4);

                int bestsource = interface:: bril::DataSource::BCM1FUTCA;
                SensorType type = m_channelInfo->getSensorType (t_channel);

                if (type == SensorType::pCVD) bestsource = interface::bril::DataSource::BCM1FUTCAPCVD ;
                else if (type == SensorType::sCVD) bestsource = interface::bril::DataSource::BCM1FUTCASCVD ;
                else if (type == SensorType::Si) bestsource = interface::bril::DataSource::BCM1FUTCASI ;

                bkghist->setResource (bestsource, t_algo, t_channel, interface::bril::StorageType::COMPOUND);
                bkghist->setTotalsize (interface::bril::bcm1futca_bkg_histT::maxsize() );

                std::string pdict = interface::bril::bcm1futca_bkg_histT::payloaddict();
                xdata::Properties plist;
                plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);
                plist.setProperty ("PAYLOAD_DICT", pdict);

                interface::bril::CompoundDataStreamer tc (pdict);
                tc.insert_field (bkghist->payloadanchor, "totalhits", &totalrate );
                tc.insert_field (bkghist->payloadanchor, "bkghist", bkg_hist.data() );

                return t_bkghist;
            }

            template<class DataT>
            DataT* getHistPtr (toolbox::mem::Reference* ref, NB4Identifier nb4, SensorType source, unsigned int channel, unsigned int algo)
            {
                //ref = m_memPoolFactory->getFrame (m_memPool, DataT::maxsize() );
                ref->setDataSize (DataT::maxsize() );
                DataT* hist = (DataT*) (ref->getDataLocation() );
                //std::cout << "inmethod:" << ref->getDataLocation() << " hist " << hist << " maxsize " << DataT::maxsize() << " ref " << ref << std::endl;

                hist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                hist->setFrequency (4);

                int bestsource = interface:: bril::DataSource::BCM1FUTCA;

                if (source == SensorType::pCVD) bestsource = interface::bril::DataSource::BCM1FUTCAPCVD ;
                else if (source == SensorType::sCVD) bestsource = interface::bril::DataSource::BCM1FUTCASCVD ;
                else if (source == SensorType::Si) bestsource = interface::bril::DataSource::BCM1FUTCASI ;

                hist->setResource (bestsource, algo, channel, interface::bril::StorageType::COMPOUND);
                hist->setTotalsize (DataT::maxsize() );

                return hist;
            }

            inline bool isLuminousBin (size_t subBXbin)
            {
                if (subBXbin < 2) return false;
                else if (subBXbin > 1 && subBXbin < 6) return true;
                else return false;
            }

            inline bool isAlbedoBin (size_t bin)
            {
                if (bin % 2 == 1) return true;
                else return false;
            }

          private:
            ChannelInfo* m_channelInfo;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
            unsigned int m_bestAlgo;
            unsigned int m_albedoAboveBkg;
            //std::map<unsigned int, std::deque<float>> m_last4BKG1;
            //std::map<unsigned int, std::deque<float>> m_last4BKG2;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
