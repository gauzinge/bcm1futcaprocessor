#ifndef _bril_bcm1f_utcaprocessor_zerocounting_h_
#define _bril_bcm1f_utcaprocessor_zerocounting_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class ZeroCounting : public LumiAlgorithm
        {
          public:
            ZeroCounting (xdata::Properties& props, ChannelInfo* channels, toolbox::mem::MemoryPoolFactory* factory, toolbox::mem::Pool* pool ) :
                m_commissioning (false)
            {
                m_channelInfo = channels;
                m_memPoolFactory = factory;
                m_memPool = pool;

                m_bestAlgo = std::stoul (props.getProperty ("bestAlgo") );
                m_calibtag = props.getProperty ("calibtag");

                std::string mode = props.getProperty ("commissioningmode");

                if (mode.find ("true") != std::string::npos) m_commissioning = 1;
                else m_commissioning = 0;

            }

            void compute (  OccupancyHistogramVector& histogramVector, const Beam& beam, toolbox::squeue<std::pair<std::string, toolbox::mem::Reference*>>* publishQueue, AlbedoQueue* albedoQueue, MonitoringVariables* monitoring)
            {
                float nb4Orbits = 4096.*4.;

                //first, loop the Histogram Vector
                for (auto& histBuffer : histogramVector)
                {
                    //first, the References for the lumi numbers
                    toolbox::mem::Reference* bcm1flumi100 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_lumiT::maxsize() );
                    interface::bril::bcm1futca_lumiT* lumihist100 = this->getHistPtr<interface::bril::bcm1futca_lumiT> (bcm1flumi100, histBuffer.m_nb4Identifier, SensorType::All, 0, 100);
                    toolbox::mem::Reference* bcm1flumi101 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_lumiT::maxsize() );
                    interface::bril::bcm1futca_lumiT* lumihist101 = this->getHistPtr<interface::bril::bcm1futca_lumiT> (bcm1flumi101, histBuffer.m_nb4Identifier, SensorType::All, 0, 101);

                    //pCVD lumi
                    toolbox::mem::Reference* bcm1fpcvdlumi100 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_pcvdlumiT::maxsize() );
                    interface::bril::bcm1futca_pcvdlumiT* pcvdhist100 = this->getHistPtr<interface::bril::bcm1futca_pcvdlumiT> (bcm1fpcvdlumi100, histBuffer.m_nb4Identifier, SensorType::pCVD, 0, 100);
                    toolbox::mem::Reference* bcm1fpcvdlumi101 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_pcvdlumiT::maxsize() );
                    interface::bril::bcm1futca_pcvdlumiT* pcvdhist101 = this->getHistPtr<interface::bril::bcm1futca_pcvdlumiT> (bcm1fpcvdlumi101, histBuffer.m_nb4Identifier, SensorType::pCVD, 0, 101);
                    //Si lumi
                    toolbox::mem::Reference* bcm1fsilumi100 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_silumiT::maxsize() );
                    interface::bril::bcm1futca_silumiT* sihist100 = this->getHistPtr<interface::bril::bcm1futca_silumiT> (bcm1fsilumi100, histBuffer.m_nb4Identifier, SensorType::Si, 0, 100);
                    toolbox::mem::Reference* bcm1fsilumi101 = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_silumiT::maxsize() );
                    interface::bril::bcm1futca_silumiT* sihist101 = this->getHistPtr<interface::bril::bcm1futca_silumiT> (bcm1fsilumi101, histBuffer.m_nb4Identifier, SensorType::Si, 0, 101);

                    //For the lumi topic
                    std::string pdict = interface::bril::bcm1futca_lumiT::payloaddict();
                    xdata::Properties plist;
                    plist.setProperty ("DATA_VERSION", interface::bril::DATA_VERSION);
                    plist.setProperty ("PAYLOAD_DICT", pdict);

                    //now I have a histogram buffer object
                    //histBuffer.first is the NB4 identifier in case I need it later
                    //histBuffer.second is the Map<HistogramIdentifier, Histogram>

                    //here i should instantiate the perSensor type arrays
                    //lumi_bx, lumi_bx_raw, inst_lumi, inst_lumi_raw
                    //to be fair, I need these split up by AlgoIDs too

                    //channel maks for lumi publishing
                    uint32_t masklow = 0;
                    uint32_t maskhigh = 0;

                    std::map<SensorType, uint32_t> masklowmap
                    {
                        std::make_pair (SensorType::pCVD, 0),
                        std::make_pair (SensorType::sCVD, 0),
                        std::make_pair (SensorType::Si, 0)
                    };

                    std::map<SensorType, uint32_t> maskhighmap = masklowmap;

                    // instantaneous lumi and raw instantaneous lumi per sensor type / algo
                    std::map<SensorIdentifier, float> inst_lumi
                    {
                        std::make_pair (SensorIdentifier (SensorType::sCVD, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::sCVD, 101), 0),
                        std::make_pair (SensorIdentifier (SensorType::pCVD, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::pCVD, 101), 0),
                        std::make_pair (SensorIdentifier (SensorType::Si, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::Si, 101), 0)
                    };
                    std::map<SensorIdentifier, float> inst_lumi_raw = inst_lumi;

                    //per bx lumi and raw lumi per sensor type and bx
                    std::map<SensorIdentifier, std::vector<float>> lumi_bx;
                    lumi_bx.emplace (SensorIdentifier (SensorType::sCVD, 100), std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    lumi_bx.emplace (SensorIdentifier (SensorType::sCVD, 101), std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    lumi_bx.emplace (SensorIdentifier (SensorType::pCVD, 100), std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    lumi_bx.emplace (SensorIdentifier (SensorType::pCVD, 101), std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    lumi_bx.emplace (SensorIdentifier (SensorType::Si, 100), std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    lumi_bx.emplace (SensorIdentifier (SensorType::Si, 101), std::vector<float> (interface::bril::MAX_NBX, 0.0) );

                    std::map<SensorIdentifier, std::vector<float>> lumi_bx_raw = lumi_bx;

                    //sensor type counter
                    std::map<SensorIdentifier, int> sensorCounter
                    {
                        std::make_pair (SensorIdentifier (SensorType::sCVD, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::sCVD, 101), 0),
                        std::make_pair (SensorIdentifier (SensorType::pCVD, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::pCVD, 101), 0),
                        std::make_pair (SensorIdentifier (SensorType::Si, 100), 0),
                        std::make_pair (SensorIdentifier (SensorType::Si, 101), 0)
                    };

                    //these are for the global average
                    std::map<unsigned int, float> avg_lumi
                    {
                        std::make_pair (100, 0),
                        std::make_pair (101, 0),
                    };

                    std::map<unsigned int, float> avg_lumi_raw = avg_lumi;

                    std::map<unsigned int, std::vector<float>> avg_lumi_bx;
                    avg_lumi_bx.emplace (100, std::vector<float> (interface::bril::MAX_NBX, 0.0) );
                    avg_lumi_bx.emplace (101, std::vector<float> (interface::bril::MAX_NBX, 0.0) );

                    std::map<unsigned int, std::vector<float>> avg_lumi_bx_raw = avg_lumi_bx;

                    std::map<unsigned int, int> total_count
                    {
                        std::make_pair (100, 0),
                        std::make_pair (101, 0),
                    };

                    //to temporarily store all the uncorrected muBX for the albedo queue
                    std::map<HistogramIdentifier, std::vector<float>> t_albedoQueueMap;

                    for (auto& hist : histBuffer.m_channelMap)
                    {
                        unsigned int t_channel;
                        unsigned int t_algo;
                        hist.first.decode (t_algo, t_channel);
                        //now looping the acutal channel histograms
                        //hist.first is the Histogram Identifier
                        //hist.second is the histogram object

                        //then, shift the MIB of BX0 back to the end of the abort gap
                        hist.second.shift (-1);

                        //then, rebin - this is now the agghist after masking the abort gap
                        Histogram < uint16_t, interface::bril::MAX_NBX> t_hist = hist.second.rebin<6>(); //rebin by a factor of 6 to get the per BX histogram
                        t_hist.maskAbortGap();//maks the abort gap first before integrating for total rate

                        //now test if we want this channel for lumi calculations
                        if (m_channelInfo->useLumi (t_channel) )
                        {
                            //first, get the sensor type
                            SensorType t_type = m_channelInfo->getSensorType (t_channel);
                            SensorIdentifier t_id (t_type, t_algo);
                            sensorCounter[t_id] += 1;
                            total_count[t_algo] += 1;

                            //for the per channel lumi publication
                            uint32_t t_masklow = 0;
                            uint32_t t_maskhigh = 0;

                            if (t_channel < 32)
                            {
                                masklow |= (1 << (t_channel - 1) );
                                t_masklow |= (1 << (t_channel - 1) );
                                masklowmap[t_type] |= (1 << (t_channel - 1) );
                            }
                            else
                            {
                                maskhigh |= (1 << (t_channel - 33) ); //equivalent to t_channel-1-32
                                t_maskhigh |= (1 << (t_channel - 33) ); //equivalent to t_channel-1-32
                                maskhighmap[t_type] |= (1 << (t_channel - 33) );
                            }

                            int t_nonZeroBunches = 0;
                            float muBX[interface::bril::MAX_NBX];
                            memset (muBX, 0, sizeof (muBX) );
                            float muBX_uncorrected[interface::bril::MAX_NBX];
                            memset (muBX_uncorrected, 0, sizeof (muBX_uncorrected) );
                            float lumiBX[interface::bril::MAX_NBX];
                            memset (lumiBX, 0, sizeof (lumiBX) );
                            float lumiChannel = 0;
                            float lumiChannelRaw = 0;

                            //NOW the actual lumi claculation
                            //but first, get the albedo fraction for this channel and algorithm
                            //first we need the correction factors
                            Corrections t_corr = m_channelInfo->getCorrections (hist.first);
                            //t_corr.print();

                            //for (size_t iBX = 0; iBX < t_hist.size(); iBX++ )
                            for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++ )
                            {
                                //calculate 1 - probability of not having a zero (so # of non-zero/# of orbits)
                                float rate0 = 1. - ( (float) t_hist.at (iBX) / ( (float) m_channelInfo->getFactor (hist.first) * (float) nb4Orbits ) );

                                //if negative, set 0
                                if (rate0 < 0.)
                                    rate0 = 0;

                                //calculate the pileup parameter as per 0counting formula
                                //first the uncorrected
                                muBX_uncorrected[iBX] = -1. * log (rate0);
                                //and now the albedo corrected version
                                muBX[iBX] = muBX_uncorrected[iBX] * m_channelInfo->getAlbedoFraction (hist.first, iBX);//TODO - m_channelInfo->getNoise (hist.first);

                                //first we need the correction factors
                                //Corrections t_corr = m_channelInfo->getCorrections (hist.first);

                                //if(iBX == 0)std::cout << YELLOW << "Debug: Channel: " << t_channel << " Algo: " << t_algo << " SigmaVis: "<<t_corr.m_sigmaVis << RESET << std::endl;
                                //now calculate some lumi
                                lumiBX[iBX] = muBX[iBX] * 11246 / t_corr.m_sigmaVis + pow (muBX[iBX], 2) * pow ( (11246 / t_corr.m_sigmaVis), 2) * t_corr.m_nonLinearity * (1 + t_corr.m_nonLinearity * t_corr.m_calibSBIL);

                                //the ones that distinguish algoID and sensor type
                                lumi_bx[t_id].at (iBX) += lumiBX[iBX];
                                lumi_bx_raw[t_id].at (iBX) += muBX[iBX];
                                //the ones that only distinguish algoID
                                avg_lumi_bx[t_algo].at (iBX) += lumiBX[iBX];
                                avg_lumi_bx_raw[t_algo].at (iBX) += muBX[iBX];


                                //test if the bunch is colliding
                                if (beam.isColliding (iBX) )
                                {
                                    lumiChannel += lumiBX[iBX];
                                    lumiChannelRaw += muBX[iBX];
                                    t_nonZeroBunches++;
                                }
                            } // end of BX loop

                            //now can insert uncorrected muBX to albedo queue
                            std::vector<float> t_tmpVec (muBX_uncorrected, muBX_uncorrected + interface::bril::MAX_NBX );
                            t_albedoQueueMap[hist.first] = t_tmpVec;

                            inst_lumi[t_id] += lumiChannel;
                            inst_lumi_raw[t_id] += lumiChannelRaw;

                            avg_lumi[t_algo] += lumiChannel;
                            avg_lumi_raw[t_algo] += lumiChannelRaw;

                            if (m_commissioning || (beam.m_vdmFlag && beam.m_vdmIP5) )
                                //only publish by channel histograms if the comissioning mode is enabled or the VdM flag is set
                            {
                                //create a new Refernce for this channel and algo
                                toolbox::mem::Reference* channellumi = m_memPoolFactory->getFrame (m_memPool, interface::bril::bcm1futca_lumiT::maxsize() );
                                interface::bril::bcm1futca_lumiT* channelhist = this->getHistPtr<interface::bril::bcm1futca_lumiT> (channellumi, histBuffer.m_nb4Identifier, m_channelInfo->getSensorType (t_channel), t_channel, t_algo);

                                //now need to fill the compound data streamer with the per channel lumi data
                                this->fillDataChannel<interface::bril::bcm1futca_lumiT> (channelhist, pdict, lumiChannelRaw, lumiChannel, muBX, lumiBX, t_masklow, t_maskhigh);

                                //and push to the queue
                                //needed to change the topic name for per channel data
                                //std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (interface::bril::bcm1futca_lumiT::topicname(), channellumi);
                                //make topicname
                                std::stringstream ss;
                                ss << "bcm1futca_lumi_" << t_algo << "_" << t_channel;
                                std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (ss.str(), channellumi);
                                publishQueue->push (t_pair);

                            }
                        } // end of if(useLumi)
                    } // end of channel / algo loop

                    //now push the albedoqueuemap to the albedo queue
                    albedoQueue->push_back (t_albedoQueueMap);

                    //here normalize the inst lumi by the number of channels and normalize the lumi_bx by the number of channels
                    //i.e. build the average
                    //first the per algo container
                    for (auto algoID : total_count)
                    {
                        if (algoID.second)
                        {
                            avg_lumi[algoID.first] /= (float) algoID.second;
                            avg_lumi_raw[algoID.first] /= (float) algoID.second;

                            for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                            {
                                avg_lumi_bx[algoID.first][iBX] /= (float) algoID.second;
                                avg_lumi_bx_raw[algoID.first][iBX] /= (float) algoID.second;
                            } // end of iBX loop
                        } // end of if(counter)
                    } //end of SensorIdentifier loop

                    //now the per algo and sensor type container
                    for (auto sensorID : sensorCounter )
                    {
                        //sensorID.first is the sensorIdentifier
                        //sensorID.second is the channel counter for a given sensor type + algoID
                        if (sensorID.second)
                        {
                            //unsigned int algo = sensorID.second.m_algoID;
                            inst_lumi[sensorID.first] /= (float) sensorID.second;
                            inst_lumi_raw[sensorID.first] /= (float) sensorID.second;

                            for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                            {
                                lumi_bx[sensorID.first][iBX] /= (float) sensorID.second;
                                lumi_bx_raw[sensorID.first][iBX] /= (float) sensorID.second;
                            } // end of iBX loop
                        } // end of if(counter)
                    } //end of SensorIdentifier loop


                    //////////////////////////////////////////////////////////
                    //Fill the data for the different types in the toolbox::mem::Reference and push to queue
                    //////////////////////////////////////////////////////////
                    //ALGO ID=100
                    //if (m_bestAlgo == 100)
                    //{
                    std::stringstream topicname;

                    unsigned int t_algo = 100;
                    this->fillData<interface::bril::bcm1futca_lumiT> (lumihist100, pdict, avg_lumi_raw[t_algo], avg_lumi[t_algo], avg_lumi_bx_raw[t_algo], avg_lumi_bx[t_algo], masklow, maskhigh);
                    topicname << interface::bril::bcm1futca_lumiT::topicname() << "_" << t_algo;
                    std::pair<std::string, toolbox::mem::Reference*> t_pair = std::make_pair (topicname.str(), bcm1flumi100);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );

                    //now deal with pCVD lumi
                    //ALGO ID=100
                    SensorIdentifier t_identifier (SensorType::pCVD, 100);
                    this->fillData<interface::bril::bcm1futca_pcvdlumiT> (pcvdhist100, pdict, inst_lumi_raw[t_identifier], inst_lumi[t_identifier], lumi_bx_raw[t_identifier], lumi_bx[t_identifier], masklowmap[SensorType::pCVD], maskhighmap[SensorType::pCVD]);
                    topicname << interface::bril::bcm1futca_pcvdlumiT::topicname() << "_" << t_algo;
                    t_pair = std::make_pair (topicname.str(), bcm1fpcvdlumi100);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );

                    //now deal with Si lumi
                    //ALGO ID=100
                    t_identifier.m_type = SensorType::Si;
                    this->fillData<interface::bril::bcm1futca_silumiT> (sihist100, pdict, inst_lumi_raw[t_identifier], inst_lumi[t_identifier], lumi_bx_raw[t_identifier], lumi_bx[t_identifier], masklowmap[SensorType::Si], maskhighmap[SensorType::Si]);
                    topicname << interface::bril::bcm1futca_silumiT::topicname() << "_" << t_algo;
                    t_pair = std::make_pair (topicname.str(), bcm1fsilumi100);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );

                    //in case I publish just one, i need to release the other references
                    //bcm1flumi101->release();
                    //bcm1flumi101 = 0;
                    //bcm1fpcvdlumi101->release();
                    //bcm1fpcvdlumi101 = 0;
                    //bcm1fsilumi101->release();
                    //bcm1fsilumi101 = 0;
                    //}
                    //else if (m_bestAlgo == 101)
                    //{
                    //ALGO ID=101
                    /*unsigned int*/ t_algo = 101;
                    this->fillData<interface::bril::bcm1futca_lumiT> (lumihist101, pdict, avg_lumi_raw[t_algo], avg_lumi[t_algo], avg_lumi_bx_raw[t_algo], avg_lumi_bx[t_algo], masklow, maskhigh);
                    topicname << interface::bril::bcm1futca_lumiT::topicname() << "_" << t_algo;
                    /*std::pair<std::string, toolbox::mem::Reference*>*/ t_pair = std::make_pair (topicname.str(), bcm1flumi101);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );


                    //ALGO ID=101
                    //SensorIdentifier t_identifier (SensorType::pCVD, 101);
                    t_identifier.m_algoID = 101;
                    t_identifier.m_type = SensorType::pCVD;
                    this->fillData<interface::bril::bcm1futca_pcvdlumiT> (pcvdhist101, pdict, inst_lumi_raw[t_identifier], inst_lumi[t_identifier], lumi_bx_raw[t_identifier], lumi_bx[t_identifier], masklowmap[SensorType::pCVD], maskhighmap[SensorType::pCVD]);
                    topicname << interface::bril::bcm1futca_pcvdlumiT::topicname() << "_" << t_algo;
                    t_pair = std::make_pair (topicname.str(), bcm1fpcvdlumi101);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );

                    //ALGO ID=101
                    t_identifier.m_type = SensorType::Si;
                    this->fillData<interface::bril::bcm1futca_silumiT> (sihist101, pdict, inst_lumi_raw[t_identifier], inst_lumi[t_identifier], lumi_bx_raw[t_identifier], lumi_bx[t_identifier], masklowmap[SensorType::Si], maskhighmap[SensorType::Si]);
                    topicname << interface::bril::bcm1futca_silumiT::topicname() << "_" << t_algo;
                    t_pair = std::make_pair (topicname.str(), bcm1fsilumi101);
                    publishQueue->push (t_pair);
                    topicname.str (std::string() );

                    //in case I publish just one, i need to release the other references
                    //bcm1flumi100->release();
                    //bcm1flumi100 = 0;
                    //bcm1fpcvdlumi100->release();
                    //bcm1fpcvdlumi100 = 0;
                    //bcm1fsilumi100->release();
                    //bcm1fsilumi100 = 0;
                    //}

                    //////////////////////////////////////////////////////////
                    //Fill the monitoring variables
                    //////////////////////////////////////////////////////////
                    monitoring->m_monBeammode = beam.getBeamMode();
                    monitoring->m_monFill = histBuffer.m_nb4Identifier.m_fillnum;
                    monitoring->m_monRun = histBuffer.m_nb4Identifier.m_runnum;
                    monitoring->m_monLs = histBuffer.m_nb4Identifier.m_lsnum;
                    monitoring->m_monNb = histBuffer.m_nb4Identifier.m_nbnum;
                    toolbox::TimeVal t = toolbox::TimeVal::gettimeofday();
                    monitoring->m_monTimestamp = t;
                    //now the lumi types
                    SensorIdentifier t_sCVD (SensorType::sCVD, m_bestAlgo);
                    monitoring->m_lumiAvg[0] = inst_lumi[t_sCVD];
                    monitoring->m_lumiAvg_raw[0] = inst_lumi_raw[t_sCVD];
                    SensorIdentifier t_pCVD (SensorType::pCVD, m_bestAlgo);
                    monitoring->m_lumiAvg[1] = inst_lumi[t_pCVD];
                    monitoring->m_lumiAvg_raw[1] = inst_lumi_raw[t_pCVD];
                    SensorIdentifier t_Si (SensorType::Si, m_bestAlgo);
                    monitoring->m_lumiAvg[2] = inst_lumi[t_Si];
                    monitoring->m_lumiAvg_raw[2] = inst_lumi_raw[t_Si];
                } // end of histogrambuffer vector loop
            }

            template<class DataT>
            DataT* getHistPtr (toolbox::mem::Reference* ref, NB4Identifier nb4, SensorType source, unsigned int channel, unsigned int algo)
            {
                //ref = m_memPoolFactory->getFrame (m_memPool, DataT::maxsize() );
                ref->setDataSize (DataT::maxsize() );
                DataT* hist = (DataT*) (ref->getDataLocation() );
                //std::cout << "inmethod:" << ref->getDataLocation() << " hist " << hist << " maxsize " << DataT::maxsize() << " ref " << ref << std::endl;

                hist->setTime (nb4.m_fillnum, nb4.m_runnum, nb4.m_lsnum, nb4.m_nbnum, nb4.m_timestampsec, nb4.m_timestampmsec);
                hist->setFrequency (4);

                int bestsource = interface:: bril::DataSource::BCM1FUTCA;

                if (source == SensorType::pCVD) bestsource = interface::bril::DataSource::BCM1FUTCAPCVD ;
                else if (source == SensorType::sCVD) bestsource = interface::bril::DataSource::BCM1FUTCASCVD ;
                else if (source == SensorType::Si) bestsource = interface::bril::DataSource::BCM1FUTCASI ;

                hist->setResource (bestsource, algo, channel, interface::bril::StorageType::COMPOUND);
                hist->setTotalsize (DataT::maxsize() );

                return hist;
            }

            template<class DataT>
            void fillDataChannel (DataT* hist, std::string dict, float avgraw, float avg, float* bxraw, float* bx, uint32_t masklow, uint32_t maskhigh )
            {
                //now need to fill the compound data streamer with the lumi data
                interface::bril::CompoundDataStreamer streamer (dict);
                streamer.insert_field (hist->payloadanchor, "calibtag", m_calibtag.c_str() );
                streamer.insert_field (hist->payloadanchor, "avgraw", &avgraw );
                streamer.insert_field (hist->payloadanchor, "avg", &avg );
                streamer.insert_field (hist->payloadanchor, "bxraw", bxraw );
                streamer.insert_field (hist->payloadanchor, "bx", bx );
                streamer.insert_field (hist->payloadanchor, "masklow", &masklow );
                streamer.insert_field (hist->payloadanchor, "maskhigh", &maskhigh );
            }

            template<class DataT>
            void fillData (DataT* hist, std::string dict, float avgraw, float avg, std::vector<float> bxraw, std::vector<float> bx, uint32_t masklow, uint32_t maskhigh )
            {
                //copy the contents of the vector into an array
                float lumi_bx_array[interface::bril::MAX_NBX];
                float lumi_bx_raw_array[interface::bril::MAX_NBX];

                std::copy (bx.begin(), bx.end(), lumi_bx_array);
                std::copy (bxraw.begin(), bxraw.end(), lumi_bx_raw_array);
                //now need to fill the compound data streamer with the lumi data
                interface::bril::CompoundDataStreamer streamer (dict);
                streamer.insert_field (hist->payloadanchor, "calibtag", m_calibtag.c_str() );
                streamer.insert_field (hist->payloadanchor, "avgraw", &avgraw );
                streamer.insert_field (hist->payloadanchor, "avg", &avg );
                streamer.insert_field (hist->payloadanchor, "bxraw", lumi_bx_raw_array );
                streamer.insert_field (hist->payloadanchor, "bx", lumi_bx_array );
                streamer.insert_field (hist->payloadanchor, "masklow", &masklow );
                streamer.insert_field (hist->payloadanchor, "maskhigh", &maskhigh );
            }

            std::string name() const
            {
                return "ZeroCounting";
            }
            //Stream operator
            std::string printSensorType ( const SensorType& ptype )
            {
                if (ptype == SensorType::sCVD)
                    return"sCVD";
                else if (ptype == SensorType::pCVD)
                    return "pCVD";
                else if (ptype == SensorType::Si)
                    return "Si";
                else if (ptype == SensorType::Disconnected)
                    return "Disconnecte";
                else if (ptype == SensorType::All)
                    return "All";
                else return "";
            }

          private:
            ChannelInfo* m_channelInfo;
            toolbox::mem::MemoryPoolFactory* m_memPoolFactory;
            toolbox::mem::Pool* m_memPool;
            unsigned int m_bestAlgo;
            std::string m_calibtag;
            bool m_commissioning;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
