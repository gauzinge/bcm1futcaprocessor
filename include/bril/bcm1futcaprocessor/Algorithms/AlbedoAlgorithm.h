#ifndef _bril_bcm1f_utcaprocessor_albedoalgorithm_h_
#define _bril_bcm1f_utcaprocessor_albedoalgorithm_h_

#include "bril/bcm1futcaprocessor/AlgorithmFactory.h"
#include "bril/bcm1futcaprocessor/Utils/Utils.h"
#include <deque>
#include <fstream>
#include <chrono>
#include <thread>

namespace bril {

    namespace bcm1futcaprocessor {

        //Zero Counting Lumi Algorithm
        class DefaultAlbedo : public AlbedoAlgorithm
        {
          public:
            DefaultAlbedo (xdata::Properties& props, ChannelInfo* channels)
            {
                m_channelInfo = channels;
                m_albedoModel.clear();

                //parse the properties
                std::string albedoFileName = props.getProperty ("modelFilePath");
                m_queueLenght = static_cast<size_t> (std::stoul (props.getProperty ("albedoQueueLength") ) );
                m_calculationTime = std::stoul (props.getProperty ("albedoCalculationTime") );

                this->readAlbedoModel (albedoFileName);

            }

            void compute (  const Beam& beam, AlbedoQueue* albedoQueue, toolbox::BSem& appLock, std::map<std::string, std::vector<float>>& chart_data, std::map<std::string, std::vector<float>>& fraction_data) //pass data struct for albedo plots??
            {
                //first, initialize the empty containers for corrected and uncorrected
                this->initArrays();
                //need to lock access to the albedoQueue when operating on it
                //same goes for Channel Info

                //loop the queue
                if (albedoQueue->size() )
                {
                    appLock.take();

                    for (auto albedoHist : *albedoQueue)
                    {
                        //loop the channels
                        for (auto hist : albedoHist)
                        {
                            //hist.first is the HistogramIdentifier
                            //hist.second is the vector<float>
                            for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                                m_uncorrected[hist.first][iBX] += hist.second.at (iBX);

                            //increment the counter for this channel/algo combination
                            //should be equivalent to albedoQueue.size()
                            m_normalizationCounter[hist.first]++;
                        }
                    }

                    appLock.give();
                }
                else
                {
                    std::this_thread::sleep_for (std::chrono::seconds (m_calculationTime) );
                    return;
                }

                //now build the average
                for (auto hist : m_uncorrected)
                {
                    for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                        //hist.second[iBX] /= m_normalizationCounter[hist.first];
                        hist.second[iBX] /= albedoQueue->size();
                }

                if (albedoQueue->size() )
                {
                    //first initialize corrected to uncorrected
                    m_corrected = m_uncorrected;

                    this->applyModel (beam);

                    //now I have corrected so I can calculate the fraction
                    for (auto hist : m_uncorrected)
                    {
                        //hist.first is the HistogramIdentifier
                        //hist.second is the float array

                        appLock.take();
                        //float noise = 0;
                        //size_t count = 0;

                        for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                        {
                            if (hist.second[iBX] != 0)
                            {
                                float fraction = m_corrected[hist.first][iBX] / hist.second[iBX];

                                if (fraction < 0) fraction = 0;

                                m_channelInfo->setAlbedoFraction (hist.first, iBX, fraction);
                                m_fraction[hist.first].at (iBX) = fraction;
                            }
                            else
                            {
                                m_channelInfo->setAlbedoFraction (hist.first, iBX, 1.);
                                m_fraction[hist.first].at (iBX) = 1.;
                            }

                            //calculate the noise in the abort gap
                            //if (iBX > 3534 + 5 && iBX < 3564 - 5 )
                            //{
                            //noise += m_corrected[hist.first][iBX];
                            //count++;
                            //}
                        }

                        //noise /= (float) count;
                        //m_channelInfo->setNoise (hist.first, noise);

                        appLock.give();
                    }

                    //make sure queue is correct max size
                    if (albedoQueue->size() > m_queueLenght)
                    {
                        size_t overflow = albedoQueue->size() - m_queueLenght;
                        appLock.take();
                        albedoQueue->erase (albedoQueue->begin(), albedoQueue->begin() + overflow);
                        appLock.give();
                    }

                    //now could still fill the data containers for the plots from the corrected and uncorrected members
                    for (auto hist : m_uncorrected)
                    {
                        unsigned int channel;
                        unsigned int algo;
                        hist.first.decode (algo, channel);
                        std::stringstream ss_uncorr;
                        ss_uncorr << "Channel_" << channel << "_Algo_" << algo << "_uncorrected";
                        std::stringstream ss_corr;
                        ss_corr << "Channel_" << channel << "_Algo_" << algo << "_corrected";
                        std::stringstream ss;
                        ss << "Channel_" << channel << "_Algo_" << algo;

                        chart_data[ss_uncorr.str()] = hist.second;
                        chart_data[ss_corr.str()] = m_corrected[hist.first];
                        fraction_data[ss.str()] = m_fraction[hist.first];
                    }

                } //end if(queue.size)

                std::this_thread::sleep_for (std::chrono::seconds (m_calculationTime) );
            }

            std::string name() const
            {
                return "DefaultAlbedo";
            }

          private:

            void readAlbedoModel (std::string filename)
            {
                std::ifstream file (bril::bcm1futcaprocessor::expandEnvironmentVariables (filename).c_str() );
                float fraction;

                size_t idx = 0;

                //std::cout << "Reading Albedo Model: " << std::endl;

                while (file >> fraction)
                {
                    m_albedoModel.push_back (fraction);
                    idx++;
                }
            }

            void initArrays()
            {
                //first clear the maps
                m_uncorrected.clear();
                m_corrected.clear();
                m_normalizationCounter.clear();
                m_fraction.clear();

                //then init with 0 arrays for each histogram identifier
                for (auto hist : m_channelInfo->getHistIdentifierVector() )
                {
                    std::vector<float> dummy (interface::bril::MAX_NBX, 0);
                    m_uncorrected[hist] = dummy;
                    m_corrected[hist] = dummy;
                    m_normalizationCounter[hist] = 0;
                    m_fraction[hist] = dummy;
                }
            }

            void applyModel (const Beam& beam)
            {
                //now calculate the corrections and fractions
                for (auto hist : m_uncorrected)
                {
                    //hist.first is the HistogramIdentifier
                    //hist.second is the float array
                    for (size_t iBX = 0; iBX < interface::bril::MAX_NBX; iBX++)
                    {
                        if (beam.isColliding (iBX) )
                        {
                            //loop all the bins after iBX for the length of the albedo model
                            for (size_t j = iBX + 1; j < iBX + m_albedoModel.size(); j++)
                            {
                                //this is the case where j extends beyond the orbit, so I need to wrap around to the next
                                if (j >= interface::bril::MAX_NBX)
                                    m_corrected[hist.first][j - interface::bril::MAX_NBX] -= m_corrected[hist.first][iBX] * m_albedoModel[j - iBX];
                                //this is the normal case
                                else
                                    m_corrected[hist.first][j] -= m_corrected[hist.first][iBX] * m_albedoModel[j - iBX];
                            } //end of albedo model loop
                        } //end of colliding condition
                    }// end of BX loop
                } //end of channel/algo loop
            }

          private:
            ChannelInfo* m_channelInfo;
            std::vector<float> m_albedoModel;
            size_t m_queueLenght;
            unsigned int m_calculationTime;
            std::map<HistogramIdentifier, unsigned int> m_normalizationCounter;
            std::map<HistogramIdentifier, std::vector<float>> m_uncorrected;
            std::map<HistogramIdentifier, std::vector<float>> m_corrected;
            std::map<HistogramIdentifier, std::vector<float>> m_fraction;
        };
    } //namespace bcm1futcaprocesor
} //namespace bril
#endif
